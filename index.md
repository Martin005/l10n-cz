---
layout: default
title: L10N.cz
redirect_from:
  - /wiki/
  - /search/
  - /Hlavní_strana/
  - /Kategorie:L10NWiki/
  - /L10N.cz_Wiki:L10NWiki/
  - /L10N.cz_Wiki:L10N.cz_Wiki/
nav_order: 0
---

# {{ site.title }}

Tento web a wiki jsou společným kooperačním místem překladatelů a dalších zájemců
o českou lokalizaci svobodného a otevřeného programového vybavení.

## Cíle projektu a komunity

- [Získávání nových zájemců o překlad]({% link _pages/zapojte-se.md %})
- Šíření povědomí o českých překladech
- Zjednodušení práce na překladech a odstranění dvojitých překladů
- Sjednocení [terminologie]({% link _pages/wiki/Slovníky/Překladatelský_slovník.md %}) v oblasti otevřeného a svobodného softwaru
- Zvyšování úrovně českých překladů
- Spolupráce mezi [překladatelskými týmy]({% link _pages/wiki/Překladatelské_týmy.md %}) z různých projektů

<hr />

## Zprávičky
{% assign posts = site.posts | sort: 'date' | reverse %}
<ul>
  {% for post in posts limit:site.pagination.per_page %}
    <li>
      <a href="{{ post.url | relative_url }}">{{ post.title }}</a>
      {% include l10n-cz/page_meta.html item=post %}
    </li>
  {% endfor %}
  <li><a href="{{ '/archiv/' | relative_url }}">Všechny zprávičky »</a></li>
</ul>
