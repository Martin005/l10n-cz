require 'jekyll/tagging'

module Jekyll
  module Helpers
    def jekyll_tagging_slug(str)
      # see https://github.com/pattex/jekyll-tagging/issues/53
      Jekyll::Utils::slugify(str.to_s, mode: 'latin', cased: true)
    end
  end

  class TagPage < Page
    def initialize(site, base, dir, name, data = {})
      # see https://github.com/pattex/jekyll-tagging/pull/83
      if (data['title'])
        data['title'] = data['title'] % { 'tag': data['tag'] }
      end

      self.content = data.delete('content') || ''
      self.data    = data

      super(site, base, dir[-1, 1] == '/' ? dir : '/' + dir, name)
    end

    def read_yaml(*)
      # Do nothing
    end
  end
end
