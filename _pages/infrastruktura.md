---
layout: page
title: Infrastruktura
permalink: /Infrastruktura/
redirect_from:
  - /Jak_psát_na_web_L10N.cz/
  - /Kategorie:Nápověda/
nav_order: 6
---

{% include l10n-cz/table-of-contents.html %}

# Webové stránky L10N.cz

  - Monitoring: <https://status.l10n.cz/>

## WWW + Wiki

  - Hosting: [GitLab Pages](#gitlab)
  - Systém: [Jekyll](https://jekyllrb.com/)
  - Kód: <https://gitlab.com/OpenAlt/L10Ncz/l10n-cz>

  Na webu informujeme o obecném dění v překladatelské komunitě. Jeho kód
  je veřejný a kdokoliv může navrhnout vylepšení nebo nový obsah. Tady najdete
  [podrobnosti](https://gitlab.com/OpenAlt/L10Ncz/l10n-cz/-/blob/master/README.md).

# Služby

## GitLab

  - Odkaz: <https://gitlab.com/OpenAlt/L10Ncz>
  - GitLab Pages: <https://docs.gitlab.com/ce/user/project/pages/>
  - GitLab CI/CD: <https://docs.gitlab.com/ce/ci/>

## GitHub

  - Nahrazený [GitLabem](#gitlab).
  - ~~Odkaz: <https://github.com/L10Ncz>~~


## Matomo

  - Odkaz: <https://analytics.openalt.org/>

## E-mailová konference

  - Odkaz: <https://lists.openalt.org/wws/info/diskuze-l10n-cz>
  - Hosting: [server spolku OpenAlt](mailto:spravci@lists.openalt.org)

## Matrix

  - Odkaz: <https://matrix.to/#/#l10ncz:matrix.org>
  - Správci:
      - Michal Stanke
      - Tomáš Zelina