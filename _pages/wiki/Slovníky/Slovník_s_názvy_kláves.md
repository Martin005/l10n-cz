---
layout: page
title: Slovník s názvy kláves
permalink: /wiki/Slovníky/Slovník_s_názvy_kláves/
redirect_from:
  - /Slovník_s_názvy_kláves/
  - /Klávesy/
parent: Slovníky
---

## Překlad kláves

| kód SDL | unicode | znak      | anglicky             | česky                                     | poznámky                                                                                                |
| ------- | ------- | --------- | -------------------- | ----------------------------------------- | ------------------------------------------------------------------------------------------------------- |
| 8       | U+0008  | <control> | backspace            |                                           |                                                                                                         |
| 9       | U+0009  | <control> | tab                  | tabulátor                                 |                                                                                                         |
| 12      |         |           | (CLEAR)              |                                           |                                                                                                         |
| 13      | U+000D  | <control> | enter, return        | enter                                     |                                                                                                         |
| 19      |         |           | pause                | pauza                                     |                                                                                                         |
| 27      | U+001B  | <control> | escape               | escape                                    |                                                                                                         |
| 32      | U+0020  | SPACE     | space                | mezera, mezerník                          |                                                                                                         |
| 33      | U+0021  | \!        | exclamation mark     | vykřičník                                 |                                                                                                         |
| 34      | U+0022  | "         | quotation mark       | (horní) (rovné) uvozovky                  |                                                                                                         |
| 35      | U+0023  | \#        | hash                 | hash, mřížka (mříž, mříže), křížek        | NUMBER SIGN, crosshatch                                                                                 |
| 36      | U+0024  | $         | dollar sign          | dolar                                     | string (v programování), označení proměnné (PHP, Perl, shell...)                                        |
| 38      | U+0026  | &         | ampersand, and       | ampersand, a, (ligatura) et               |                                                                                                         |
| 39      | U+0027  | '         | apostrophe           | apostrof                                  |                                                                                                         |
| 40      | U+0028  | (         | left parenthesis     | levá (kulatá) závorka                     |                                                                                                         |
| 41      | U+0029  | )         | right parenthesis    | pravá (kulatá) závorka                    |                                                                                                         |
| 42      | U+002A  | \*        | asterisk             | hvězdička                                 |                                                                                                         |
| 43      | U+002B  | \+        | plus sign            | plus                                      |                                                                                                         |
| 44      | U+002C  | ,         | comma                | čárka                                     |                                                                                                         |
| 45      | U+002D  | \-        | minus                | mínus                                     | HYPHEN = spojovník (v textových editorech se význam klávesy/znaku 2D nechápe jako matematické znaménko) |
| 46      | U+002E  | .         | period               | tečka                                     | FULL STOP                                                                                               |
| 47      | U+002F  | /         | slash                | lomítko                                   | SOLIDUS                                                                                                 |
| 48      | U+0030  | 0         |                      |                                           |                                                                                                         |
| 49      | U+0031  | 1         |                      |                                           |                                                                                                         |
| 50      | U+0032  | 2         |                      |                                           |                                                                                                         |
| 51      | U+0033  | 3         |                      |                                           |                                                                                                         |
| 52      | U+0034  | 4         |                      |                                           |                                                                                                         |
| 53      | U+0035  | 5         |                      |                                           |                                                                                                         |
| 54      | U+0036  | 6         |                      |                                           |                                                                                                         |
| 55      | U+0037  | 7         |                      |                                           |                                                                                                         |
| 56      | U+0038  | 8         |                      |                                           |                                                                                                         |
| 57      | U+0039  | 9         |                      |                                           |                                                                                                         |
| 58      | U+003A  | :         | colon                | dvojtečka                                 |                                                                                                         |
| 59      | U+003B  | ;         | semicolon            | středník                                  |                                                                                                         |
| 60      | U+003C  | \<        | less than            | menší než                                 |                                                                                                         |
| 61      | U+003D  | \=        | equals               | rovná se                                  |                                                                                                         |
| 62      | U+003E  | \>        | greater than         | větší než                                 |                                                                                                         |
| 63      | U+003F  | ?         | question mark        | otazník                                   |                                                                                                         |
| 64      | U+0040  | @         | at                   | zavináč                                   |                                                                                                         |
| 91      | U+005B  | \[        | left square bracket  | levá hranatá závorka                      |                                                                                                         |
| 92      | U+005C  | \\        | backslash            | zpětné lomítko                            |                                                                                                         |
| 93      | U+005D  | \]        | right square bracket | pravá hranatá závorka                     |                                                                                                         |
| 94      | U+005E  | ^         | caret                | stříška                                   | CIRCUMFLEX ACCENT                                                                                       |
| 95      | U+005F  | _        | underscore           | podtržítko                                |                                                                                                         |
| 96      | U+0060  | \`        | backquote            | zpětný apostrof                           | backtick, GRAVE ACCENT = opačná čárka (gravis)                                                          |
| 97      | U+0061  | a         | a                    | a                                         |                                                                                                         |
| ...     | ...     | ...       | ...                  | ...                                       |                                                                                                         |
| 122     | U+007A  | z         | z                    | z                                         |                                                                                                         |
| 127     | U+007F  | <control> | delete               | (delete)                                  |                                                                                                         |
| 256     |         | 0         | keypad 0             | (0 na numerické klávesnici)               |                                                                                                         |
| 257     |         | 1         | keypad 1             | (1 na numerické klávesnici)               |                                                                                                         |
| 258     |         | 2         | keypad 2             | (2 na numerické klávesnici)               |                                                                                                         |
| 259     |         | 3         | keypad 3             | (3 na numerické klávesnici)               |                                                                                                         |
| 260     |         | 4         | keypad 4             | (4 na numerické klávesnici)               |                                                                                                         |
| 261     |         | 5         | keypad 5             | (5 na numerické klávesnici)               |                                                                                                         |
| 262     |         | 6         | keypad 6             | (6 na numerické klávesnici)               |                                                                                                         |
| 263     |         | 7         | keypad 7             | (7 na numerické klávesnici)               |                                                                                                         |
| 264     |         | 8         | keypad 8             | (8 na numerické klávesnici)               |                                                                                                         |
| 265     |         | 9         | keypad 9             | (9 na numerické klávesnici)               |                                                                                                         |
| 266     |         | .         | keypad period        | (desetinná tečka na numerické klávesnici) |                                                                                                         |
| 267     |         | /         | keypad divide        | (lomítko na numerické klávesnici)         |                                                                                                         |
| 268     |         | \*        | keypad multiply      | (krát na numerické klávesnici)            |                                                                                                         |
| 269     |         | \-        | keypad minus         | (mínus na numerické klávesnici)           |                                                                                                         |
| 270     |         | \+        | keypad plus          | (plus na numerické klávesnici)            |                                                                                                         |
| 271     |         | <control> | keypad enter         | (enter na numerické klávesnici)           |                                                                                                         |
| 272     |         | \=        | keypad equals        | (rovná se na numerické klávesnici)        |                                                                                                         |
| 273     |         |           | up                   | (šipka) nahoru                            |                                                                                                         |
| 274     |         |           | down                 | (šipka) dolů                              |                                                                                                         |
| 275     |         |           | right                | (šipka) vpravo                            |                                                                                                         |
| 276     |         |           | left                 | (šipka) vlevo                             |                                                                                                         |
| 277     |         |           | insert               | (vložit)                                  |                                                                                                         |
| 278     |         |           | home                 | (domů)                                    |                                                                                                         |
| 279     |         |           | end                  | (konec)                                   |                                                                                                         |
| 280     |         |           | page up              | (stránka nahoru)                          |                                                                                                         |
| 281     |         |           | page down            | (stránka dolů)                            |                                                                                                         |
| 282     |         |           | F1                   | F1                                        |                                                                                                         |
| 283     |         |           | F2                   | F2                                        |                                                                                                         |
| 284     |         |           | F3                   | F3                                        |                                                                                                         |
| 285     |         |           | F4                   | F4                                        |                                                                                                         |
| 286     |         |           | F5                   | F5                                        |                                                                                                         |
| 287     |         |           | F6                   | F6                                        |                                                                                                         |
| 288     |         |           | F7                   | F7                                        |                                                                                                         |
| 289     |         |           | F8                   | F8                                        |                                                                                                         |
| 290     |         |           | F9                   | F9                                        |                                                                                                         |
| 291     |         |           | F10                  | F10                                       |                                                                                                         |
| 292     |         |           | F11                  | F11                                       |                                                                                                         |
| 293     |         |           | F12                  | F12                                       |                                                                                                         |
| 294     |         |           | F13                  | F13                                       |                                                                                                         |
| 295     |         |           | F14                  | F14                                       |                                                                                                         |
| 296     |         |           | F15                  | F15                                       |                                                                                                         |
| 300     |         |           | numlock              | (numlock)                                 |                                                                                                         |
| 301     |         |           | capslock             | (capslock)                                |                                                                                                         |
| 302     |         |           | scrollock            | (scrollock)                               |                                                                                                         |
| 303     |         |           | right shift          | (pravý shift)                             |                                                                                                         |
| 304     |         |           | left shift           | (levý shift)                              |                                                                                                         |
| 305     |         |           | right control        | (pravý control)                           |                                                                                                         |
| 306     |         |           | left control         | (levý control)                            |                                                                                                         |
| 307     |         |           | right alt            | (pravý alt)                               |                                                                                                         |
| 308     |         |           | left alt             | (levý alt)                                |                                                                                                         |
| 309     |         |           | right meta           | (pravá meta)                              |                                                                                                         |
| 310     |         |           | left meta            | (levá meta)                               |                                                                                                         |
| 315     |         |           | help                 | (help)                                    |                                                                                                         |
| 316     |         |           | print (-screen)      | (printscreen)                             |                                                                                                         |
| 317     |         |           | system request       | (sysreq)                                  |                                                                                                         |
| 318     |         |           | break                | (break)                                   |                                                                                                         |
| 319     |         |           | menu                 | (menu)                                    |                                                                                                         |
