---
layout: page
title: Slovník s názvy jazyků
permalink: /wiki/Slovníky/Slovník_s_názvy_jazyků/
redirect_from:
  - /Slovník_s_názvy_jazyků/
  - /Jazyky/
parent: Slovníky
---

## České názvy jazyků

Překlady jsou vyexportovány z [Weblate]({% link _pages/wiki/Překladatelský_software/Weblate.md %}), budu rád,
když případné opravy proběhnou i
[tam](https://hosted.weblate.org/projects/weblate/master/cs/). Dalším
zdrojem může být [CLDR]({% link _pages/wiki/Softwarové_knihovny/CLDR.md %}) nebo
[Wikislovník](https://cs.wiktionary.org/wiki/Wikislovn%C3%ADk:Jazyky).

| kód (ISO-639-2/ISO-639-1) | anglicky                 | česky                           |
| ------------------------- | ------------------------ | ------------------------------- |
| ab                        | Abkhazian                | abcházština                     |
| ach                       | Acholi                   | acholština                      |
| aa                        | Afar                     | afarština                       |
| af                        | Afrikaans                | afrikánština                    |
| ak                        | Akan                     | akanština                       |
| sq                        | Albanian                 | albánština                      |
| am                        | Amharic                  | amharština                      |
| anp                       | Angika                   | angičtina                       |
| ar                        | Arabic                   | arabština                       |
| ar_DZ                    | Arabic (Algeria)         | arabština (Alžírsko)            |
| ar_MA                    | Arabic (Morocco)         | arabština (Maroko)              |
| an                        | Aragonese                | aragonština                     |
| hy                        | Armenian                 | arménština                      |
| as                        | Assamese                 | ásámština                       |
| ast                       | Asturian                 | asturština                      |
| de_AT                    | Austrian German          | rakouská němčina                |
| av                        | Avaric                   | avarština                       |
| ae                        | Avestan                  | avestánština                    |
| ay                        | Aymará                   | ajmarština                      |
| az                        | Azerbaijani              | ázerbájdžánština                |
| bm                        | Bambara                  | bambarština                     |
| ba                        | Bashkir                  | baškirština                     |
| eu                        | Basque                   | baskičtina                      |
| bar                       | Bavarian                 | bavorština                      |
| be                        | Belarusian               | běloruština                     |
| be@latin                  | Belarusian (latin)       | běloruština (latinkou)          |
| be_Latn                  | Belarusian (latin)       | běloruština (latinkou)          |
| bem                       | Bemba                    | bemba                           |
| bn                        | Bengali                  | bengálština                     |
| bn_BD                    | Bengali (Bangladesh)     | bengálština (Bangladéš)         |
| bn_IN                    | Bengali (India)          | bengálština (Indie)             |
| bh                        | Bihari languages         | bihárské jazyky                 |
| bi                        | Bislama                  | bislamština                     |
| brx                       | Bodo                     | bodoština                       |
| bs                        | Bosnian                  | bosenština                      |
| bs_Cyrl                  | Bosnian (cyrillic)       | bosenština (cyrilicí)           |
| bs_Latn                  | Bosnian (latin)          | bosenština (latinkou)           |
| br                        | Breton                   | bretonština                     |
| bg                        | Bulgarian                | bulharština                     |
| my                        | Burmese                  | barmština                       |
| ca                        | Catalan                  | katalánština                    |
| km                        | Central Khmer            | khmerština                      |
| ch                        | Chamorro                 | chamorro                        |
| ce                        | Chechen                  | čečenština                      |
| chr                       | Cherokee                 | čerokézština                    |
| hne                       | Chhattisgarhi            | čhattísgarhčina                 |
| cgg                       | Chiga                    | rukiga                          |
| zh                        | Chinese                  | čínština                        |
| zh_CN                    | Chinese (China)          | čínština (Čína)                 |
| zh_HK                    | Chinese (Hong Kong)      | čínština (Hongkong)             |
| zh_Hant_HK              | Chinese (Hong Kong)      | čínština (Hongkong)             |
| zh_Hans                  | Chinese (Simplified)     | čínština (zjednodušená)         |
| zh_TW                    | Chinese (Taiwan)         | čínština (Taiwan)               |
| zh_Hant                  | Chinese (Traditional)    | čínština (tradiční)             |
| cv                        | Chuvash                  | čuvaština                       |
| ksh                       | Colognian                | ripuariánština                  |
| kw                        | Cornish                  | kornština                       |
| co                        | Corsican                 | korsičtina                      |
| cr                        | Cree                     | kríjština                       |
| crh                       | Crimean Tatar            | krymská tatarština              |
| hr                        | Croatian                 | chorvatština                    |
| cs                        | Czech                    | čeština                         |
| da                        | Danish                   | dánština                        |
| da_DK                    | Danish                   | dánština                        |
| dv                        | Dhivehi                  | dhivehi                         |
| doi                       | Dogri                    | dógrí                           |
| nl                        | Dutch                    | nizozemština                    |
| nl_BE                    | Dutch (Belgium)          | nizozemština (Belgie)           |
| dz                        | Dzongkha                 | dzongkä                         |
| en                        | English                  | angličtina                      |
| en_AU                    | English (Australia)      | angličtina (Austrálie)          |
| en_CA                    | English (Canada)         | angličtina (Kanada)             |
| en_IE                    | English (Ireland)        | angličtina (Irsko)              |
| en_PH                    | English (Philippines)    | angličtina (Filipíny)           |
| en_ZA                    | English (South Africa)   | angličtina (Jižní Afrika)       |
| en_GB                    | English (United Kingdom) | angličtina (Spojené království) |
| en_US                    | English (United States)  | angličtina (Spojené státy)      |
| eo                        | Esperanto                | esperanto                       |
| et                        | Estonian                 | estonština                      |
| ee                        | Ewe                      | eveština                        |
| fo                        | Faroese                  | faerština                       |
| fj                        | Fijian                   | fidžijština                     |
| fil                       | Filipino                 | filipínština                    |
| fi                        | Finnish                  | finština                        |
| frp                       | Franco-Provençal         | franko-provensálština           |
| fr                        | French                   | francouzština                   |
| fr_FR                    | French                   | francouzština                   |
| fr_CA                    | French (Canada)          | francouzština (Kanada)          |
| fy                        | Frisian                  | fríština                        |
| fur                       | Friulian                 | furlanština                     |
| ff                        | Fulah                    | fulbština                       |
| gd                        | Gaelic                   | gaelština                       |
| gl                        | Galician                 | galicijština                    |
| lg                        | Ganda                    | gandština                       |
| ka                        | Georgian                 | gruzínština                     |
| de                        | German                   | němčina                         |
| de_DE                    | German                   | němčina                         |
| ki                        | Gikuyu                   | kikujština                      |
| el                        | Greek                    | řečtina                         |
| kl                        | Greenlandic              | grónština                       |
| gn                        | Guarani                  | guaranština                     |
| gu                        | Gujarati                 | gudžarátština                   |
| gun                       | Gun                      | fonština                        |
| ht                        | Haitian                  | haitština                       |
| ha                        | Hausa                    | hauština                        |
| haw                       | Hawaiian                 | havajština                      |
| he                        | Hebrew                   | hebrejština                     |
| hz                        | Herero                   | hererština                      |
| hil                       | Hiligaynon               | hiligaynon                      |
| hi                        | Hindi                    | hindština                       |
| ho                        | Hiri Motu                | hiri motu                       |
| hu                        | Hungarian                | maďarština                      |
| hu_HU                    | Hungarian                | maďarština                      |
| is                        | Icelandic                | islandština                     |
| io                        | Ido                      | ido                             |
| ig                        | Igbo                     | igboština                       |
| id                        | Indonesian               | indonéština                     |
| in                        | Indonesian               | indonéština                     |
| ia                        | Interlingua              | interlingua                     |
| iu                        | Inuktitut                | inuitština                      |
| ik                        | Inupiaq                  | inupiaq                         |
| ga                        | Irish                    | irština                         |
| it                        | Italian                  | italština                       |
| ja                        | Japanese                 | japonština                      |
| ja_JP                    | Japanese                 | japonština                      |
| jv                        | Javanese                 | javánština                      |
| kab                       | Kabyle                   | kabylština                      |
| kn                        | Kannada                  | kannadština                     |
| kr                        | Kanuri                   | kanurijština                    |
| ks                        | Kashmiri                 | kašmírština                     |
| csb                       | Kashubian                | kašubština                      |
| kk                        | Kazakh                   | kazaština                       |
| rw                        | Kinyarwanda              | rwandština                      |
| tlh                       | Klingon                  | klingonština                    |
| tlh-qaak                  | Klingon (pIqaD)          | klingonština (pIqaD)            |
| kv                        | Komi                     | komijština                      |
| kg                        | Kongo                    | konžština                       |
| kok                       | Konkani                  | konkánština                     |
| ko                        | Korean                   | korejština                      |
| ku                        | Kurdish                  | kurdština                       |
| kmr                       | Kurmanji                 | kurmándží                       |
| kj                        | Kwanyama                 | kuanyama                        |
| ky                        | Kyrgyz                   | kyrgyzština                     |
| lo                        | Lao                      | laoština                        |
| la                        | Latin                    | latina                          |
| lv                        | Latvian                  | lotyština                       |
| li                        | Limburgish               | limburština                     |
| ln                        | Lingala                  | ngalština                       |
| lt                        | Lithuanian               | litevština                      |
| jbo                       | Lojban                   | lojban                          |
| nds                       | Low German               | dolnoněmčina                    |
| dsb                       | Lower Sorbian            | dolnolužičtina                  |
| lu                        | Luba-Katanga             | lubština                        |
| lb                        | Luxembourgish            | lucemburština                   |
| mk                        | Macedonian               | makedonština                    |
| mai                       | Maithili                 | maithilština                    |
| mg                        | Malagasy                 | malgaština                      |
| ms                        | Malay                    | malajština                      |
| ml                        | Malayalam                | malajálamština                  |
| mt                        | Maltese                  | maltština                       |
| mnk                       | Mandinka                 | mandinka                        |
| mni                       | Manipuri                 | manipurí                        |
| gv                        | Manx                     | manština                        |
| mi                        | Maori                    | maorština                       |
| arn                       | Mapudungun               | araukánština                    |
| mr                        | Marathi                  | maráthština                     |
| chm                       | Mari                     | marijština                      |
| mh                        | Marshallese              | maršálština                     |
| mhr                       | Meadow Mari              | lužnomarijština                 |
| mo                        | Moldovan                 | moldavština                     |
| mn                        | Mongolian                | mongolština                     |
| me                        | Montenegrin              | černohorština                   |
| mfe                       | Morisyen                 | mauretánština                   |
| nqo                       | N'Ko                     | n'ko                            |
| nah                       | Nahuatl languages        | aztéčtina                       |
| na                        | Nauru                    | naurština                       |
| nv                        | Navaho                   | navažština                      |
| ng                        | Ndonga                   | ndondština                      |
| nap                       | Neapolitan               | neapolština                     |
| ne                        | Nepali                   | nepálština                      |
| nd                        | North Ndebele            | severní ndebelština             |
| se                        | Northern Sami            | severní sámština                |
| no                        | Norwegian (old code)     | norština (staré označení)       |
| no_NO                    | Norwegian (old code)     | norština (staré označení)       |
| nb                        | Norwegian Bokmål         | norština (Bokmål)               |
| nb_NO                    | Norwegian Bokmål         | norština (Bokmål)               |
| nn                        | Norwegian Nynorsk        | norština (Nynorsk)              |
| ii                        | Nuosu                    | ijo                             |
| ny                        | Nyanja                   | čičevština                      |
| ie                        | Occidental               | occidental                      |
| oc                        | Occitan                  | okcitánština                    |
| or                        | Odia                     | urijština                       |
| oj                        | Ojibwe                   | odžibvejština                   |
| cu                        | Old Church Slavonic      | staroslověnština                |
| om                        | Oromo                    | oromština                       |
| os                        | Ossetian                 | osetština                       |
| pi                        | Pali                     | páli                            |
| pap                       | Papiamento               | papiamento                      |
| nso                       | Pedi                     | severní sotština                |
| fa                        | Persian                  | perština                        |
| pms                       | Piemontese               | piemontština                    |
| pr                        | Pirate                   | pirátština                      |
| pl                        | Polish                   | polština                        |
| pt                        | Portuguese               | portugalština                   |
| pt_BR                    | Portuguese (Brazil)      | portugalština (Brazílie)        |
| pt_PT                    | Portuguese (Portugal)    | portugalština (Portugalsko)     |
| pa                        | Punjabi                  | paňdžábština                    |
| ps                        | Pushto                   | paštština                       |
| qu                        | Quechua                  | kečuánština                     |
| ro                        | Romanian                 | rumunština                      |
| ro_RO                    | Romanian                 | rumunština                      |
| rm                        | Romansh                  | rétorománština                  |
| rn                        | Rundi                    | rundi                           |
| ru                        | Russian                  | ruština                         |
| rue                       | Rusyn                    | rusínština                      |
| sm                        | Samoan                   | samojština                      |
| sg                        | Sango                    | sangoština                      |
| sa                        | Sanskrit                 | sanskrt                         |
| sat                       | Santali                  | santálština                     |
| sc                        | Sardinian                | sardinština                     |
| sco                       | Scots                    | skotština                       |
| sr                        | Serbian                  | srbština                        |
| sr@cyrillic               | Serbian (cyrillic)       | srbština (cyrilicí)             |
| sr_RS@cyrillic           | Serbian (cyrillic)       | srbština (cyrilicí)             |
| sr_Cyrl                  | Serbian (cyrillic)       | srbština (cyrilicí)             |
| sr@latin                  | Serbian (latin)          | srbština (latinkou)             |
| sr_RS@latin              | Serbian (latin)          | srbština (latinkou)             |
| sr_Latn                  | Serbian (latin)          | srbština (latinkou)             |
| sh                        | Serbo-Croatian           | srbochorvatština                |
| shn                       | Shan                     | šanština                        |
| sn                        | Shona                    | šonština                        |
| szl                       | Silesian                 | slezština                       |
| sd                        | Sindhi                   | sindhština                      |
| si                        | Sinhala                  | sinhálština                     |
| sk                        | Slovak                   | slovenština                     |
| sl                        | Slovenian                | slovinština                     |
| so                        | Somali                   | somálština                      |
| son                       | Songhai languages        | songhaiština                    |
| ckb                       | Sorani                   | sorání                          |
| wen                       | Sorbian                  | lužičtina                       |
| st                        | Sotho                    | jižní sotština                  |
| nr                        | South Ndebele            | jižní ndebelština               |
| sma                       | Southern Sami            | jižní sámština                  |
| es                        | Spanish                  | španělština                     |
| es_                      | Spanish                  | španělština                     |
| es_CO                    | Spanish                  | španělština                     |
| es_ES                    | Spanish                  | španělština                     |
| es_US                    | Spanish (American)       | španělština (Amerika)           |
| es_AR                    | Spanish (Argentina)      | španělština (Argentina)         |
| es_CL                    | Spanish (Chile)          | španělština (Chile)             |
| es_MX                    | Spanish (Mexico)         | španělština (Mexiko)            |
| es_PR                    | Spanish (Puerto Rico)    | španělština (Portoriko)         |
| su                        | Sundanese                | sundánština                     |
| sw                        | Swahili                  | svahilština                     |
| ss                        | Swati                    | svazijština                     |
| sv                        | Swedish                  | švédština                       |
| de_CH                    | Swiss High German        | švýcarská němčina               |
| tl                        | Tagalog                  | tagalština                      |
| ty                        | Tahitian                 | tahitština                      |
| tg                        | Tajik                    | tádžičtina                      |
| ta                        | Tamil                    | tamilština                      |
| tt                        | Tatar                    | tatarština                      |
| te                        | Telugu                   | telužština                      |
| th                        | Thai                     | thajština                       |
| bo                        | Tibetan                  | tibetština                      |
| tig                       | Tigre                    | tigre                           |
| ti                        | Tigrinya                 | tigriňa                         |
| to                        | Tonga (Tonga Islands)    | tongánština (ostrovy Tonga)     |
| ts                        | Tsonga                   | tsonga                          |
| tn                        | Tswana                   | tswanština                      |
| tr                        | Turkish                  | turečtina                       |
| tk                        | Turkmen                  | turkmenština                    |
| tw                        | Twi                      | ťwiština                        |
| ug                        | Uighur                   | ujgurština                      |
| uk                        | Ukrainian                | ukrajinština                    |
| hsb                       | Upper Sorbian            | hornolužičtina                  |
| ur                        | Urdu                     | urdština                        |
| ur_PK                    | Urdu (Pakistan)          | urdština (Pákistán)             |
| uz                        | Uzbek                    | uzbečtina                       |
| uz@latin                  | Uzbek (latin)            | uzbečtina (latinkou)            |
| uz_Latn                  | Uzbek (latin)            | uzbečtina (latinkou)            |
| ca@valencia               | Valencian                | valencijština                   |
| ve                        | Venda                    | vendština                       |
| vec                       | Venetian                 | benátština                      |
| vi                        | Vietnamese               | vietnamština                    |
| vi_VN                    | Vietnamese               | vietnamština                    |
| vo                        | Volapük                  | volapük                         |
| wa                        | Walloon                  | valonština                      |
| wae                       | Walser German            | walserská němčina               |
| cy                        | Welsh                    | velština                        |
| vls                       | West Flemish             | západní vlámština               |
| wo                        | Wolof                    | volofština                      |
| xh                        | Xhosa                    | xhoština                        |
| sah                       | Yakut                    | jakutština                      |
| yi                        | Yiddish                  | jidiš                           |
| yo                        | Yoruba                   | jorubština                      |
| yue                       | Yue                      | jüe                             |
| za                        | Zhuang                   | čuangština                      |
| zu                        | Zulu                     | zulština                        |

## Názvy jazyků v daném jazyce

  - [Curated List of Languages -
    Firefox](https://mozilla-l10n.github.io/firefox-languages/)
  - [Complete Languages Analysis -
    Firefox](https://mozilla-l10n.github.io/firefox-languages/complete.html)
