---
layout: page
title: Slovníky
permalink: /wiki/Slovníky/
redirect_from:
  - /Překladatelské_slovníky/
  - /Kategorie:Slovníky/
has_children: true
nav_order: 3
---

{% include l10n-cz/table-of-contents.html %}

Odkazy na rozličné druhy online slovníků najdete na [samostatné
stránce]({% link _pages/wiki/Pomůcky_pro_překladatele.md %}). Níže jsou uvedeny odkazy
na slovníky či terminologické databáze jednotlivých překladatelských
týmů, či na slovníky obecně ze světa otevřeného i uzavřeného softwaru.
V rámci projektu L10N.cz je udržován také náš [společný překladatelský
slovník]({% link _pages/wiki/Slovníky/Překladatelský_slovník.md %}).

Zcela dole pak najdete slovníky na této wiki.

### Slovníky jednotlivých překladatelských týmů

  - [Drupal]({% link _pages/wiki/Drupal/index.md %})
  - [Fedora](https://wiki.mojefedora.cz/doku.php?id=navody:slovnik)
  - [GNOME](https://wiki.gnome.org/Czech/PrekladatelskySlovnik),
    [Slovník vizuálních prvků
    GNOME](https://live.gnome.org/Czech/JakPrekladat/SlovnikVizualnichPrvku)
  - [KDE](https://web.archive.org/web/https://l10n.kde.org/docs/visualdict/dict.html)
  - [LibreOffice](https://wiki.documentfoundation.org/CS/P%C5%99ekladatelsk%C3%BD_slovn%C3%ADk)
  - [Mozilla](https://www.mozilla.cz/zapojte-se/lokalizace/prekladovy-slovnik/)
  - [Ubuntu](https://wiki.ubuntu.cz/Lokalizace/P%C5%99ekladatelsk%C3%BD%20slovn%C3%ADk)
  - [WordPress](https://docs.google.com/spreadsheets/d/1OvvC4pcpUnE_cb67b2O8KhEbCz9au6TKH3WGc13VH0g/edit)
    (Google dokument)
  - [WordPress](https://translate.wordpress.org/locale/cs/default/glossary)
    (na translate.wordpress.org)

### Slovníky dalších projektů

  - [Facebook Translate
    Glossary](https://www.facebook.com/?sk=trans_glossary)
  - [Jabber](https://www.jabber.cz/wiki/P%C5%99eklady)
  - [Microsoft - International Word
    List](https://web.archive.org/web/20041211055336/http://msdn.microsoft.com/library/en-us/dnwue/html/CZE_word_list.htm)
  - [Microsoft Language
    Portal](https://www.microsoft.com/en-us/language/)
  - [OpenERP](https://docs.google.com/spreadsheets/d/1BOibrcT3Hvad2qKcpMSBxarsmzw-Wyl_xovvjYv8Z3o/edit#gid=0)
  - [Terminologická databáze Pavla
    Rychlého](https://www.fi.muni.cz/~pary/term/term.html)
