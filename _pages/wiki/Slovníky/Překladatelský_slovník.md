---
layout: page
title: Překladatelský slovník
permalink: /wiki/Slovníky/Překladatelský_slovník/
redirect_from:
  - /Překladatelský_slovník/
parent: Slovníky
---

{% include l10n-cz/table-of-contents.html %}

Při překladech je důležitá konzistence termínů. K tomu vám pomůže tento
překladatelský slovník, který shrnuje ustálené anglické překlady. Pokud
si nejste jisti nějakým zde neuvedeným překladem, či s nějakým překladem
nesouhlasíte, proberte to v naší [poštovní
konferenci]({% link _pages/kontakty.md %}).

# Pravidla pro diskuzi o slovníku

***Tato pravidla jsou zatím ve fázi návrhu.***

Většina komunikace L10N.cz probíhá v naší [poštovní
konferenci]({% link _pages/kontakty.md %}). Pro účel diskuze
o terminologii v překladovém slovníku jsme si stanovili následující
pravidla:

  - Aby diskuze, kde nedojdeme ke shodě, netrvaly věčně, po měsíci nebo
    delší odmlce se ujme slova jeden z moderátorů.
  - Moderátor vybere z diskuze návrhy, které ještě mají podporu, a nechá
    o nich hlasovat.
  - Výsledek diskuze a hlasování, ať už vítězný návrh nebo informaci o
    neshodě, moderátor zanese do slovníku s odkazem na danou diskuzi.
  - Překladatelský slovník platí pouze jako doporučení pro jednotlivé
    týmy. Ty se jej nemusí držet a mohou si klidně udržovat vlastní
    slovník pro svůj projekt.

Na ostatní diskuze nebo dotazy se tato pravidla samozřejmě nevztahují.

**Zbývá dořešit:**

  - vybrat moderátory (ideálně co nejvíce napříč projekty a někoho
    jiného, než kdo už je koordinátorem některého z týmů, ať není
    všechno jenom na nich)
  - zvolit vhodnou maximální dobu trvání diskuze
  - určit dobu a pravidla pro hlasování
  - domluvit pravidelná setkání na IRC/Matrixu

***Tato pravidla pro komunikaci a diskuze jsou zatím ve fázi návrhu.***

# Speciální stránky

  - [Slovník vizuálních prvků]({% link _pages/wiki/Slovníky/Slovník_vizuálních_prvků.md %})
  - [Slovník s názvy kláves]({% link _pages/wiki/Slovníky/Slovník_s_názvy_kláves.md %})
  - [Slovník s názvy států]({% link _pages/wiki/Slovníky/Slovník_s_názvy_států.md %})
  - [Slovník s názvy jazyků]({% link _pages/wiki/Slovníky/Slovník_s_názvy_jazyků.md %})
  - [Překladatelské slovníky jednotlivých
    týmů]({% link _pages/wiki/Slovníky/index.md %})
  - [Terminologická a překladová
    paměť]({% link _pages/wiki/Slovníky/Terminologická_a_překladová_paměť.md %})
    automaticky generovaná z tohoto slovníku

# A

  - <u>A</u>bout (název aplikace)...
    O <u>a</u>plikaci (název)...
    O (název, 6. pád)... (jen pokud je skloňování názvu u dané aplikace
    vhodné)
    ([diskuze 2009](https://lists.l10n.cz/pipermail/diskuze/2009-March/000188.html),
    [diskuze 2016](https://lists.l10n.cz/pipermail/diskuze/2016-December/002334.html))
  - addon / add-on
    doplněk
  - address bar
    adresní řádek
  - administrator
    správce
  - administrate
    spravovat
  - always on top
    vždy navrchu
  - all-in-one
    zařízení vše v jednom
    multifunkční zařízení (v případě tiskáren se skenerem)
  - applet
    applet
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-March/000131.html))
  - application
    aplikace
  - apply
    použít
    uplatnit
  - Are you sure...
    Opravdu chcete...
    Opravdu si přejete...
    \! určitě ne: Jste si jisti... \!
    ([diskuze 2016](https://lists.l10n.cz/pipermail/diskuze/2016-December/002347.html))
  - archive
    archiv
  - argument (command line; argument je to, co se funkci/příkazu
    předává; viz
    [Quora](https://www.quora.com/What-is-the-difference-between-an-argument-and-a-parameter))
    argument
    ([diskuze 2009](https://lists.l10n.cz/pipermail/diskuze/2009-June/000713.html),
    [diskuze 2016](https://lists.l10n.cz/pipermail/diskuze/2016-December/002336.html))

# B

  - back
    zpět (prohlížeč)
  - backend (v závislosti na kontextu\!)
    [v diskuzi, následují
    návrhy](https://lists.l10n.cz/pipermail/diskuze/2009-May/000498.html)
    podpůrná vrstva
    konec období nebo procesu; konec; část něčeho, která je za tou
    částí, kterou lze spatřit (pozadí)
    jádro
    server
    serverová část
    výkonná část
    implementace
  - backtrace
    [diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-May/000570.html)
  - binary
    binární soubor, dvojková/binární soustava
    dvojkový, binární
  - bitrate
    datový tok (pro multimédia, např. údaj u MP3)
    přenosová rychlost (pro komunikace, např. rychlost spojení přes
    modem)
  - bold
    tučné, tučně
  - bookmark
    záložka
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-May/000538.html))
  - branch
    větev
    vývojová větev
  - browser
    prohlížeč
  - buffer
    vyrovnávací paměť
  - bug report
    hlášení o chybě
    ([diskuze 2016](https://lists.l10n.cz/pipermail/diskuze/2016-December/002337.html))
  - bugtracker / bug tracker
    systém pro sledování chyb
  - burn
    vypálit
  - by default
    standardně
    ve výchozím nastavení
  - byte
    bajt

# C

  - cache
    mezipaměť
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-June/000610.html))
  - cancel
    zrušit
  - canonize
    kanonizovat, převést na kanonický tvar
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2014-November/002209.html))
  - carrier
    nosný signál (u sítí)
  - clear
    vymazat
    vyčistit (odebrat všechny položky ze seznamu, celý obsah vstupního
    pole, případně nějaké plochy)
    ([diskuze 2009](https://lists.l10n.cz/pipermail/diskuze/2009-June/000609.html),
    [diskuze 2016](https://lists.l10n.cz/pipermail/diskuze/2016-December/002339.html))
  - click
    [zatím v
    diskuzi](https://lists.l10n.cz/pipermail/diskuze/2009-March/000199.html)
  - copy
    kopírovat
  - component
    součást
  - concept
    pojetí
    pojem
  - configuration
    nastavení
    ([dřívější
    diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-June/000713.html))
  - configure settings
    upravit nastavení
  - console
    konzole
    [dřívější
    diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-March/000128.html)
  - context menu
    místní nabídka
    ([diskuze 2016](https://lists.l10n.cz/pipermail/diskuze/2016-December/002356.html))
  - channel
    kanál
  - chat
    chat
    ([diskuze 2009](https://lists.l10n.cz/pipermail/diskuze/2009-June/000669.html),
    [diskuze 2016](https://lists.l10n.cz/pipermail/diskuze/2016-December/002357.html))
  - close
    zavřít (okno)
  - cluster
    cluster, klastr
    *Poznámka:* V současnosti je spisovná i počeštěná varianta klastr.
    Ačkoliv v jiných oborech se používá zcela běžně, v IT příliš
    rozšířená není. Proto použití té či oné varianty záleží na
    zvážení překladatele.
  - create
    vytvořit
  - cursor
    kurzor
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2010-May/001523.html))
  - cut
    vyjmout

# D

  - debug
    ladit
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-May/000570.html))
  - default
    výchozí
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-February/000117.html))
    předem nastavený
    standardní (= na obvyklé, běžné úrovni; ustálená míra, podoba
    něčeho)
  - delete
    smazat (soubor, záložku; trvale a nenávratně)
    ([diskuze 2016](https://lists.l10n.cz/pipermail/diskuze/2016-December/002339.html))
  - derivative
    odvozenina
  - desktop
    stolní počítač (typ počítače, např. ve vztahu k serveru)
    pracovní prostředí (GNOME, KDE apod.)
    pracovní plocha, plocha (plocha s ikonami)
  - deploy/deployment
    nasadit/nasazení (učinit produkčním)
  - device
    zařízení
  - directory
    adresář
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2010-February/001308.html))
  - disable
    zakázat (zdroje softwaru), vypnout...
  - display
    displej (přesná terminologie v souvislosti s X serverem)
    obrazovka (obecný netechnický význam)
    [diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-March/000439.html)
  - display manager
    správce displeje
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-March/000439.html))
  - Do you want to...
    Přejete si...
    Chcete...
    ([diskuze 2016](https://lists.l10n.cz/pipermail/diskuze/2016-December/002347.html))
  - Do you really want to...
    Opravdu chcete...
    Opravdu si přejete...
    ([diskuze 2016](https://lists.l10n.cz/pipermail/diskuze/2016-December/002347.html))
  - download
    stáhnout
  - drag'n'drop, drag and drop, D'n'D
    přetáhnout
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2010-November/001825.html))
  - drop
    [zatím v
    diskuzi](https://lists.l10n.cz/pipermail/diskuze/2009-June/000609.html)
  - dump
    výpis, vypsat...
  - demuxer
    demultiplexer

# E

  - <u>E</u>dit
    <u>U</u>pravit (jako běžné slovo i v nabídce;
    [diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-March/000163.html))
  - element
    prvek
  - email
    e-mail
  - empty
    vyprázdnit
    vymazat
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-June/000609.html))
  - enable
    povolit, příp. zapnout či odblokovat
  - encoder
    kodér
  - engine
    [v
    diskuzi](https://lists.l10n.cz/pipermail/diskuze/2009-May/000498.html)
  - exit code
    návratový kód

# F

  - factory
    generátor (překlad pro běžného uživatele)
    tovární třída (programátorský překlad)
    Poznámka: Dané překlady jsou myšleny v kontextu vět typu *Object
    factory error*. Tyto fráze by se ideálně v hláškách pro běžné
    uživatele neměly vůbec vyskytovat, neboť je to "programátorština".
    Vhodné je u vývojáře požádat o pro uživatele intuitivnější hlášku.
    [Diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-March/000130.html).
  - fallback
    záloha
  - favorites
    oblíbené
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-May/000538.html))
  - feature
    vlastnost
  - find (next)
    najít (další)
  - file a bug report
    nahlásit chybu
    podat hlášení o chybě
    ([diskuze 2016](https://lists.l10n.cz/pipermail/diskuze/2016-December/002337.html))
  - focus
    zaměření
    zaměřit
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2010-December/001841.html))
  - folder
    složka
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2010-February/001308.html))
  - font
    písmo
  - fork
    rozvětvení
  - forward
    vpřed (prohlížeč, opak "zpět")
    další (průvodce instalací/wizard)
    přeposlat (emailový klient, přeposlat zprávu)
  - framework
    aplikační rámec ([nebo
    nepřekládat](https://cs.wikipedia.org/wiki/Framework))
  - free software
    svobodný software
  - frontend
    [v
    diskuzi](https://lists.l10n.cz/pipermail/diskuze/2009-May/000498.html)

# G

  - getting started
    začínáme
  - go
    přejít
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-April/000472.html))

# H

  - hardware
    hardware \[pozor na správné skloňování, 2. pád: hardwaru - nikoliv
    hardware\]
  - hardlink / hard link
    pevný odkaz
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2012-February/002003.html))
  - help
    nápověda
  - hibernate
    uspat na disk
  - hide
    skrýt
  - HOWTO ...
    Jak (na) ...
    Návod
  - hyperlink
    hypertextový odkaz
    odkaz

# I

  - icon
    ikona
  - input
    vstup
  - install
    nainstalovat
  - interface
    rozhraní
  - Internet
    1\. internet *(propojené počítačové sítě)* = komunikační prostředí
    globální počítačové sítě (Internetu); na tom založený celosvětový
    komunikační prostředek, médium: internet opět míří k výšinám;
    kdybych se narodil později, surfoval bych dnes taky na internetu;
    vyhledat údaje na internetu; stáhnout z internetu
    2\. Internet *vlastní jméno (Celosvětová informační a komunikační
    síť)*

<!-- end list -->

  -
    Poznámka: Malé písmeno patří do trojice pojmů intranet – extranet –
    internet. Také ve významu „komunikační médium“ se stále častěji píše
    malé písmeno: informace najdete na internetu; první seznámení s
    internetem; nabízíme připojení k internetu. Pokud jde o velké
    písmeno, mělo by vyznačovat vlastní název jedinečného produktu.
    Počítačoví odborníci však nejsou zajedno v tom, zda internet
    opravdu vznikl jako jedinečný produkt. Kromě toho i názvy
    jedinečných produktů časem přecházejí v názvy obecné a píšou se s
    malým písmenem (sýr Niva – chlebíčky s nivou apod.).
  - insert
    vložit
  - italic
    kurzíva

# J

# K

  - keyboard shortcut / keyboard short-cut
    klávesová zkratka

# L

  - launcher
    spouštěč
  - line
    čára
    řádek
    řádka
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2010-March/001336.html))
  - Loading, please wait... (?)
    Načítá se, prosím čekejte
  - locale
    národní prostředí (hovoří-li se obecně - o nastavení formátu data
    nebo měny pro celý systém, aplikaci, uživatelský účet apod.)
    lokalizace
    ([diskuze 2016](https://lists.l10n.cz/pipermail/diskuze/2016-December/002344.html),
    [Wikipedia](https://cs.wikipedia.org/wiki/Locale))
  - log
    protokol
    zapsat (zapisovat) do protokolu
  - log off
    odhlásit (se)
  - login photo
    přihlašovací fotografie
  - lower
    odsunout do pozadí, do pozadí *(okno)*
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2011-January/001861.html))
  - live cd
    spustitelné CD

# M

  - mailing list
    poštovní konference
    e-mailová konference
    elektronická konference
  - mark
    označit
  - maximize (a window)
    maximalizovat (okno)
  - menu
    nabídka
  - metadata
    metadata
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2010-April/001411.html))
  - minimize (a window)
    minimalizovat (okno)
  - mode
    režim
  - modified
    změněno
  - monitor
    monitor
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-March/000439.html))
    sledovat
  - most easy
    nejsnazší (podle PČP povoleno i *nejsnadnější*, ale *nejsnazší* je
    dle vyhledávačů násobně používanější a zažitější v \[písemných\]
    sděleních)
  - mount
    připojit
  - move (here)
    přesunout (sem)
  - mute (multimedia)
    [v
    diskuzi](https://lists.l10n.cz/pipermail/diskuze/2009-May/000509.html)
  - muxer
    multiplexer

# N

  - name
    název (souboru, počítače...)
    jméno (osoby)
  - new...
    nový...
  - next
    následující
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2010-May/001494.html))
  - non-free
    nesvobodný

# O

  - open
    otevřít
  - open source software
    software s otevřeným zdrojovým kódem
    otevřený software
  - open stack
    ??
  - option
    možnost
    přepínač (obecně v příkazovém řádku)
    volba (command line option)
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-June/000713.html))
  - overlay
    překryv

# P

  - package
    balík
  - panel
    panel
  - parameter (command line; parametr je část definice funkce,
    pojmenovává proměnné sloužící k přístupu k hodnotě argumentu uvnitř
    programu; viz
    [Quora](https://www.quora.com/What-is-the-difference-between-an-argument-and-a-parameter))
    parametr
    ([diskuze 2009](https://lists.l10n.cz/pipermail/diskuze/2009-June/000713.html),
    [diskuze 2016](https://lists.l10n.cz/pipermail/diskuze/2016-December/002336.html))
  - parse
    analyzovat
  - parser
    syntaktický analyzátor
  - partition
    oddíl
  - partitioning menu
    nabídka pro rozdělení
  - paste
    vložit
  - password
    heslo
  - pause (multimedia)
    [v
    diskuzi](https://lists.l10n.cz/pipermail/diskuze/2009-May/000509.html)
  - permission
    oprávnění
  - pin
    *(n)* připínáček
    *(v)* připnout
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2010-June/001657.html))
  - plain text
    prostý text
  - play (multimedia)
    přehrát
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-May/000509.html))
  - plugin / plug-in
    zásuvný modul
  - pointer
    ukazatel
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2010-May/001523.html))
  - policy
    zásada
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2011-January/001850.html))
  - postprocessing
    [v
    diskuzi](https://lists.l10n.cz/pipermail/diskuze/2009-June/000600.html)
  - preferences
    *(obvykle v plurálu)* předvolby
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-June/000713.html))
  - previous
    předchozí
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2010-May/001494.html))
  - probe
    sonda
    ([git](https://sourceware.org/git/gitweb.cgi?p=systemtap.git;a=tree;f=man/cs))
  - probe point
    sondážní bod
  - program
    program
  - prompt
    příkazový řádek
  - properties
    *(obvykle v plurálu)* vlastnosti
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-June/000713.html))
  - proprietary
    [v
    diskuzi](https://lists.l10n.cz/pipermail/diskuze/2009-August/000760.html)

# Q

  - quit
    ukončit

# R

  - raise
    přenést do popředí, do popředí
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2011-January/001861.html))
  - really
    opravdu
  - realm (Kerberos)
    sféra
  - region
    oblast
  - release
    *(software release)* vydání
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2010-July/001660.html))
  - reload
    obnovit
  - remove
    odstranit (soubor, aplikaci)
    odebrat (položku ze seznamu)
    ([diskuze 2009](https://lists.l10n.cz/pipermail/diskuze/2009-June/000606.html),
    [diskuze 2016](https://lists.l10n.cz/pipermail/diskuze/2016-December/002339.html))
  - removable media
    výměnná média
  - report
    hlášení / nahlásit
    sestava
    ([diskuze 2016](https://lists.l10n.cz/pipermail/diskuze/2016-December/002337.html))
  - repository
    repozitář
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2011-February/001885.html))
  - resource
    zdroj
    prostředek
    ([diskuze 2016](https://lists.l10n.cz/pipermail/diskuze/2016-December/002350.html))
  - restricted formats
    vyhrazené formáty
  - revoke
    odvolat (certifikát, klíč...)
  - rip
    [zatím v
    diskuzi](https://lists.l10n.cz/pipermail/diskuze/2009-June/000623.html)
  - root
    kořen *(file system root, root directory)*
    root *(konkrétní název účtu)*
    superuživatel *(obecně účet s nejvyššími oprávněními)*
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2011-March/001950.html))

# S

  - screen
    obrazovka
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-March/000439.html))
  - screensaver
    šetřič obrazovky
  - server
    server
  - session
    sezení
  - settings
    *(obvykle v plurálu)* nastavení
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-June/000713.html))
  - setup
    nastavení, nastavit
  - sheet
    list (např. v tabulkovém kalkulátoru)
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-May/000538.html))
  - shell
    shell
  - side- (bar, pane)
    postranní
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2011-January/001859.html))
  - sidebar
    [v
    diskuzi](https://lists.l10n.cz/pipermail/diskuze/2009-May/000564.html)
  - skin
    motiv vzhledu
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2013-April/002125.html))
  - skip
    přeskočit
  - smart card / smartcard
    čipová karta
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2010-October/001798.html))
  - smooth scroll
    plynulé posouvání
  - snippet
    úryvek
  - software
    software (pozor na správné
    [skloňování](https://prirucka.ujc.cas.cz/?slovo=software), 2.
    pád: softwaru - nikoliv software)
  - software source
    zdroj softwaru
  - source
    zdroj, zdrojový kód
  - splash screen
    spouštěcí obrazovka
  - spreadsheet / spread sheet (dokument)
    tabulkový sešit
    sešit
    ([diskuze 2009](https://lists.l10n.cz/pipermail/diskuze/2009-May/000538.html),
    [diskuze 2016](https://lists.l10n.cz/pipermail/diskuze/2016-December/002335.html))
  - spreadsheet / spread sheet (aplikace jako LO Calc, Gnumeric, ...)
    tabulkový procesor
    ([diskuze 2009](https://lists.l10n.cz/pipermail/diskuze/2009-May/000538.html),
    [diskuze 2016](https://lists.l10n.cz/pipermail/diskuze/2016-December/002335.html))
  - stack
    zásobník
  - statusbar / status bar
    [v
    diskuzi](https://lists.l10n.cz/pipermail/diskuze/2009-May/000564.html)
  - stream
    proud
    datový proud
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2010-August/001678.html))
  - streaming
    proudový přenos
    proudové vysílání
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2010-August/001678.html))
  - streaming video (audio, …)
    přenášené video (přenášený zvuk, …)
    vysílané video (vysílaný zvuk, …)
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2010-August/001678.html))
  - stylesheet / style sheet
    [v
    diskuzi](https://lists.l10n.cz/pipermail/diskuze/2009-May/000538.html)
  - summary
    shrnutí
  - superuser
    superuživatel
  - suspend (to RAM)
    uspat do paměti
  - switch (command line)
    přepínač
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-June/000713.html))
  - system administrator
    správce systému
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2010-September/001797.html))

# T

  - tab
    karta
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-May/000520.html);
    v produktech Mozilly
    [zůstává](https://lists.l10n.cz/pipermail/diskuze/2009-May/000533.html)
    *panel*)
  - tag
    štítek (sémantika, např. v CMS)
    značka (syntax, např. v HTML)
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2010-March/001345.html))
  - terminal
    terminál
  - theme
    motiv
  - thumbnail
    náhled
  - time zone
    časové pásmo
  - token
    token
  - toolbar
    nástrojová lišta
    ([dřívější diskuze, dohodnuto na
    LinuxDays 2012](https://lists.l10n.cz/pipermail/diskuze/2009-May/000564.html))
  - tooltip
    vysvětlivka
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2011-April/001953.html))

# U

  - unmark
    zrušit označení
  - unmount
    odpojit
  - unmute
    [v
    diskuzi](https://lists.l10n.cz/pipermail/diskuze/2009-May/000509.html)
  - update
    aktualizovat / aktualizace
  - upgrade
    povýšení
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-November/000895.html))
  - upload
    odeslat
  - user interface
    uživatelské rozhraní
  - username / user name
    uživatelské jméno

# V

  - view (View file)
    zobrazit
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-May/000497.html))
  - view (Icon view)
    [v
    diskuzi](https://lists.l10n.cz/pipermail/diskuze/2009-May/000497.html)
  - viewport
    [v
    diskuzi](https://lists.l10n.cz/pipermail/diskuze/2009-May/000514.html)
  - volume (audio)
    hlasitost
  - volume (hardware)
    svazek

# W

  - website / web site
    webové stránky
  - when getting started
    zpočátku
  - wizard
    průvodce
  - workbook
    sešit (např. v MS Excel;
    [diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-May/000538.html))
  - workload
    úloha
    ([diskuze](https://lists.openalt.org/wws/arc/diskuze-l10n-cz/2022-06/msg00000.html))
  - worksheet
    list (např. v MS Excel;
    [diskuze](https://lists.l10n.cz/pipermail/diskuze/2009-May/000538.html))
  - wrapper
    [v
    diskuzi](https://lists.l10n.cz/pipermail/diskuze/2009-May/000498.html)

# X

# Y

# Z

  - zone (time zone)
    pásmo (časové pásmo)
