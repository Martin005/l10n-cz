---
layout: page
title: Překladatelská hantýrka
permalink: /wiki/Slovníky/Překladatelská_hantýrka/
redirect_from:
  - /Překladatelská_hantýrka/
parent: Slovníky
---

{% include l10n-cz/table-of-contents.html %}

Cílem lokalizace a překladů je přizpůsobení programů do češtiny a pro
české zvyklosti. Díky styku s angličtinou i svým technickým znalostem
se ale často sami překladatelé stávají obětí nadužívání anglických slov
nebo termínů ze své překladatelské profese. Další specifické termíny
nebo názvy překladatelských technologií najdete v kategoriích
[Překladatelský
software]({% link _pages/wiki/Překladatelský_software/index.md %}) a [Formáty
souborů]({% link _pages/wiki/Formáty_souborů/index.md %}).

# C

  - CAT
    viz [Překladatelský
    software]({% link _pages/wiki/Překladatelský_software/index.md %})

# E

  - entita
    Entita je označení pro samostatný řetězec k překladu v kombinaci s
    jeho unikátním identifikátorem. Podle identifikátoru program řetězce
    používá místo nepřeložených (většinou anglických) textů. O entitách
    se většinou mluví v souvislosti s [jednojazyčnými souborovými
    formáty]({% link _pages/wiki/Formáty_souborů/index.md %}).

# F

  - fuzzy
    Jako fuzzy (nejasné, chlupaté) jsou označované automaticky
    importované překlady u [řetězců](#t), u kterých změnil
    originální text, nebo byly přesunuty na jiné místo v programu.
    Řetězce označené jako fuzzy je potřeba vždy zkontrolovat a
    případně potvrdit jako správné, nebo opravit. Používají se hlavně
    u formátu [Gettext]({% link _pages/wiki/Formáty_souborů/Gettext.md %}).

# I

  - internacionalizace, i18n
    Internacionalizace je uzpůsobení programu, aby bylo vůbec možné ho
    překládat (lokalizovat), a program pak dokázal zobrazovat
    uživatelské rozhraní ve vybraném jazyce. Zkratka i18n je numeronym
    z anglického slova *internationalization* (I, pak 18 písmen, a na
    konci N).

# L

  - lokalizace, l10n
    Česky bychom mohli říci i překlad, i když to není úplně přesné.
    Lokalizace není jen samotný překlad, ale také přizpůsobení softwaru
    místním zvyklostem. Tedy aby používal správné oddělovače desetinných
    míst, jednotky, formátování odstavců, idiomy apod. Zkratka l10n (čti
    *el ten en*) je numeronym z anglického slova *localization* (L, pak
    10 písmen, a na konci N).

# P

  - placeholder
    Parametr v textovém řetězci, který se jako takový nepřekládá.
    Ponechává se na programu, aby místo něj za běhu doplnil vlastní
    hodnotu, často jiný řetězec. Formát placeholderů v řetězcích závisí
    na konkrétním formátu souborů. Nejčastěji ho poznáte podle procenta
    nebo složených závorek - *%s*, *%1$s*, *%i*, *%@*,
    *{nazevPlaceholderu}* apod.
  - printf
    *printf* je název funkce používané v programovacích jazycích k
    formátování textů, nejčastěji určení počtu desetinných míst u čísel
    nebo doplnění proměnné do textu. Překládaný text pak může vypadat
    třeba takto: *Dnes je %s*, kde za [placeholder](#p) *%s*
    program doplní název dne nebo třeba datum.
  - projekt
    Projekt je balík souborů a řetězci, které dohromady tvoří překlad
    nějakého programu nebo jeho ucelené komponenty. S projekty se
    nejčastěji potkáte u větších firem nebo organizací, které dávají k
    překladu nejenom svůj hlavní produkt, ale i třeba webové stránky,
    nápovědu, nebo mají produktů více.

# S

  - string
    viz [textový řetězec](#t)
  - suggestion
    Česky návrh. Jde o navržený překlad, třeba od anonymního nebo zatím
    neznámého přispěvatele do překladů, který nemá právo překlady přímo
    upravovat. Návrhy schvaluje buď správce překladů, komunita, nebo se
    o nich hlasuje.

# T

  - textový řetězec
    String, nebo též textový řetězec, je v programování datový typ pro
    ukládání znaků (textu). Pro překladatele je to obvykle jednotka,
    které se v překladatelském softwaru nebo souborech překládá jako
    celek. Můžete to být popisek jednoho tlačítka (např. *OK* nebo
    *Zrušit*), ale i několik vět (*Opravdu chcete smazat tento soubor?
    Po smazání už nebude možné ho obnovit.*).
