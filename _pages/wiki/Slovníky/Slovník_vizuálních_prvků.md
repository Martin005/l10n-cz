---
layout: page
title: Slovník vizuálních prvků
permalink: /wiki/Slovníky/Slovník_vizuálních_prvků/
redirect_from:
  - /Slovník_vizuálních_prvků/
parent: Slovníky
---

Tato stránka referuje o zaběhlém způsobu překladu názvů jednotlivých
komponent grafického prostředí. Stránka je silně inspirována [Slovníkem
vizuálních prvků
GNOME](https://live.gnome.org/Czech/JakPrekladat/SlovnikVizualnichPrvku)
a [Slovníkem vizuálních prvků
KDE](https://web.archive.org/web/https://l10n.kde.org/docs/visualdict/dict.html).

| **anglický název** | **český překlad**                     | **obrázek**                                                           |  |
| ------------------ | ------------------------------------- | --------------------------------------------------------------------- |  |
| Button             | tlačítko                              | [![Button.png]({% link /assets/img/pages/Button.png %})]({% link /assets/img/pages/Button.png %})                     |  |
| Check Boxes        | zaškrtávací pole                      | [![Check boxes.png]({% link /assets/img/pages/Check_boxes.png %})]({% link /assets/img/pages/Check_boxes.png %})           |  |
| Color Selector     | paleta barev                          | [![Color selector.png]({% link /assets/img/pages/Color_selector.png %})]({% link /assets/img/pages/Color_selector.png %})     |  |
| Color Wheel        | barevný kruh                          | [![Color wheel.png]({% link /assets/img/pages/Color_wheel.png %})]({% link /assets/img/pages/Color_wheel.png %})           |  |
| Combo Box          | rozbalovací seznam                    | [![Combo box.png]({% link /assets/img/pages/Combo_box.png %})]({% link /assets/img/pages/Combo_box.png %})               |  |
| Combo Box Entry    | kombinovaný seznam                    | [![Combo box entry.png]({% link /assets/img/pages/Combo_box_entry.png %})]({% link /assets/img/pages/Combo_box_entry.png %})   |  |
| Dialog Window      | dialogové okno                        | [![Dialog window.png]({% link /assets/img/pages/Dialog_window.png %})]({% link /assets/img/pages/Dialog_window.png %})       |  |
| Expander           | rozbalující prvek; rozbalení (funkce) | [![Expander.png]({% link /assets/img/pages/Expander.png %})]({% link /assets/img/pages/Expander.png %})                 |  |
| Handlebox Mark     | značka úchytu                         | [![Handlebox mark.png]({% link /assets/img/pages/Handlebox_mark.png %})]({% link /assets/img/pages/Handlebox_mark.png %})     |  |
| Icons              | ikony                                 | [![Icons.png]({% link /assets/img/pages/Icons.png %})]({% link /assets/img/pages/Icons.png %})                       |  |
| Icon List          | seznam ikon                           | [![Icon list.png]({% link /assets/img/pages/Icon_list.png %})]({% link /assets/img/pages/Icon_list.png %})               |  |
| List Box           | seznam                                | [![List box.png]({% link /assets/img/pages/List_box.png %})]({% link /assets/img/pages/List_box.png %})                 |  |
| (Main) Menu        | (hlavní) nabídka                      | [![Menu.png]({% link /assets/img/pages/Menu.png %})]({% link /assets/img/pages/Menu.png %})                         |  |
| Menubar            | lišta nabídek                         | [![Menubar.png]({% link /assets/img/pages/Menubar.png %})]({% link /assets/img/pages/Menubar.png %})                   |  |
| Progress bar       | ukazatel průběhu                      | [![Progress bar.png]({% link /assets/img/pages/Progress_bar.png %})]({% link /assets/img/pages/Progress_bar.png %})         |  |
| Radio Buttons      | (skupinový) přepínač                  | [![Radio buttons.png]({% link /assets/img/pages/Radio_buttons.png %})]({% link /assets/img/pages/Radio_buttons.png %})       |  |
| Scroll Bar         | posuvná lišta; posuvník               | [![Scrollbar.png]({% link /assets/img/pages/Scrollbar.png %})]({% link /assets/img/pages/Scrollbar.png %})               |  |
| Separator          | oddělovač                             | [![Separator.png]({% link /assets/img/pages/Separator.png %})]({% link /assets/img/pages/Separator.png %})               |  |
| Slider             | posuvník, jezdec                      | [![Slider.png]({% link /assets/img/pages/Slider.png %})]({% link /assets/img/pages/Slider.png %})                     |  |
| Spinbox            | číselník                              | [![Prirustkove pole.png]({% link /assets/img/pages/Prirustkove_pole.png %})]({% link /assets/img/pages/Prirustkove_pole.png %}) |  |
| Statusbar          | stavová lišta                         | [![Statusbar.png]({% link /assets/img/pages/Statusbar.png %})]({% link /assets/img/pages/Statusbar.png %})               |  |
| Tab                | karta                                 | [![Tab.png]({% link /assets/img/pages/Tab.png %})]({% link /assets/img/pages/Tab.png %})                           |  |
| Tabbed Window      | okno s kartami                        | [![Tabbed window.png]({% link /assets/img/pages/Tabbed_window.png %})]({% link /assets/img/pages/Tabbed_window.png %})       |  |
| Text entry         | textové pole                          | [![Text entry.png]({% link /assets/img/pages/Text_entry.png %})]({% link /assets/img/pages/Text_entry.png %})             |  |
| Toolbar            | nástrojová lišta                      | [![Toolbar.png]({% link /assets/img/pages/Toolbar.png %})]({% link /assets/img/pages/Toolbar.png %})                   |  |
| Tree View          | stromové zobrazení                    | [![Tree view.png]({% link /assets/img/pages/Tree_view.png %})]({% link /assets/img/pages/Tree_view.png %})               |  |
| Window Titlebar    | záhlaví okna                          | [![Window titlebar.png]({% link /assets/img/pages/Window_titlebar.png %})]({% link /assets/img/pages/Window_titlebar.png %})   |  |
