---
layout: page
title: Stylistická příručka
permalink: /wiki/Slovníky/Stylistická_příručka/
redirect_from:
  - /Stylistická_příručka/
parent: Slovníky
---

{% include l10n-cz/table-of-contents.html %}

Stylistická příručka definuje pravidla pro překlady z pohledu používání
jazyka, slohu, číselných formátů apod. Je to velmi užitečný dokument pro
dodržení stylové a jazykové konzistence napříč produktem.

## Stylistická příručka L10N.cz

Tato příručka poskytuje obecné informace k překladům softwaru do českého
jazyka. Pokud má překládaný projekt vlastní příručku, řiďte se
přednostně podle jejího obsahu a až poté podle pravidel zde. Cílem
příručky je sjednotit styl přístup k překladům napříč překládanými
projekty a vyhnout se častým chybám.

Společně s touto příručkou doporučujeme přečíst si náš [Překladatelský
slovník]({% link _pages/wiki/Slovníky/Překladatelský_slovník.md %}). I zde platí, že slovník
konkrétního projektu by měl brán s vyšší prioritou, protože termíny v
něm obsažené mohou mít v jeho kontextu jiný význam.

### Čeština v kostce

Čeština je slovanský jazyk podobný polštině nebo lužické srbštině a
velmi podobný slovenštině. Podobně jako o ostatních slovanských jazyků
se podstatná a přídavná jména nebo slovesa skloňují a časují, tedy mění
tvar podle osoby, počtu, času, osoby a dalších gramatických kategorií.

### Formální úroveň jazyka

Čeština používaná v softwarových produktech by měla být přivětivá pro
cílovou skupinu uživatelů, pro uživatele zkušené i začínající.
Používejte češtinu spisovnou, ale výsledný překlad by neměl znít
příliš naškrobeně ani strojově. Vyjma případů, které uvádíme níže,
upřednostňujte činný rod před trpným. Vyhýbejte se dlouhým a složitým
souvětím s velkým počtem vedlejších vět nebo složitými obraty. Jasný
význam každé věty i popisku jsou pro uživatele za každé situace to
nejdůležitější.

Pro oslovení uživatele používejte vykání ("zadejte", nikoliv "zadej").
Uživateli netykejte ani striktně nerozkazujte. Vykání nezní nijak
přísně a zároveň se jím vyhnete rodům ("Pokud si nejste jistý/á, ..."
-\> "Pokud si nejste jisti, ...").

Vyhněte se použití první osoby ("já", "mé", "moje"), která může
uživatele mást, o kom nebo čem je řeč. Také nepersonifikujte software,
nejedná se o živou osobu. Např. místo překladu "Downloading..." jako
"Stahuji..." můžete zvolit trpný rod "Stahuje se..." nebo
"Stahování...".

Při překladu softwaru se také setkáte s velmi krátkými popisky, jako
např. "Install" nebo "Select". Vždy dodržujte k uživateli respekt a
vyhněte se rozkazovacímu způsobu\! Pro krátké popisky a tlačítka akcí
používejte infinitiv slovesa ("Zobrazit", "Použít", nikoliv "Zobraz",
"Použij"). Pokud program potřebuje od uživatele potvrzení nebo nějaký
výběr, měl by být překlad zdvořilý ("Vyberte (prosím)...").

### Přirozenost vyjadřování

Každý překlad by měl dodržovat pravidla české gramatiky. Věty musí být
správně jak gramaticky i obsahově a dobře srozumitelné. O jasný význam a
snadné porozumění je při překladu potřeba velmi dbát. Nestyďte se po
sobě texty číst nahlas nebo požádat někoho, aby si výsledek vaší práce
přečetl, jestli zní přirozeně.

Vyhněte se zastaralým slovním obratům a složitým větám. Dávejte také
pozor na příliš doslovný překlad z angličtiny, čeština má jinou stavbu
vět a pořadí slov. Překlady *slovo od slova* nejsou přesné a zní jako
stroj. Pro převedení zamýšleného významu do češtiny používejte vlastní
slova z každodenního života. Typickým příkladem je překlad "Are you sure
you want...?" jako "Jste si jisti, že chcete...?". Mnohem přirozeněji a
lépe zní věta "Opravdu chcete...?".

V případě, že narazíte na souvětí, před překladem si jej několikrát
přečtěte. Aby zněl výsledek přirozeně, může být potřeba změnit pořadí
nebo i počet vět, souvětí rozdělit nebo naopak několik vět spojit.
Snažte se udržet český text co nejjednodušší a nejvíce srozumitelný.
Dejte také pozor na čárky a středníky.

  - [Internetová jazyková příručka: Psaní čárky v
    souvětí](https://prirucka.ujc.cas.cz/?id=150)
  - [Internetová jazyková příručka v části "Pravopis –
    interpunkce"](https://prirucka.ujc.cas.cz/)
  - [Interpunkce na webu](https://www.jakpsatweb.cz/interpunkce.html)

Při překladech přivlastňovacích zájmen platí *dvakrát měř, jednou řež*.
Hlavně u anglické "your" vás může svádět k překladu "Set Firefox as your
default browser" jako "Nastavte si Firefox jako váš výchozí prohlížeč".
Nicméně správný překlad by byl "Nastavte si Firefox jako svůj výchozí
prohlížeč". Čeština má, na rozdíl od angličtiny, speciální zájmeno
"svůj" pro přivlastňování podmětu. Nahlédněte do jazykové příručky níže
a nebojte se ho použít. ;) Při překladu "you"/"your" jako "vy"/"váš"
používejte malé počáteční písmeno "v", samozřejmě s výjimkami, kdy si
to žádá gramatika (začátek věty nebo popisku tlačítka). Angličtina také
používá "you"/"your" častěji, než zní v češtině přirozeně. Někdy je tak
lepší toto zájmeno úplně vypustit.

  - [Internetová jazyková příručka: Konkurence přivlastňovacích
    zájmen](https://prirucka.ujc.cas.cz/?id=630)

Častou chybou v překladu i mluveném jazyce bývá nadužívání zájmen a
spojek jako "který", "pokud" a "ale". Pokud vás nenapadne dobrá
formulace, kde se jim můžete vyhnout, zkuste je místy nahradit za
"jenž", "avšak" nebo podmiňovací způsob slovesa ("-li").

  - [Česká zájmena –
    Wikipedie](https://cs.wikipedia.org/wiki/%C4%8Cesk%C3%A1_z%C3%A1jmena)

Ještě častějším problémem může být zbytečné používání cizích výrazů. I
když se některé běžně používají, třeba i v dané oblasti a kontextu, vždy
v překladu používejte české ekvivalenty, pokud existují. I když slova
jako "menu" a "level" mohou mnoha uživatelům počítačů znít obyčejně,
vaši rodiče nebo prarodiče mohou uvítat českou "nabídka" a "úroveň".
Pro vyhledání správných termínů použijte náš [Překladatelský
slovník]({% link _pages/wiki/Slovníky/Překladatelský_slovník.md %}) nebo některý [online
srovnávací
slovník]({% link _pages/wiki/Pomůcky_pro_překladatele.md %}#srovnávací-slovníky).
Jedinou výjimkou jsou názvy produktů nebo firem a ochranné známky.

Obvykle je také dobré se vyhnout zkratkám. Pokud je potřebujete použít,
např. z důvodu nedostatku místa v nabídce, podívejte se nejdříve do
stávajících překladů. Pokud je to poprvé, co takový problém potřebujete
řešit, pokuste se najít nějakou ustálenou zkratku nebo se podívejte do
jazykové příručky na pravidla pro jejich vytváření.

  - [Zkratky.cz - významy zkratek](https://www.zkratky.cz/)
  - [Internetová jazyková příručka: Zkratková
    slova](https://prirucka.ujc.cas.cz/?id=784)

### Kulturní odkazy, ustálená spojení a slang

Anglická ustálená spojení (idiomy) a fráze doslovně přeložená do češtiny
někdy nedávají žádný smysl. Pokud v předloze vidíte přirovnání nebo
slovní spojení, které vypadá divně, půjde o anglický idiom. Naštěstí
existují webové stránky a slovníky, které vám pomůžou pochopit jejich
význam.

  - [List of Common
    Proverbs](https://www.engvid.com/english-resource/50-common-proverbs-sayings/)
  - [List of Common
    Idioms](https://www.smart-words.org/quotes-sayings/idioms-meaning.html)
  - [Cambridge English
    Dictionary](https://dictionary.cambridge.org/dictionary/english/)

Jakmile budete vědět, co slovní spojení znamená, můžete zkusit najít
jeho český ekvivalent, nebo vyjádřit jeho význam vlastními slovy bez
jakýchkoliv přirovnání.

  - [České idiomy -
    Wikislovník](https://cs.wiktionary.org/wiki/Kategorie:%C4%8Cesk%C3%A9_idiomy)
  - [České fráze -
    Wikislovník](https://cs.wiktionary.org/wiki/Kategorie:%C4%8Cesk%C3%A9_fr%C3%A1ze)

### Jednotky a jejich převody

#### Kalendář

V českém prostředí se používá 12 měsíční [gregoriánský
kalendář](https://cs.wikipedia.org/wiki/Gregori%C3%A1nsk%C3%BD_kalend%C3%A1%C5%99).

#### Datum

| Zápis                                              | Formát                                      | Příklad           |
| -------------------------------------------------- | ------------------------------------------- | ----------------- |
| Krátký (pro roky 2000 a dále)                      | d. m. yy                                    | 1\. 12. ’99       |
| Zkrácený                                           | d. mmm                                      | 1\. pro           |
| Dlouhý (číselný)                                   | d. m. yyyy                                  | 1\. 12. 1999      |
| Dlouhý                                             | den měsíc rok (název měsíce ve druhém pádu) | 1\. prosince 1999 |
| [ISO 8601](https://cs.wikipedia.org/wiki/ISO_8601) | yy-mm-dd                                    | 1999-12-01        |

Datum a měsíc zapsané pořadovou číslicí (s tečkou a mezerou na konci)
mohou být buď jedno nebo dvouciferné. V oficiální korespondenci se
můžete setkat se striktně dvouciferným zápisem (01. 12. 1999), ale v
překladech vždy používejte zápis jednociferný (1. 12. 1999).

  - [Internetová jazyková příručka: Kalendářní datum a místo
    původu](https://prirucka.ujc.cas.cz/?id=810)
  - [Date and time notation in the Czech Republic -
    Wikipedia](https://en.wikipedia.org/wiki/Date_and_time_notation_in_the_Czech_Republic)

#### Čas

V českém prostředí používáme pro zápis času 24 hodinový formát. V
mluveném jazyce je sice 12 hodinový formát běžný ("jsou tři hodiny
odpoledne"), ale v překladu softwaru ho prosím nepoužívejte.

Hodiny a minuty jsou při zápisu času odděleny tečkou bez mezery. Hodiny
se pak zapisují jednociferně, minuty vždy dvouciferně (7.30, 18.05,
23.25). Tečku lze nahradit dvojtečkou. V tom případě se hodiny mohou
zapisovat dvouciferně také (7:30 i 07:30). Při překladu softwaru
používejte jednociferný zápis.

Pro zápis časového rozpětí se používá pomlčka rovněž bez mezer (10-13 h,
12.00-12.45).

  - [Internetová jazyková příručka: Časové
    údaje](https://prirucka.ujc.cas.cz/?id=820)
  - [Date and time notation in the Czech Republic -
    Wikipedia](https://en.wikipedia.org/wiki/Date_and_time_notation_in_the_Czech_Republic)

#### Čísla

|                           | Oddělovač                           | Symbol | Příklad            |
| ------------------------- | ----------------------------------- | ------ | ------------------ |
| Desetinná                 | čárka                               | `,`    | 1,23               |
| Tisíce                    | žádný, nebo mezera pro dlouhá čísla |        | 1234 nebo 123 456  |
| Procenta (počet)          | znak procento s mezerou             | `%`    | 99 % (99 procent)  |
| Procenta (přídavné jméno) | znak procento bez mezery            | `%`    | 99% (99 procentní) |

Číslovky se mohou stát velmi složitým oříškem, zejména při kombinaci
číslic a slov. Jako překladatel byste měli byste se měli seznámit se
správnou gramatikou pro čísla a číslovky, jaký je rozdíl mezi zápisem
"12m" a "12 m", a jak správně napsat "dvaačtyřicetiletý" s použitím
číslic.

  - [Internetová jazyková příručka: Znaky, čísla a
    číslice](https://prirucka.ujc.cas.cz/?id=785)
  - [Internetová jazyková příručka: Tvoření a psaní výrazů složených z
    číslic a slov](https://prirucka.ujc.cas.cz/?id=790)
  - [České číslovky –
    Wikipedie](https://cs.wikipedia.org/wiki/%C4%8Cesk%C3%A9_%C4%8D%C3%ADslovky)

#### Měna

Měnovou jednotkou v České republice je [české
koruna](https://cs.wikipedia.org/wiki/Koruna_%C4%8Desk%C3%A1). Značí se
buď `Kč` nebo `,-`, které následují za číslem oddělené mezerou ("50
Kč"). Při označení mincí, například "padesátikorunová mince" se mezera
nepíše (50Kč mince).

  - [Internetová jazyková příručka: Peněžní částky, značky
    měn](https://prirucka.ujc.cas.cz/?id=786)
  - [Internetová jazyková příručka: Tvoření a psaní výrazů složených z
    číslic a slov](https://prirucka.ujc.cas.cz/?id=790)

#### Fyzikální hodnoty

V České republice se používá [metrický
systém](https://cs.wikipedia.org/wiki/Metrick%C3%A1_soustava), tedy
kilogramy, metry, atd.

#### Adresa

> [adresát] <br/>
> [ulice] [číslo domu, vchodu nebo bytu] <br/>
> [poštovní směrovací číslo] [název města nebo pošty] <br/>

> Vážená paní <br/>
> Jarmila Novotná <br/>
> Pod Mlýnem 4 <br/>
> 463 41 Dlouhý Most <br/>

  - [Internetová jazyková příručka:
    Adresy](https://prirucka.ujc.cas.cz/?id=800)

#### Telefonní čísla

Telefonní čísla v České republice se sestávají z devíti číslic. Při
zápisu se obvykle oddělují mezerou do skupin po třech. Pokud je
telefonní číslo včetně mezinárodního kódu s plusem, nebo méně často se
dvěma nulami na začátku, odděluje se od zbytku čísla také mezerou.

> +420 ### ### ### <br/>
> 00420 ### ### ### <br/>

### Gramatika

Každý překlad by měl dodržovat pravidla české gramatiky. Věty musí být
správně jak gramaticky i obsahově a dobře srozumitelné. Vyhněte se
zastaralým slovním obratům a složitým větám. Při psaní souvětí dejte
[pozor na správnost
interpunkce](https://www.radyzezivota.cz/wp-content/uploads/Poj%C4%8Fme-j%C3%ADst-d%C4%9Bti_vtip.jpg).

  - [Internetová jazyková příručka: Psaní čárky v
    souvětí](https://prirucka.ujc.cas.cz/?id=150)
  - [Internetová jazyková příručka v části "Pravopis –
    interpunkce"](https://prirucka.ujc.cas.cz/)
  - [Interpunkce na webu](https://www.jakpsatweb.cz/interpunkce.html)

### Práce s texty v aplikacích, psaní velkých písmen, ...

V celých větách nebo textech na webových stránkách či článcích se pro
psaní velkých písmen vždy řiďte pravidly české gramatiky.

  - [Internetová jazyková příručka: Psaní velkých písmen – obecné
    poučení](https://prirucka.ujc.cas.cz/?id=180)
  - [Internetová jazyková příručka: Pravopis – velká
    písmena](https://prirucka.ujc.cas.cz/)

#### Interpunkční znaménka

Pokud text webového odkazu končí s koncem věty, vkládejte do odkazu na
konci tečku jenom v případě, že je odkazem celý přeložený text nebo
věta. Pokud je textem odkazu jen několik slov nebo část věty na konci,
pište tečku až za odkaz.

Při překladů textů také nezapomeňte za tečky na konci vět psát mezeru.
Mezery se píší také za (ale nikdy před) dvojtečky a středníky.
Uzávorkovaný text od závorek z vnitřní strany oddělený mezerami není.
Naopak z vnější strany závorek už mezery správně patří.

  - [Internetová jazyková příručka:
    Tečka](https://prirucka.ujc.cas.cz/?id=160)
  - [Internetová jazyková příručka:
    Dvojtečka](https://prirucka.ujc.cas.cz/?id=161)
  - [Internetová jazyková příručka:
    Závorky](https://prirucka.ujc.cas.cz/?id=163)
  - [Internetová jazyková příručka:
    Lomítko](https://prirucka.ujc.cas.cz/?id=167)

Pro uvozovky používejte správně jejich českou variantu pro začátek a
konec citovaného textu (`„` a `“`). Na české klávesnici je můžete napsat
zkratkami `Alt + 0132` respektive `Alt + 0147`. Odpovídajícími HTML
entitami jsou `„` a `“`. V ukázkách kódu, používání různých API nebo
volání funkcí programů ponechávejte uvozovky nebo apostrofy podle
předlohy (`"` nebo `'`).

  - [Internetová jazyková příručka:
    Uvozovky](https://prirucka.ujc.cas.cz/?id=162)

#### Seznamy

Čeština nemá pevná pravidla pro psaní seznamů položek. Jazyková příručka
ale obsahuje dobré zvyky, jak se s nimi vypořádat.

  - [Internetová jazyková příručka: Psaní
    výčtů](https://prirucka.ujc.cas.cz/?id=870)

#### Názvy funkcí a vlastností programů

Psaní velkých písmen mimo začátek věty nebo vlastní jména nevypadá
přirozeně. I pro názvy funkcí a vlastností programů používejte malá
písmena a dodržujte běžná pravidla české gramatiky. Např. větu "With
Tracking protection..." přeložte jako "S ochranou proti sledování...".

Pokud opravdu chcete zdůraznit důležitost a jedinečnost dané funkce,
můžete v krajním případě použít velké písmeno pro první slovo jejího
názvu v prvním pádu, společně se slovem "funkce". Předchozí příklad by
tak vypadal "S funkcí Ochrana proti sledování...". I v takovém případě
ale upřednostňujeme psát celý název malými písmeny a skloňovat ho - "S
funkcí ochrany proti sledování...".

#### Prvky uživatelského rozhraní

  - **Nadpisy** by měly být stručné a jasné. Pokud to není nutné, nemusí
    mít formu celé věty. S velkým počátečním písmenem pište jen první
    slovo nadpisu.
  - **Názvy oken a dialogů** jsou co nejkratší a používejte pro ně
    infinitiv, např. "Uložit soubor".
  - U **popisků tlačítek** píšeme s velkým počátečním písmenem jen první
    slovo, podobně jako u nadpisů. Snažte se také o popisek co
    nejkratší, ideálně jedno nebo dvě slova. Používejte slovesa v
    infinitivu, která nejlépe vystihují výsledek děje nebo akce, kterou
    tlačítko provede. Např. u tlačítka "Install" je vhodnější dokované
    sloveso "Nainstalovat", než "Instalovat". Většinu těchto případů
    byste měli najít v našem
    [slovníku]({% link _pages/wiki/Slovníky/Překladatelský_slovník.md %}).
  - Rovněž u **zaškrtávacích polí** a **přepínačů** používejte infinitiv
    a s velkým počátečním písmenem pište jen první slovo. Dejte pozor,
    aby překlady všech položek stejné skupiny byly konzistentní a zněly
    přirozeně jako výběr ze seznamu možností.
  - U **rozbalovacích seznamů** můžete také psát první slovo s velkým
    počátečním písmenem, pokud není prvek součástí věty nebo s
    odpovídajícím popiskem nedává smysl věty. V tom případě se pokuste
    popisek přizpůsobit tak, aby tvořil gramaticky správnou větu se
    všemi možnostmi ze seznamu. Také se pokuste udržet český překlad
    všech možností přibližně stejně dlouhý.
  - **Vysvětlivky** zobrazované po najetí myší na prvek v uživatelském
    rozhraní fungují jako rychlá nápověda. Použijte pro překlad jejich
    textů činný rod s daným prvkem v pozici nevyjádřeného podmětu. Např.
    "Zobrazí historii", "Uloží soubor", "Otevře nabídku".
  - **Access keys**, někdy také
    **[Akcelerátory]({% link _pages/wiki/Návody/Akcelerátory.md %})**, jsou jednoklávesové
    zkratky k určitým prvkům uživatelského rozhraní. V kombinaci s
    čtečkami obrazovky umožňují nevidomým nebo slabozrakým uživatelům
    rychlou navigaci v uživatelském rozhraní. V uživatelském rozhraní je
    v popiscích často uvidíte jako jednotlivá podtržená písmena. Při
    překladu access keys nezapomeňte, že vybraný znak musí být na české
    klávesnici a lze ho stisknout/napsat společně se stisknutou klávesou
    `Alt`. Můžete se řídit pravidlem, že vybrané písmeno musí být
    obsaženo v popisku, je bez diakritiky a v ideálním případě je v
    nabídce nebo dialogu jako access key použito jenom jednou. Pokud
    písmeno z angličtiny už tyto vlastnosti splňuje, je ideálním
    kandidátem.
  - **Klávesové zkratky** jsou tvořeny z funkční klávesy nebo kláves
    (`Ctrl`, `Cmd`, `Alt`, `Shift`, ...) a jednoho nebo více písmen.
    Např. `Ctrl+S` pro uložení souboru nebo `Ctrl+Q` pro ukončení
    aplikace. Při překladu se s nimi můžete potkat v kompletní formě
    (zápis všech kláves) nebo můžete změnit jen písmeno, které je
    součástí zkratky. Velmi doporučujeme klávesové zkratky ponechávat
    stejné, jako v anglické verzi softwaru. Výjimkou jsou situace, kdy
    použitý znak není na české klávesnici nebo ho nelze stisknou
    společně s potřebnou funkční klávesou.

## Externí odkazy

Nejrelevantnější odkazy ke konkrétním částem příručky najdete přímo v
textu.

  - [Čeština –
    Wikipedie](https://cs.wikipedia.org/wiki/%C4%8Ce%C5%A1tina)
  - [Kategorie:Česká gramatika –
    Wikipedie](https://cs.wikipedia.org/wiki/Kategorie:%C4%8Cesk%C3%A1_gramatika)
  - [Unicode CLDR - Czech Plural
    Rules](https://www.unicode.org/cldr/charts/latest/supplemental/language_plural_rules.html#cs)

### Stylistické příručky cizích projektů

  - [Drupal]({% link _pages/wiki/Drupal/pravidla.md %})
  - [Facebook Language Style
    Guides](https://www.facebook.com/translations/style_guides)
  - [Guidelines for contractors translating into Czech | Evropská
    komise](https://ec.europa.eu/info/resources-partners/translation-and-drafting-resources/guidelines-translation-contractors/guidelines-contractors-translating-czech_cs)
  - [Microsoft Style
    Guides](https://www.microsoft.com/en-us/language/StyleGuides)
  - [Mozilla - Překladatelská stylistická
    příručka](https://mozilla-l10n.github.io/styleguides/cs/)
  - [Překlady - AbcLinuxu.cz](https://www.abclinuxu.cz/preklady)
  - [Wiki překladů -
    AbcLinuxu.cz](https://www.abclinuxu.cz/preklady/wiki)
  - [WikiProjekt
    Překlad](https://cs.wikipedia.org/wiki/Wikipedie:WikiProjekt_P%C5%99eklad)
  - [WordPress](https://docs.google.com/document/d/1bVTr-Qw2jiXr6_vzztl4SoZTi0InHjkMGkLa7UEbu3s/edit)
