---
layout: page
title: Slovník s názvy států
permalink: /wiki/Slovníky/Slovník_s_názvy_států/
redirect_from:
  - /Slovník_s_názvy_států/
parent: Slovníky
---

## Názvy států

Plné názvy nebo kódy států či zemí hledejte v [číselníku
zemí](https://www.czso.cz/csu/czso/ciselnik_zemi_-czem-) vydaném Českým
statistickým úřadem (přiložený soubor XLS).

Seznam zemí je dostupný také v české lokalizací kódů ISO (konkrétně ISO
3166) na adrese <http://pkg-isocodes.alioth.debian.org/>. Viz také
<https://cs.wikipedia.org/wiki/ISO_3166-1>.

Využít lze i *Pravidla pro jednotnou úpravu dokumentů*, která vydává
Úřad pro publikace Evropské unie a jejichž součástí je seznam zemí,
území a měn: <https://publications.europa.eu/code/cs/cs-5000500.htm>.

Dalším zdrojem může být [CLDR]({% link _pages/wiki/Softwarové_knihovny/CLDR.md %}).

| **anglický název**                           | **český překlad**                         | **alternativa, doplnění**                                  |
| -------------------------------------------- | ----------------------------------------- | ---------------------------------------------------------- |
| Afghanistan                                  | Afghánistán                               |                                                            |
| Aland Islands                                | Alandy                                    |                                                            |
| Albania                                      | Albánie                                   |                                                            |
| Algeria                                      | Alžírsko                                  |                                                            |
| American Samoa                               | Americká Samoa                            |                                                            |
| Andorra                                      | Andorra                                   |                                                            |
| Angola                                       | Angola                                    |                                                            |
| Anguilla                                     | Anguilla                                  |                                                            |
| Antarctica                                   | Antarktida                                |                                                            |
| Antigua and Barbuda                          | Antigua a Barbuda                         |                                                            |
| Argentina                                    | Argentina                                 |                                                            |
| Armenia                                      | Arménie                                   |                                                            |
| Aruba                                        | Aruba                                     |                                                            |
| Australia                                    | Austrálie                                 |                                                            |
| Austria                                      | Rakousko                                  |                                                            |
| Azerbaijan                                   | Ázerbájdžán                               |                                                            |
| Bahamas                                      | Bahamy                                    |                                                            |
| Bahrain                                      | Bahrajn                                   |                                                            |
| Bangladesh                                   | Bangladéš                                 |                                                            |
| Barbados                                     | Barbados                                  |                                                            |
| Belarus                                      | Bělorusko                                 |                                                            |
| Belgium                                      | Belgie                                    |                                                            |
| Belize                                       | Belize                                    |                                                            |
| Benin                                        | Benin                                     |                                                            |
| Bermuda                                      | Bermudy                                   |                                                            |
| Bhutan                                       | Bhútán                                    |                                                            |
| Bolivia                                      | Bolívie                                   |                                                            |
| Bosnia and Herzegovina                       | Bosna a Hercegovina                       |                                                            |
| Botswana                                     | Botswana                                  |                                                            |
| Bouvet Island                                | Bouvetův ostrov                           |                                                            |
| Brazil                                       | Brazílie                                  |                                                            |
| British Indian Ocean Territory               | Britské indickooceánské území             |                                                            |
| Brunei                                       | Brunej                                    | (Sultanát Brunej)                                          |
| Bulgaria                                     | Bulharsko                                 |                                                            |
| Burkina Faso                                 | Burkina Faso                              | (dříve Horní Volta)                                        |
| Burundi                                      | Burundi                                   |                                                            |
| Cambodia                                     | Kambodža                                  |                                                            |
| Cameroon                                     | Kamerun                                   |                                                            |
| Canada                                       | Kanada                                    |                                                            |
| Cape Verde                                   | Kapverdy                                  |                                                            |
| Cayman Islands                               | Kajmanské ostrovy                         |                                                            |
| Central African Republic                     | Středoafrická republika                   |                                                            |
| Chad                                         | Čad                                       |                                                            |
| Chile                                        | Chile                                     |                                                            |
| China                                        | Čína                                      | (Čínská lidová republika; ČLR)                             |
| Christmas Island                             | Vánoční ostrovy                           |                                                            |
| Cocos (Keeling) Islands                      | Kokosové (Keelingovy) ostrovy             |                                                            |
| Colombia                                     | Kolumbie                                  |                                                            |
| Comoros                                      | Komory                                    |                                                            |
| Congo                                        | Kongo                                     |                                                            |
| Congo, The Democratic Republic of the        | Kongo, demokratická republika             | (Konžská demokratická republika), (dříve Zair)             |
| Cook Islands                                 | Cookovy ostrovy                           |                                                            |
| Costa Rica                                   | Kostarika                                 |                                                            |
| Cote d'Ivoire                                | Pobřeží slonoviny                         |                                                            |
| Croatia                                      | Chorvatsko                                |                                                            |
| Cuba                                         | Kuba                                      |                                                            |
| Cyprus                                       | Kypr                                      |                                                            |
| Czech Republic                               | Česko                                     | (Česká republika)                                          |
| Denmark                                      | Dánsko                                    |                                                            |
| Djibouti                                     | Džibutsko                                 |                                                            |
| Dominica                                     | Dominika                                  |                                                            |
| Dominican Republic                           | Dominikánská republika                    |                                                            |
| Ecuador                                      | Ekvádor                                   |                                                            |
| Egypt                                        | Egypt                                     |                                                            |
| El Salvador                                  | Salvador                                  |                                                            |
| Equatorial Guinea                            | Rovníková Guinea                          |                                                            |
| Eritrea                                      | Eritrea                                   |                                                            |
| Estonia                                      | Estonsko                                  |                                                            |
| Ethiopia                                     | Etiopie                                   |                                                            |
| Falkland Islands (Malvinas)                  | Falklandy (Malvíny)                       |                                                            |
| Faroe Islands                                | Faerské ostrovy                           |                                                            |
| Fiji                                         | Fidži                                     |                                                            |
| Finland                                      | Finsko                                    |                                                            |
| France                                       | Francie                                   |                                                            |
| French Guiana                                | Francouzská Guyana                        |                                                            |
| French Polynesia                             | Francouzská Polynésie                     |                                                            |
| French Southern Territories                  | Francouzská jižní území                   |                                                            |
| Gabon                                        | Gabon                                     |                                                            |
| Gambia                                       | Gambie                                    |                                                            |
| Georgia                                      | Gruzie                                    |                                                            |
| Germany                                      | Německo                                   |                                                            |
| United Kingdom                               | Spojené království                        |                                                            |
| Ghana                                        | Ghana                                     |                                                            |
| Gibraltar                                    | Gibraltar                                 |                                                            |
| Greece                                       | Řecko                                     |                                                            |
| Greenland                                    | Grónsko                                   |                                                            |
| Grenada                                      | Grenada                                   |                                                            |
| Guadeloupe                                   | Guadeloupe                                |                                                            |
| Guam                                         | Guam                                      |                                                            |
| Guatemala                                    | Guatemala                                 |                                                            |
| Guernsey                                     | Guernsey                                  |                                                            |
| Guinea                                       | Guinea                                    |                                                            |
| Guinea-Bissau                                | Guinea-Bissau                             |                                                            |
| Guyana                                       | Guyana                                    |                                                            |
| Haiti                                        | Haiti                                     |                                                            |
| Heard Island and McDonald Islands            | Heardův ostrov a McDonaldovy ostrovy      |                                                            |
| Holy See (Vatican City State)                | Svatý stolec (Vatikánský městský stát)    |                                                            |
| Honduras                                     | Honduras                                  |                                                            |
| Hong Kong                                    | Hongkong                                  |                                                            |
| Hungary                                      | Maďarsko                                  |                                                            |
| Iceland                                      | Island                                    |                                                            |
| India                                        | Indie                                     |                                                            |
| Indonesia                                    | Indonésie                                 |                                                            |
| Iran                                         | Írán                                      | (Íránská islámská republika)                               |
| Iraq                                         | Irák                                      |                                                            |
| Ireland                                      | Irsko                                     |                                                            |
| Isle of Man                                  | Ostrov Man                                |                                                            |
| Israel                                       | Izrael                                    |                                                            |
| Italy                                        | Itálie                                    |                                                            |
| Jamaica                                      | Jamajka                                   |                                                            |
| Japan                                        | Japonsko                                  |                                                            |
| Jersey                                       | Jersey                                    |                                                            |
| Jordan                                       | Jordánsko                                 |                                                            |
| Kazakhstan                                   | Kazachstán                                |                                                            |
| Kenya                                        | Keňa                                      |                                                            |
| Kiribati                                     | Kiribati                                  |                                                            |
| Korea, Democratic People's Republic of       | Korea, lidově demokratická republika      | (Korejská lidově demokratická republika)                   |
| Korea, Republic of                           | Korea, republika                          | (Korejská republika)                                       |
| Kosovo                                       | Kosovo                                    |                                                            |
| Kuwait                                       | Kuvajt                                    |                                                            |
| Kyrgyzstan                                   | Kyrgyzstán                                |                                                            |
| Laos                                         | Laos                                      | (Laoská lidově demokratická republika)                     |
| Latvia                                       | Lotyšsko                                  |                                                            |
| Lebanon                                      | Libanon                                   |                                                            |
| Lesotho                                      | Lesotho                                   |                                                            |
| Liberia                                      | Libérie                                   |                                                            |
| Libya                                        | Libye                                     | (Libyjská arabská džamáhíríje)                             |
| Liechtenstein                                | Lichtenštejnsko                           |                                                            |
| Lithuania                                    | Litva                                     |                                                            |
| Luxembourg                                   | Lucembursko                               |                                                            |
| Macao                                        | Macao                                     |                                                            |
| Macedonia                                    | Makedonie                                 | (Makedonie, bývalá jugoslávská republika)                  |
| Madagascar                                   | Madagaskar                                |                                                            |
| Malawi                                       | Malawi                                    |                                                            |
| Malaysia                                     | Malajsie                                  |                                                            |
| Maldives                                     | Maledivy                                  |                                                            |
| Mali                                         | Mali                                      |                                                            |
| Malta                                        | Malta                                     |                                                            |
| Marshall Islands                             | Marshallovy ostrovy                       |                                                            |
| Martinique                                   | Martinik                                  |                                                            |
| Mauritania                                   | Mauritánie                                |                                                            |
| Mauritius                                    | Mauricius                                 |                                                            |
| Mayotte                                      | Mayotte                                   |                                                            |
| Mexico                                       | Mexiko                                    |                                                            |
| Micronesia                                   | Mikronésie                                | (Mikronésie, federativní státy)                            |
| Moldova                                      | Moldavsko                                 |                                                            |
| Monaco                                       | Monako                                    |                                                            |
| Mongolia                                     | Mongolsko                                 |                                                            |
| Montenegro                                   | Černá Hora                                |                                                            |
| Montserrat                                   | Montserrat                                |                                                            |
| Morocco                                      | Maroko                                    |                                                            |
| Mozambique                                   | Mosambik                                  |                                                            |
| Myanmar                                      | Myanmar                                   | (dříve Barma)                                              |
| Namibia                                      | Namibie                                   |                                                            |
| Nauru                                        | Nauru                                     |                                                            |
| Nepal                                        | Nepál                                     |                                                            |
| Netherlands                                  | Nizozemsko                                |                                                            |
| Netherlands Antilles                         | Nizozemské Antily                         |                                                            |
| New Caledonia                                | Nová Kaledonie                            |                                                            |
| New Zealand                                  | Nový Zéland                               |                                                            |
| Nicaragua                                    | Nikaragua                                 |                                                            |
| Niger                                        | Niger                                     |                                                            |
| Nigeria                                      | Nigérie                                   |                                                            |
| Niue                                         | Niue                                      |                                                            |
| Norfolk Island                               | Ostrov Norfolk                            |                                                            |
| Northern Mariana Islands                     | Severní Mariany                           |                                                            |
| Norway                                       | Norsko                                    |                                                            |
| Oman                                         | Omán                                      |                                                            |
| Pakistan                                     | Pákistán                                  |                                                            |
| Palau                                        | Palau                                     |                                                            |
| Palestine                                    | Palestina                                 | (Palestinská území, okupovaná)                             |
| Panama                                       | Panama                                    |                                                            |
| Papua New Guinea                             | Papua-Nová Guinea                         |                                                            |
| Paraguay                                     | Paraguay                                  |                                                            |
| Peru                                         | Peru                                      |                                                            |
| Philippines                                  | Filipíny                                  |                                                            |
| Pitcairn                                     | Pitcairn                                  |                                                            |
| Poland                                       | Polsko                                    |                                                            |
| Portugal                                     | Portugalsko                               |                                                            |
| Puerto Rico                                  | Portoriko                                 |                                                            |
| Qatar                                        | Katar                                     |                                                            |
| Reunion                                      | Réunion                                   |                                                            |
| Romania                                      | Rumunsko                                  |                                                            |
| Russia                                       | Rusko                                     | (Ruská federace)                                           |
| Rwanda                                       | Rwanda                                    |                                                            |
| Saint Barthelemy                             | Svatý Bartoloměj                          |                                                            |
| Saint Helena                                 | Svatá Helena                              |                                                            |
| Saint Kitts and Nevis                        | Svatý Kryštof a Nevis                     |                                                            |
| Saint Lucia                                  | Svatá Lucie                               |                                                            |
| Saint Martin                                 | Svatý Martin                              |                                                            |
| Saint Pierre and Miquelon                    | Svatý Pierre a Miquelon                   |                                                            |
| Saint Vincent and the Grenadines             | Svatý Vincenc a Grenadiny                 |                                                            |
| Samoa                                        | Samoa                                     |                                                            |
| San Marino                                   | San Marino                                |                                                            |
| Sao Tome and Principe                        | Svatý Tomáš a Princův ostrov              |                                                            |
| Saudi Arabia                                 | Saúdská Arábie                            |                                                            |
| Senegal                                      | Senegal                                   |                                                            |
| Serbia                                       | Srbsko                                    |                                                            |
| Seychelles                                   | Seychely                                  |                                                            |
| Sierra Leone                                 | Sierra Leone                              |                                                            |
| Singapore                                    | Singapur                                  |                                                            |
| Slovakia                                     | Slovensko                                 |                                                            |
| Slovenia                                     | Slovinsko                                 |                                                            |
| Solomon Islands                              | Šalamounovy ostrovy                       |                                                            |
| Somalia                                      | Somálsko                                  |                                                            |
| South Africa                                 | Jihoafrická republika                     |                                                            |
| South Georgia and the South Sandwich Islands | Jižní Georgie a Jižní Sandwichovy ostrovy |                                                            |
| Spain                                        | Španělsko                                 |                                                            |
| Sri Lanka                                    | Srí Lanka                                 | (dříve Ceylon)                                             |
| Sudan                                        | Súdán                                     |                                                            |
| Suriname                                     | Surinam                                   |                                                            |
| Svalbard and Jan Mayen                       | Špicberky a Jan Mayen                     |                                                            |
| Swaziland                                    | Svazijsko                                 |                                                            |
| Sweden                                       | Švédsko                                   |                                                            |
| Switzerland                                  | Švýcarsko                                 |                                                            |
| Syria                                        | Sýrie                                     | (Syrská arabská republika)                                 |
| Taiwan                                       | Tchaj-wan                                 | (Tchaj-wan, čínská provincie; Tchaj-wan, Čínská republika) |
| Tajikistan                                   | Tádžikistán                               |                                                            |
| Tanzania                                     | Tanzanie                                  | (Sjednocená tanzanská republika)                           |
| Thailand                                     | Thajsko                                   |                                                            |
| Timor-Leste                                  | Východní Timor                            |                                                            |
| Togo                                         | Togo                                      |                                                            |
| Tokelau                                      | Tokelau                                   |                                                            |
| Tonga                                        | Tonga                                     |                                                            |
| Trinidad and Tobago                          | Trinidad a Tobago                         |                                                            |
| Tunisia                                      | Tunisko                                   |                                                            |
| Turkey                                       | Turecko                                   |                                                            |
| Turkmenistan                                 | Turkmenistán                              |                                                            |
| Turks and Caicos Islands                     | Ostrovy Turks a Caicos                    |                                                            |
| Tuvalu                                       | Tuvalu                                    |                                                            |
| Uganda                                       | Uganda                                    |                                                            |
| Ukraine                                      | Ukrajina                                  |                                                            |
| United Arab Emirates                         | Spojené arabské emiráty                   |                                                            |
| United States                                | Spojené státy americké                    |                                                            |
| United States Minor Outlying Islands         | Menší odlehlé ostrovy USA                 |                                                            |
| Uruguay                                      | Uruguay                                   |                                                            |
| Uzbekistan                                   | Uzbekistán                                |                                                            |
| Vanuatu                                      | Vanuatu                                   |                                                            |
| Venezuela                                    | Venezuela                                 |                                                            |
| Viet Nam                                     | Vietnam                                   |                                                            |
| Virgin Islands, British                      | Britské Panenské ostrovy                  |                                                            |
| Virgin Islands, U.S.                         | Americké Panenské ostrovy                 |                                                            |
| Wallis and Futuna                            | Wallis a Futuna                           |                                                            |
| Western Sahara                               | Západní Sahara                            |                                                            |
| Yemen                                        | Jemen                                     |                                                            |
| Zambia                                       | Zambie                                    |                                                            |
| Zimbabwe                                     | Zimbabwe                                  |                                                            |
