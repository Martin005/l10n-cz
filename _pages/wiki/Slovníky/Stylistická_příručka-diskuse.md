---
layout: page
title: Stylistická příručka - archiv diskuse
permalink: /wiki/Slovníky/Stylistická_příručka/diskuse
redirect_from:
  - /Diskuse:Stylistická_příručka/
parent: Slovníky
---

Je zde dost významových chyb, pravopisných jen pár, většinou překlepy.
Editnul bych je, ale... Jsem zde dnes poprvé, a jako takový se spíše
cvičím, opravuji jen drobnosti a ještě jen někdy. Nevím totiž, zda má
má práce smysl. Jaká životnost místních textů a stránek vůbec se
předpokládá?

Myslí autor vážně věty z článku? Autor napsal: Nadpisy by měly být
stručné a jasné. Pokud to není nutné, nemusí mít formu celé věty. S
velkým počátečním písmenem pište jen první slovo nadpisu. Já bych
napsal: Nadpisy by měly být stručné a jasné. Nemají mít formu celé věty.
Nepište názvy jako např. v angličtině, kde každé slovo začíná velkým
písmenem. Nadpis pište stejně jako kteroukoliv větu v češtině, jen jej
nikdy neukončujte žádným interpunkčním znaménkem.

Podle první definice bych totiž musel tvarovat nadpisy třeba takto:
Zaměstnanci města praha jsou petr a pavel

Co si může čtenář myslet o tvaru "... není nutné, nemusí..."? Asi že má
napsat jeden nadpis jako větu, druhý nikoliv, třetí opět jako větu... a
tím text získá příjemnou osobitost projevu = k čertu s nudnou
konzistencí.

Co si dále mohu myslet o tvrzení, že čeština nemá definice seznamů?
Abychom zůstali v rozpětí jediného\! screenu, co si pak má kdokoliv
myslet o tvrzení, že na konec linku v určitém případě přidáváme tečku?

Počkám na vyjádření kohokoliv. Jsem ochoten i schopen to překopat.
Nikoliv zbytečně.

---

Ahoj.

Jako autor příručky ti rád odpovím. Originál je
[tady](https://mozilla-l10n.github.io/styleguides/cs/general.html). Do
češtiny jsem ji přeložil a trochu upravil pro ostatní projekty a jsi
první, kdo nějak diskutuje obsah české verze (díky). Překlepy apod.
klidně rovnou uprav. Hlavně v delších textech na počítači mi to občas
ujede a druhý pár očí pro kontrolu se vždy hodí.

K první formulaci - snažil jsem se celou příručku psát *pozitivně*. Tedy
minimalizovat pokyny, co nedělat nebo nepsat, ale naopak uvádět, co ano.
Také s ohledem na cílovou skupinu nepředpokládám, že na základě
stylistické příručky někdo napíše *praha* s malým *p*. Pravidla
gramatiky pořád platí a připomínek k jejich dodržování je v textu docela
dost.

Příručka není dogma. Můžou se najít případy, kdy se nadpis jako věta
hodí (třeba stránka s častými otázkami). Klíčové je ono *není nutné*.
Konečné posouzení konkrétní situace je na překladateli a případně někom
dalším, kdo bude jeho práci kontrolovat. Neumím si takovou situaci
představit, ale pokud *bude nutné*, aby každý druhý nadpis měl formu
věty, je jen na něm, jak s tím naloží. Asi by byla lepší formulace
*Pokud to není nutné, neměl by*.

Čeština opravdu nemá definováno, jak formátovat seznamy, viz v textu
odkazovaný jazyková příručka. Co se týče "vlezení se na screenu" úplně
nevím, co tím myslíš. Můžeš prosím uvést příklad dobrého a špatného
seznamu? Zdá se mi to jako obecné pravidlo pro překlad softwaru, ne
jazykový problém. Můžeme to ale někam (nevím, jestli sem) samozřejmě
doplnit. Každopádně cílem této příručky není poskytnout obsáhlý manuál
pro překlad. Dokonce by ani neměla být specifická pro software,
překládat se dá i nápověda, webové stránky nebo marketingové
materiály. K softwaru i úplně z jiných oblastí. Viz hned první věta
*Stylistická příručka definuje pravidla pro překlady z pohledu používání
jazyka, slohu, číselných formátů apod.*

Jaký je problém s tečkou?

Michal

P. S. Doporučuji tuto diskuzi nasměrovat do mailinglistu
<https://lists.l10n.cz/mailman/listinfo/diskuze>. Nevím, jestli wiki
někdo sleduje na změny kromě mě.