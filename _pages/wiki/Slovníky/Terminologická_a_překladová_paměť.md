---
layout: page
title: Terminologická a překladová paměť
permalink: /wiki/Slovníky/Terminologická_a_překladová_paměť/
redirect_from:
  - /Terminologická_a_překladová_paměť/
  - /files/terms.tbx
  - /assets/terms.tbx
  - /files/terms.tmx
  - /assets/terms.tmx
parent: Slovníky
---

{% include l10n-cz/table-of-contents.html %}

Terminologická i překladová paměť jsou příkladem
funkce/nástroje pro usnadnění práce a zajištění konzistence a
správnosti překladů.

### Terminologická paměť

Terminologická paměť nebo databáze slouží k ukládání běžných pojmů,
jednotlivých slov a jejich překladů, včetně případných doplňujících
informací. Vytváří se většinou ručně. Hlavním cílem je udržování
konzistence v celém produktu či projektu, zejména pokud na něm pracuje
více překladatelů. Konzistence (jednotnosti) se dosahuje zaznamenáním
přesných překladů odborných nebo specifických termínů a názvů, s jejich
následným využitím při překladech (i tvorbě) textů. Nástroji, které tuto
funkci podporují, lze obvykle pomocí klávesové zkratky doplnit překlad
známých termínů, a ručně přeložit jen zbytek věty. Příkladem formátu pro
ukládání a sdílení terminologie je
[TBX](https://en.wikipedia.org/wiki/TermBase_eXchange).

### Překladová paměť

Překladová paměť je automaticky generovaná databáze zdrojových a
přeložených textů (vět, odstavců, nadpisů, popisků). Může obsahovat i
staré texty, které už se v projektu nepoužívají. Používá se k
našeptávání možných překladů podle dříve přeložených podobných
řetězců, např. na základě jejich [Levenshteinovi
vzdálenosti](https://www.algoritmy.net/article/1699/Levenshteinova-vzdalenost).
To opět urychluje práci překladatele hlavně v situacích, kdy došlo jen k
drobné změně zdrojového textu nebo se tento přesunul do jiného souboru
apod. Jako jeden z formátů pro ukládání a sdílení překladové paměti se
používá
[TMX](https://en.wikipedia.org/wiki/Translation_Memory_eXchange).

## Soubory s českou terminologickou a překladovou pamětí některých projektů

  - [Glosář](https://dl.cihar.com/l10n/) generovaný z [překladatelského
    slovníku]({% link _pages/wiki/Slovníky/Překladatelský_slovník.md %})
  - [Glosář](https://fedora.zanata.org/glossary?locale=cs) z překladů
    [Fedory]({% link _pages/wiki/Překladatelské_týmy.md %}#fedora)
  - [Překladová paměť](https://transvision.mozfr.org/downloads/) z
    produktů Mozilly generovaná nástrojem
    [Transvision]({% link _pages/wiki/Překladatelský_software/Transvision.md %})
  - [Překladová paměť]({% link _pages/wiki/Překladatelský_software/Pontoon.md %}#stažení-tmx)
    z projektů Mozilly generovaná nástrojem [Pontoon]({% link _pages/wiki/Překladatelský_software/Pontoon.md %})

## Externí odkazy

  - [TermBase eXchange -
    Wikipedia](https://en.wikipedia.org/wiki/TermBase_eXchange)
  - [Translation Memory eXchange -
    Wikipedia](https://en.wikipedia.org/wiki/Translation_Memory_eXchange)
  - [Kód pro generování glosáře z překladatelského
    slovníku](https://github.com/nijel/l10n-slovnik)
