---
layout: page
title: glibc - testcasy
permalink: /wiki/Glibc/testcasy/
redirect_from:
  - /Glibc/testcasy/
  - /Glibc-testcasy/
parent: glibc
---

# načítání a výstup čísel

    #include <stdio.h>
    #include <locale.h>

    testlocales () {
        printf ("Negative sign test:\n");
        char *hyphenminus = "-1";
        // U2013 => E28093
        char *endash = "\xE2\x80\x93\x31";
        // U2212 => E28892
        char *minussign = "\xE2\x88\x92\x31";
        float x, y, z;
        sscanf (hyphenminus,"%f",&x);
        printf ("1) U002D %s = %f\n", hyphenminus, x);
        sscanf (endash,"%f",&y);
        printf ("2) U2013 %s = %f\n", endash, y);
        sscanf (minussign,"%f",&z);
        printf ("3) U2212 %s = %f\n", minussign, z);

        printf ("Decimal point test:\n");
        char *point = "123456.7890";
        char *comma = "123456,7890";
        float p, q;
        sscanf (point,"%f",&p);
        printf ("1) %s = %f\n", point, p);
        sscanf (comma,"%f",&q);
        printf ("2) %s = %f\n", comma, q);

        printf ("Thousands separator test:\n");
        char *none = "123456";
        char *tcomma = "123,456";
        char *fullstop = "123.456";
        char *space = "123 456";
        char *nbsp = "123\xC2\xA0" "456";
        // U202F => E280AF
        char *narrownbsp = "123\xE2\x80\xAF" "456";
        float a, b, c, d, e, f;
        sscanf (none,"%f",&a);
        printf ("1)       %s = %f\n", none, a);
        sscanf (tcomma,"%f",&b);
        printf ("2) U002C %s = %f\n", tcomma, b);
        sscanf (fullstop,"%f",&c);
        printf ("3) U002E %s = %f\n", fullstop, c);
        sscanf (space,"%f",&d);
        printf ("4) U0020 %s = %f\n", space, d);
        sscanf (nbsp,"%f",&e);
        printf ("5) U00A0 %s = %f\n", nbsp, e);
        sscanf (narrownbsp,"%f",&f);
        printf ("6) U202F %s = %f\n", narrownbsp, f);
        }

    main () {
        setlocale (LC_ALL,"C");
        printf ("*** C ***\n");
        testlocales ();

        setlocale (LC_ALL,"cs_CZ.UTF-8");
        printf ("\n*** cs_CZ.UTF-8 ***\n");
        testlocales ();
    }

očekávaný výstup:

    *** C ***
    Negative sign test:
    1) U002D -1 = -1.000000
    2) U2013 –1 = 0.000000
    3) U2212 −1 = 0.000000
    Decimal point test:
    1) 123456.7890 = 123456.789000
    2) 123456,7890 = 123456.000000
    Thousands separator test:
    1)       123456 = 123456.000000
    2) U002C 123,456 = 123.000000
    3) U002E 123.456 = 123.456000
    4) U0020 123 456 = 123.000000
    5) U00A0 123 456 = 123.000000
    6) U202F 123 456 = 123.000000

    *** cs_CZ.UTF-8 ***
    Negative sign test:
    1) U002D -1 = −1,000000
    2) U2013 –1 = −1,000000
    3) U2212 −1 = −1,000000
    Decimal point test:
    1) 123456.7890 = 123 456,000000
    2) 123456,7890 = 123 456,789000
    Thousands separator test:
    1)       123456 = 123 456,000000
    2) U002C 123,456 = 123,456000
    3) U002E 123.456 = 123,000000
    4) U0020 123 456 = 123 456,000000
    5) U00A0 123 456 = 123 456,000000
    6) U202F 123  456 = 123 456,000000

*pozn. Cčkové locale by nemělo jiná mínuska poznat, neboť operuje nad
ANSI_X3.4-1968*
