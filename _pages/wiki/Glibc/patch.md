---
layout: page
title: glibc - patch
permalink: /wiki/Glibc/patch/
redirect_from:
  - /Glibc/patch/
  - /Glibc-patch/
parent: glibc
---

{% include l10n-cz/table-of-contents.html %}

# Patche

  - [verze 1](https://lists.ubuntu.cz/pipermail/diskuze/2009-September/000843.html)
      - konverze
        [/usr/share/i18n/locales/cs_CZ](https://sourceware.org/git/?p=glibc.git;a=blob;f=localedata/locales/cs_CZ;hb=HEAD)
        na UTF-8
      - učinění UTF-8 defaultem pro cs_CZ v souboru
        [/usr/share/i18n/SUPPORTED](https://sourceware.org/git/?p=glibc.git;a=blob;f=localedata/SUPPORTED;hb=HEAD)
      - změna aliase `czech` na `cs_CZ.UTF-8` v souboru
        [/usr/share/locale/locale.alias](https://sourceware.org/git/?p=glibc.git;a=blob;f=intl/locale.alias;hb=HEAD)

# HowTo

## Fedora

  - `rpm -i glibc-2.10.1-5.src.rpm`
  - do `~/rpmbuild/SOURCES` rozbalit patche
  - `cd ~/rpmbuild/SPECS`
  - aplikovat patch:

<!-- end list -->

    --- glibc.spec~ 2009-08-19 16:22:18.000000000 +0200
    +++ glibc.spec  2009-09-21 15:12:03.875191839 +0200
    @@ -23,7 +23,7 @@
     Summary: The GNU libc libraries
     Name: glibc
     Version: %{glibcversion}
    -Release: 5
    +Release: 5.cslocale.1
     # GPLv2+ is used in a bunch of programs, LGPLv2+ is used for libraries.
     # Things that are linked directly into dynamically linked programs
     # and shared libraries (e.g. crt files, lib*_nonshared.a) have an additional
    @@ -42,6 +42,9 @@
     Source2: %{glibcsrcdir}-fedora.tar.bz2
     Patch0: %{name}-fedora.patch
     Patch1: %{name}-ia64-lib64.patch
    +Patch2: cs_CZ.patch
    +Patch3: locale.alias.patch
    +Patch4: SUPPORTED.patch
     Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
     Obsoletes: glibc-profile < 2.4
     Provides: ldconfig
    @@ -234,6 +237,9 @@
     %patch1 -p1
     %endif
     %endif
    +%patch2
    +%patch3
    +%patch4

     # A lot of programs still misuse memcpy when they have to use
     # memmove. The memcpy implementation below is not tolerant at

  - `rpmbuild -bb glibc.spec`
  - nainstalovat nové balíky (jako root `rpm -U ../RPMS/*/*`)

## Gentoo

todo

## openSUSE

todo

## Ubuntu

todo
