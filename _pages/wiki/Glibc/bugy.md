---
layout: page
title: glibc - bugy
permalink: /wiki/Glibc/bugy/
redirect_from:
  - /Glibc/bugy/
  - /Glibc-bugy/
parent: glibc
---

  - (Fedora/glibc) [Bug 524223 - incomplete/wrong LC_NUMERIC
    handling](https://bugzilla.redhat.com/show_bug.cgi?id=524223)
