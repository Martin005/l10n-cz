---
layout: page
title: glibc
permalink: /wiki/Glibc/
redirect_from:
  - /Glibc/
  - /Kategorie:Glibc/
has_children: true
nav_exclude: true
---

{% include l10n-cz/table-of-contents.html %}

# Lokalizace [GNU C Library (glibc)](https://cs.wikipedia.org/wiki/GNU_C_Library).

Návrh na aktualizaci českého locale v glibc aneb pokus o shrnutí
[diskuse](https://lists.ubuntu.cz/pipermail/diskuze/2009-August/000745.html)

Aktuální verzi patche a postup jeho aplikace lze najít na stránce
[Glibc-patch]({% link _pages/wiki/Glibc/patch.md %}).

Návrhy na testování jsou na stránce
[Glibc-testcasy]({% link _pages/wiki/Glibc/testcasy.md %}).

Přehled relevantních bugů roste na [Glibc-bugy]({% link _pages/wiki/Glibc/bugy.md %}).

# Návrhy k diskusi

<span style="color:green">**aktuální téma:**</span>

## UTF-8 jako default

**Aktuální stav:**

1.  V souboru
    [/usr/share/i18n/SUPPORTED](https://sourceware.org/git/?p=glibc.git;a=blob;f=localedata/SUPPORTED;hb=HEAD)
    je jako výchozí kódování ISO-8859-2 a UTF-8 jako alternativa.
2.  V souboru
    [/usr/share/locale/locale.alias](https://sourceware.org/git/?p=glibc.git;a=blob;f=intl/locale.alias;hb=HEAD)
    je nadefinován alias czech jako cs_CZ.ISO-8859-2.

**Změna:**

1.  Uvádět jako výchozí UTF-8, ISO-8859-2 jako alternativní.
2.  Změnit alias `czech cs_CZ.UTF-8`

**Odůvodnění:** Jestliže UTF-8 je defaultní kódování na konzoli, pak
například "LANG=cs_CZ date" (v distribucích neměnících tento default)
nevypíše správně znaky s diakritikou, což je nekonzistentní - default
pro locale se liší od defaultu pro zobrazení.

**Pro:**

  - Karel Volný

**Proti:**

  - Petr Písař

<!-- end list -->

  -
    \- může to rozbít skripty počítající natvrdo s tím, že default pro
    české locale je ISO-8859-2

---

<span style="color:red">**nevyřešená témata:**</span>

## LC_NUMERIC

### thousands_sep

**Aktuální stav:** Nedělitelná mezera

**Změna:** Nedělitelná zúžená mezera (U+202F)

**Odůvodnění:** V každém případě by mělo jít o nedělitelnou mezeru,
rozdělovat čísla není v češtině přípustné - přípustné je sázet bez
mezer, ale ne rozdělit. Číslo by mělo jasně stát pohromadě, proto navíc
mezera zúžená - pokud ji výstupní zařízení neumí, renderuje se jako
normální šířka, takže z hlediska terminálu žádná změna.

**Pro:**

  - Karel Volný
  - Petr Písař

## LC_TIME

### d_t_fmt

**Aktuální stav:**
%a<U00A0>%-d.<U00A0>%B<U00A0>%Y,<U00A0>%H:%M:%S<U00A0>%Z

**Změna:** %a<U00A0>%-d.<U202F>%B %Y, %-H.%-M:%-S<U00A0>%Z

**Odůvodnění:**
[viz](https://lists.ubuntu.cz/pipermail/diskuze/2009-August/000746.html)

**Pro:**

  - Petr Písař

**Proti:**

  - Karel Volný

<!-- end list -->

  -
    \- dvojtečka v oddělení hodin a minut je historicky zažitá (viz
    nádražní hodiny ...); tečka může být v časových údajích zaměněna s
    čárkou oddělující setiny sekund

<!-- end list -->

  - Miroslav Kuře

<!-- end list -->

  -
    \- ČSN 01 6910 zmiňuje pouze variantu s dvojtečkou

**Alternativa:** Petr Kovář navrhuje čárku za názvem dne.

### d_fmt

**Aktuální stav:** %-d.%-m.%Y

**Změna:** nedělitelné zúžené mezery (U+202F) po tečkách

**Odůvodnění:** Po tečce má být vždy mezera; zúžená zachovává celistvost
data.

**Pro:**

  - Karel Volný
  - Petr Písař

**Proti:**

  - Petr Kovář

<!-- end list -->

  -
    \- Krátké datum je krátké právě proto, aby bylo krátké. Čili
    rozšíření o další znaky situaci jen dále komplikuje.

### date_fmt

**Aktuální stav:** %a %b %e %H:%M:%S %Z %Y (příklad: Út zář 8 10:28:00
CEST 2009)

**Změna:** %a<U00A0>%e<U202F>.%b<U202F>%Y, %T %Z (příklad: Út 8. zář
2009, 10:28:00 CEST)

**Odůvodnění:** Původní formát je pouhou kopií amerického. V češtině se
ale pro datum používá výhradně pořadí seřazené podle délky úseků, buď
vzestupně (den. měsíc rok) nebo sestupně (rok-měsíc-den).

**Pro:**

  - Karel Volný

## LC_MONETARY

### mon_thousands_sep

**Aktuální stav:** nedělitelná mezera

**Změna:** tečka

**Odůvodnění**: Peněžní částky se zapisují bez mezer, aby nebylo možno
vepisovat číslice. Místo mezery je možno použít tečku, viz
[ÚJČ](https://www.ujc.cas.cz/poradna/odpo.html#pencast) nebo ČSN 01
6910.

**Pro:**

  - Karel Volný

<!-- end list -->

  -
    \+ (proti argumentu Petra Kováře) glibc locales negenerují "běžný
    text" (český tisk), ale strojový výstup, což mohou být typicky právě
    různé výplně formulářů včetně platebních ...

**Proti:**

  - Petr Písař

<!-- end list -->

  -
    \- Měla by tam být zúžená nezlomitelná mezera, doba psacích strojů
    je dávno pryč.

<!-- end list -->

  - Petr Kovář

<!-- end list -->

  -
    \- Řekl bych, že v běžném textu se vyskytuje spíše varianta bez
    teček (zkuste nahlédnout třeba do českého tisku, do korpusu apod.).
    Takže opět, jsem proti tomu, abychom s tímto hýbali. Je to zbytečné.

<!-- end list -->

  - Vít Pelčák

### p_sep_by_space a n_sep_by_space

**Aktuální stav:** 1

**Změna:** 0 a přidat nedělitelnou zúženou mezeru (U+202F) před Kč v
currency_symbol

**Odůvodnění:** Čeština požaduje, aby jednotka stála zároveň s číslem,
ke kterému se váže; bohužel automaticky vložená mezera je dělitelná.
Zúžená mezera zdůrazňuje, že to stojí pohromadě - pokud ji výstupní
zařízení neumí, renderuje se jako normální šířka, takže z hlediska
terminálu žádná změna.

**Pro:**

  - Karel Volný
  - Vít Pelčák

**?:**

  - Petr Písař

<!-- end list -->

  -
    "Jestli to zařídí nezlomitelnou zúženou mezeru mezi hodnotou a měnou
    a zároveň dokáže zarovnávat kolem na číslici jednotek, tak bych byl
    spokojený."

### duo_currency_symbol, duo_p_sep_by_space, duo_n_sep_by_space, monetary-thousands-sep-wc

**Aktuální stav:**

`duo_currency_symbol="Kč"`
`duo_p_sep_by_space=1`
`duo_n_sep_by_space=1`
`monetary-thousands-sep-wc=160`

**Změna:**

`duo_currency_symbol="`<U202F>`Kč"`
`duo_p_sep_by_space=0`
`duo_n_sep_by_space=0`
`monetary-thousands-sep-wc="."`

*Poznámka: Tato nastavení se zdají býti generována v souladu s
nastaveními výše uvedenými, je třeba jen zkontrolovat výsledek, aby byl
konsistentní. Z toho důvodu rovněž nemá smysl o tomto rozhodovat
samostatně.*

## LC_ADDRESS

### postal_fmt

**Aktuální stav:** %f%N%a%N%d%N%b%N%s %h %e %r%N%z %T%N%c%N

**Návrhy na změnu:**

  - %f%N%d%N%b%N%s<U00A0>%h %r%N%z<U2003>%T%N%Rc%N

<!-- end list -->

  -
    **Odůvodnění:** %a ("C/O") se v ČR nepoužívá, %e (číslo podlaží) a
    %r (číslo dveří) jen vyjímečně. Vzhledem k nemožnosti podmíněného
    zápisu by jejich přítomnost mohla být zaměněna s jinými čísly.
    NIcméně %r, které je v našich podmínkách možno chápat jako číslo
    bytu, by mělo být uvedeno dle vyhlášky 28/2001 Sb. (Uvádí se s
    lomítkem, ale při nevyplněném %r by lomítko zůstalo na ocet, takže
    raději bez.) Za PSČ by měla být široká mezera (dvě mezery ve fontu s
    pevnou šířkou). Stát by měl být verzálkami, proto prefix R.
    (Alternativně lze přepsat hodnotu country_name="Česká republika" do
    verzálek.)

<!-- end list -->

  -
    **Pro:**
      - Karel Volný

<!-- end list -->

  -
    **Proti:**
      - Petr Písař
    <!-- end list -->
      -
        \- %r by mohlo být číslo bytu nebo dveří. Někde se používá
        číslo_popisné/číslo_bytu.

<!-- end list -->

  - %f%N%d%N%b%N%s %h %r%N%z %T%N%Rc%N

<!-- end list -->

  -
    **Odůvodnění:** Jako výše, ale bez použití Unicode.

<!-- end list -->

  -
    **Pro:**
      - Petr Kovář
    <!-- end list -->
      -
        \+ ČSN 01 6910 mluví o dvou mezerách. Což odpovídá praktické
        potřebě běžného uživatele. Nikdo se nechce trápit s U+2003.

## LC_TELEPHONE

### tel_dom_fmt

**Aktuální stav:** (0%a) %l

**Návrhy na změnu:**

  - %a %l

<!-- end list -->

  -
    **Odůvodnění:** Meziměstská nula vypadla (resp. byla přesunuta jako
    součást mezinárodní předvolby) a předvolby se již nepoužívají
    (vzhledem k přenositelnosti čísla nemají význam).

<!-- end list -->

  -
    **Pro:**
      - Karel Volný

<!-- end list -->

  - \+%c %a %l

<!-- end list -->

  -
    **Odůvodnění:** ČSN 01 6910 odkazuje na Zlaté stránky, čili +420 123
    456 789. (Pozn. mezery do %l nelze vložit.)

<!-- end list -->

  -
    **Pro:**
      - Petr Písař

<!-- end list -->

  -
    **Proti:**
      - Karel Volný
    <!-- end list -->
      -
        \- vnitrorepublikově není třeba užívat mezinárodní předvolbu,
        nevidím smysl duplikovat tel_int_fmt

# Přijaté návrhy

## Konverze z ISO-8859-2 na UTF-8

**Aktuální stav:** Soubor
[/usr/share/i18n/locales/cs_CZ](https://sourceware.org/git/?p=glibc.git;a=blob;f=localedata/locales/cs_CZ;hb=HEAD)
je v kódování ISO-8859-2 a české locales se generují z tohoto kódování
(localedef -i cs_CZ@-ch -f "ISO-8859-2" ...)

**Změna:** Konverze na UTF-8.

**Odůvodnění:** Většina velkých distribucí již delší dobu používá jako
default UTF-8, ve kterém jsou i definiční soubory mnoha jiných jazyků.
UTF-8 umožňuje lépe zachytit místní specifika.

**Pro:**

  - Karel Volný

<!-- end list -->

  -
    \+ do budoucna lépe podporováno - když si to nyní otevřu na konzoli
    (která je by default UTF-8), vidím tečky místo znaků s diakritikou
    \+ narozdíl od ISO-8859-2 obsahuje Unicode (i v UTF-8) všechny znaky
    používané v češtině

<!-- end list -->

  - Petr Písař

<!-- end list -->

  -
    \- ... ale rozbíjí to zpětnou kompatibilitu

**Diskuse**: [Konverze z ISO-8859-2 na
UTF-8](https://lists.ubuntu.cz/pipermail/diskuze/2009-September/000805.html)

# Pozdržené návrhy

## LC_MONETARY

### negative_sign

**Aktuální stav:** "-" (U+002D)

**Změna:** znaménko mínus (U+2212)

**Odůvodnění:** Původní ASCII "mínus" je dnes většinou zobrazováno jako
spojovník, jeho reprezentace není protipólem znaménka plus. Lepší je
použít jasně definovaný matematický symbol.

**Pro:**

  - Karel Volný
  - Petr Písař
  - Vít Pelčák

**Důvod pozdržení:** glibc neumí rozpoznat jiné než hyphen-minus (aka
ASCII mínus), viz [(Fedora/Glibc) Bug 524223 - incomplete/wrong
LC_NUMERIC
handling](https://bugzilla.redhat.com/show_bug.cgi?id=524223)

# Zamítnuté návrhy

# další

  - Co na to textová konzola? Zobrazuje se vše podle očekávání?

<!-- end list -->

  - Když se zavedou ty nezlomitelné a zmenšené mezery, počítají s nimi i
    nástroje, které oddělují pole pomocí whitespace?

<!-- end list -->

  - glibc, zdá se, neumí měsíce ve druhém pádě

<!-- end list -->

  - otázkou je, co to udělá se vstupem programů?

<!-- end list -->

  -
    programy, které používají lokalizovaný vstup:
      - *(ví někdo o něčem?)*

<!-- end list -->

  - je třeba dořešit, co je zač duo_\* u LC_MONETARY

<!-- end list -->

  -
    Z materiálu [Specifications for handling of the
    Euro](https://people.exeter.ac.uk/DCannon/ist-5-15/document/p363.txt)
    chápu, že se nás to bude týkat, až vyměníme koruny za eura. Existují
    proměnné duo_valid_from, uno_valid_to a conversion_rate. Glibc
    zatím údaj proměnné duo_\* kopíruje z jejich uno protějšků, pokud
    nejsou zadefinovány ručně (locale/programs/ld-monetary-c:299) a
    označuje je za *non-POSIX.2 extensions*.

<!-- end list -->

  - glibc (asi?) neumí fomátovat samotné %l ("místní" část telefonního
    čísla; v současnosti převládá formát po trojicích číslic)

<!-- end list -->

  - k čemu je soubor
    [cs_CZ.in](https://sourceware.org/git/?p=glibc.git;a=blob;f=localedata/cs_CZ.in;hb=HEAD)
    a nebude potřeba jej též zkonvertovat?
