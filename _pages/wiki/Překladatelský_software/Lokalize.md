---
layout: page
title: Lokalize
permalink: /wiki/Překladatelský_software/Lokalize/
redirect_from:
  - /Lokalize/
parent: Překladatelský software
---

{% include l10n-cz/table-of-contents.html %}

**Lokalize** je lokalizační program pro KDE. Nahrazuje
[KBabel]({% link _pages/wiki/Překladatelský_software/KBabel.md %}) jehož vývoj je přerušen. Vytvořil ho Nick
Shaforostoff.

## Nastavení editoru při překladu KDE

Nejprve si stáhněte překlady, jak je popsáno na [jiné
stránce]({% link _pages/wiki/Návody/Jak_překládat_Kde.md %}). Po spuštění Lokalize proveďte
toto: **Projekt→Nastavit projekt** (případně **Vytvořit nový** a zadat
požadované údaje).

V **Hlavní** je důležitá položka **Kořenový adresář**. Tady je třeba
uvést cestu k adresáři, který se vytvořil po [stažení překladů ze
Summitu]({% link _pages/wiki/Návody/Jak_překládat_Kde.md %}), a do jeho podsložky "messages".

Pak se přepněte do **Pokročilé**, kde jsou tři důležité položky: složka
šablon, složka větve a alternativní složka překladu.

Do první položky je třeba zadat cestu ke složce "messages", která je
podsložkou složky vytvořené stažením šablon.

Druhá položka je identická jako položka v **Hlavní**.

A poslední položka je složka, kde se mají (respektive by měly) ukládat
překládané soubory PO.

Nakonec vše potvrďte. V okně by se měl vypsat seznam se strukturou
podobnou té na [webových stránkách
KDE](https://l10n.kde.org/stats/gui/trunk-kde4/team/cs/).

Pak si jednoduše vyberete překlad a překládáte. Po uložení by se soubory
měly ukládat tam, kde byla nastavena třetí cesta výše. Tyto soubory pak
pošlete e-mailem koordinátorovi (ideálně rozdělené do adresářů podle
toho, kam jednotlivé překlady patří, a zabalené do archivu). Opět viz
stránka [Jak překládat KDE]({% link _pages/wiki/Návody/Jak_překládat_Kde.md %}).

Čas od času nezapomeňte v adresářích/složkách, které vznikly při stažení
překladů (musí v nich být skrytý adresář .svn), spustit:

```bash
svn up
```

Aktualizace spuštěná v nadřazeném adresáři se provede i v těch
vnořených, takže nemusíte spouštět "svn up" úplně všude, stačí dva
hlavní adresáře, jak ten s překlady, tak ten se šablonami. Tím se
provede aktualizace překladů a zamezí se práci na neaktuálních
souborech.

### Překlad dokumentace

Pokud chcete překládat dokumentaci, přenastavíte cesty v Lokalize tak,
že ve čtyřech výše zmíněných položkách změníte "messages" na
"docmessages" a necháte Lokalize data zpracovat.

## Překladová paměť

Při otevřeném souboru PO v editoru Lokalize přejděte na
**Nástroje→Spravovat paměti překladu→Vytvořit→Jméno**, lze zadat
"kde-cs_CZ", "gnome-cs" apod., podle toho, na jakém projektu v Lokalize
chcete pracovat. Jako **Markup regex** zadejte "%", akcelerátor je "&"
(pro KDE) nebo "_" (pro GNOME/GTK+).

Pak zvolte **Přidat data** a zadejte cestu do adresáře se soubory PO,
které se mají zpracovat a z nichž mají být překlady převedeny do
překladové paměti.

Při překladu KDE lze zadat cestu do adresářů "messages" (vytvořených při
stažení překladů KDE).

Při překladu GNOME si můžete adresář s překlady obstarat stažením
archivu se soubory PO ze stránek [Damned
Lies](https://l10n.gnome.org/teams/cs), např. odkaz na archiv souborů PO,
které pocházejí z modulů gnome-extras, naleznete ve spodní části
[stránky s příslušnými
moduly](https://l10n.gnome.org/languages/cs/gnome-extras/ui/).

## Glosář

[Můžete si stáhnout český
glosář]({% link _pages/wiki/Slovníky/Terminologická_a_překladová_paměť.md %}) ve formátu
TBX, který v editoru Lokalize slouží jako doplňující terminologická
paměť ke standardní překladové paměti (TM). Glosář sestavil Vít
Pelčák, upravil Petr Kovář.

### Návod k použití glosáře

Soubor *terms.tbx* rozbalte na libovolné místo, a pak v programu
Lokalize vyberte **Projekt→Nastavit projekt→Hlavní→Glosář**, kde do
textového pole vyberte cestu k souboru. Posléze se ujistěte, že máte v
programu Lokalize otevřen nějaký překlad a že je zobrazen panel s
glosářem (vyberte **Nastavení→Nástrojové panely→Glosář**).

Načtený glosář můžete jednoduše upravovat klávesovou zkratkou
**Ctrl+Alt+G**.

## Externí odkazy

  - [Popis Lokalize na KDE Userbase
    (cs)](https://userbase.kde.org/Lokalize_%28cs%29)
  - [Příručka](https://docs.kde.org/development/en/kdesdk/lokalize/index.html)
