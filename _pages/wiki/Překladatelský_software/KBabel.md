---
layout: page
title: KBabel
permalink: /wiki/Překladatelský_software/KBabel/
redirect_from:
  - /KBabel/
parent: Překladatelský software
---

**KBabel** je dnes již zastaralý robustní editor souborů PO, určený pro
platformu KDE 3. Jeho nástupcem je program
[Lokalize]({% link _pages/wiki/Překladatelský_software/Lokalize.md %}), který je součástí platformy KDE 4.

## Externí odkazy

  - [Domovská stránka programu KBabel](http://kbabel.kde.org/)
