---
layout: page
title: OmegaT
permalink: /wiki/Překladatelský_software/OmegaT/
redirect_from:
  - /OmegaT/
parent: Překladatelský software
---

**OmegaT** je svobodný multiplatformní nástroj, který umí pracovat s
formáty jako je [Gettext]({% link _pages/wiki/Formáty_souborů/Gettext.md %}), ale hlavně s dokumenty
typu OpenOffice/LibreOffice, MS Office, HTML i prostými textovými
soubory. Umí používat standardní [překladovou
paměť]({% link _pages/wiki/Slovníky/Terminologická_a_překladová_paměť.md %}) a pro kontrolu
pravopisu [Hunspell]({% link _pages/wiki/Kontrola_pravopisu/Hunspell.md %}).

## Externí odkazy

  - [Domovská stránka programu OmegaT](https://omegat.org/cs/)
  - [Instalace a použití
    OmegaT](https://wiki.documentfoundation.org/CS/Documentation/UsingOmegaT#Instalace_OmegaT)
