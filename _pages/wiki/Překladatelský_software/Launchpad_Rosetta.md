---
layout: page
title: Launchpad Rosetta
permalink: /wiki/Překladatelský_software/Launchpad_Rosetta/
redirect_from:
  - /Launchpad_Rosetta/
parent: Překladatelský software
---

**Launchpad Rosetta** je webová aplikace umožňující lokalizaci aplikací
založenou na knihovně [gettext]({% link _pages/wiki/Formáty_souborů/Gettext.md %}), která je
provozovaná v rámci služby launchpad.net, spjaté především s vývojem
distribuce Ubuntu.

## Projekty používající Rosettu

  - [Ubuntu]({% link _pages/wiki/Překladatelské_týmy.md %}#ubuntu)
  - [Linux Mint]({% link _pages/wiki/Překladatelské_týmy.md %}#linux-mint)

## Externí odkazy

  - [Domovská stránka programu Launchpad
    Rosetta](https://translations.launchpad.net/)
