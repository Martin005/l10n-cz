---
layout: page
title: Transvision
permalink: /wiki/Překladatelský_software/Transvision/
redirect_from:
  - /Transvision/
parent: Překladatelský software
---

**Transvision** je webová aplikace vyvíjená francouzskou lokalizační
komunitou Mozilly. Hlavní funkcí aplikace je vyhledávač v řetězcích
všech produktů ([Firefox](https://www.mozilla.org/firefox/products/),
[Thunderbird](https://www.mozilla.org/thunderbird/) i
[SeaMonkey](https://www.seamonkey-project.org/)) a webových stránek
[www.mozilla.org](https://www.mozilla.org/). Mezi dalšími funkcemi je
[generování TMX]({% link _pages/wiki/Slovníky/Terminologická_a_překladová_paměť.md %}) nebo
zobrazení [nekonzistentních
překladů](https://transvision.mozfr.org/consistency/). Aplikace je
napsaná v jazyce [PHP](https://php.net/).

## Externí odkazy

  - [Transvision](https://transvision.mozfr.org/)
  - [Ukázka používání](https://www.youtube.com/watch?v=r8krrF-nroo)
  - [Jak se zapojit do
    vývoje](https://github.com/mozfr/transvision/blob/master/CONTRIBUTING.md)
  - [Zdrojový kód na GitHubu](https://github.com/mozfr/transvision)
