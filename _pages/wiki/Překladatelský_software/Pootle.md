---
layout: page
title: Pootle
permalink: /wiki/Překladatelský_software/Pootle/
redirect_from:
  - /Pootle/
parent: Překladatelský software
---

**Pootle** je (dnes již nevyvíjená) webová aplikace umožňující
lokalizaci softwaru založenou na knihovně [gettext]({% link _pages/wiki/Formáty_souborů/Gettext.md %}),
příp. dalších knihovnách, a to online přímo ve webovém prohlížeči. Není
tedy omezen fungováním na jen na určitě platformy. Využívá sadu
lokalizačních nástrojů [Translate
Toolkit]({% link _pages/wiki/Softwarové_knihovny/Translate_Toolkit.md %}). Pro své překlady ho v minulosti
používaly projekty LibreOffice a Mozilla.

## Projekty používající Pootle

  - Jolla

## Externí odkazy

  - [Domovská stránka aplikace
    Pootle](https://pootle.translatehouse.org/)
  - [Repositář na GitHubu](https://github.com/translate/pootle)
