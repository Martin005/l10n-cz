---
layout: page
title: Weblate
permalink: /wiki/Překladatelský_software/Weblate/
redirect_from:
  - /Weblate/
parent: Překladatelský software
---

**Weblate** je open-source webová aplikace umožňující lokalizaci. Může
být přímo propojeno s vývojářským repozitářem, takže překlady se do
projektu dostávají automaticky. Weblate je napsaný v jazyce
[Python](https://www.python.org/) a využívá framework
[Django](https://www.djangoproject.com/).

## Projekty používajcí Weblate

  - [Fedora]({% link _pages/wiki/Překladatelské_týmy.md %}#fedora)
  - [openSUSE]({% link _pages/wiki/Překladatelské_týmy.md %}#opensuse)
  - Elementary OS
  - LXQt
  - [LibreOffice]({% link _pages/wiki/Překladatelské_týmy.md %}#libreoffice)

## Externí odkazy

  - [Weblate](https://weblate.org/)
  - [Česká lokalizace
    Weblate](https://hosted.weblate.org/languages/cs/weblate/)
  - [Dokumentace Weblate](https://docs.weblate.org/)
  - [Zdrojový kód na GitHubu](https://github.com/WeblateOrg/weblate)
