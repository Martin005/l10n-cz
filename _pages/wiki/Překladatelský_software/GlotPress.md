---
layout: page
title: GlotPress
permalink: /wiki/Překladatelský_software/GlotPress/
redirect_from:
  - /GlotPress/
parent: Překladatelský software
---

**GlotPress** je webová služba umožňující lokalizaci softwaru online ve
webovém prohlížeči. Dostupná je i formou pluginu do WordPressu. Používá
se i pro překlady samotného WordPressu, jeho pluginů a šablon.

## Projekty používající GlotPress

  - [WordPress]({% link _pages/wiki/Překladatelské_týmy.md %}#wordpress)
  - pluginy pro WordPress
  - šablony pro WordPress
  - Gravatar

## Externí odkazy

  - [Vývojářský GlotPress.blog](https://glotpress.blog/)
  - [GlotPress WordPress.org](https://translate.wordpress.org/)
