---
layout: page
title: Textový editor
permalink: /wiki/Překladatelský_software/Textový_editor/
redirect_from:
  - /Textový_editor/
parent: Překladatelský software
---

Textové editory jsou určené pro úpravu obyčejných textových formátů,
jako jsou třeba *.txt*, *.csv*, *.xml*, *.json* apod. Často textové
editory přímo nebo formou pluginů podporují např. zvýrazňování nebo
automatické doplňování syntaxe nejpoužívanějších formátů. I většina
formátů souborů používaných při lokalizaci lze upravovat v textových
editorech, někdy je to dokonce ten nejpohodlnější způsob.

Vyvarujte se však používání textových procesorů (MS Word) nebo
Poznámkového bloku Windows, které soubory buď korektně nenačtou, nebo
po úpravě neuloží ve správném formátu. Jako vyzkoušené textové editory
můžeme doporučit např.

  - [Atom](https://atom.io/)
  - [Gedit](https://wiki.gnome.org/Apps/Gedit)
  - [Geanny](https://www.geany.org/)
  - [Notepad++](https://notepad-plus-plus.org/)
  - [PSPad](https://www.pspad.com/cz/)
  - textové editory předinstalované v linuxových distribucích
