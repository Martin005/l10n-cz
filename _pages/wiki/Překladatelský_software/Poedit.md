---
layout: page
title: Poedit
permalink: /wiki/Překladatelský_software/Poedit/
redirect_from:
  - /Poedit/
parent: Překladatelský software
---

**Poedit** je jednoduchý editor souborů *.po* a *.pot*
([gettext]({% link _pages/wiki/Formáty_souborů/Gettext.md %})) a [XLIFF]({% link _pages/wiki/Formáty_souborů/XLIFF.md %}). Podporuje
více platforem vč. Linuxu, Windows a macOS. V základní verzi je
dostupný pod svobodou licencí MIT. Jeho autorem je Václav Slavík.

## Externí odkazy

  - [poEdit – Wikipedie](https://cs.wikipedia.org/wiki/PoEdit)
  - [Domovská stránka programu Poedit](https://poedit.net/)
  - [Repositář na GitHubu](https://github.com/vslavik/poedit)
