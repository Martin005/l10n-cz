---
layout: page
title: LTT Edit
permalink: /wiki/Překladatelský_software/LTT_Edit/
redirect_from:
  - /LTT_Edit/
parent: Překladatelský software
---

[LTT Edit](https://github.com/pervoj/ltt-edit) je oficiální editor
souborů *.ltt* a *.lttp* (překladového systému [LTT]({% link _pages/wiki/Formáty_souborů/LTT.md %})).
