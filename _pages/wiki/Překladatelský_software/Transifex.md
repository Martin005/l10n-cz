---
layout: page
title: Transifex
permalink: /wiki/Překladatelský_software/Transifex/
redirect_from:
  - /Transifex/
parent: Překladatelský software
---

**Transifex** je webová služba umožňující lokalizaci softwaru, a to
online přímo ve webovém prohlížeči. Není tedy omezen fungováním na jen
na určitě platformy. Původně šlo o open source aplikaci, nicméně v roce
2013 byl její vývoj v této formě ukončen a dále funguje jen jako
hostovaná SaaS služba, která je pro projekty zpoplatněná. Jako
překladatel si ale můžete založit účet zdarma a bez omezení si najít
projekt, do kterého se zapojit.

## Projekty používající Transifex

  - [Mageia]({% link _pages/wiki/Překladatelské_týmy.md %}#mageia)
  - [Tor]({% link _pages/wiki/Tor.md %}) a Tails
  - NextCloud
  - ownCloud

## Externí odkazy

  - [Transifex.com](https://www.transifex.com/)
  - [Dokumentace Transifexu](https://docs.transifex.com/)
