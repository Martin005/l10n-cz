---
layout: page
title: Překladatelský software
permalink: /wiki/Překladatelský_software/
redirect_from:
  - /Kategorie:Překladatelský_software/
  - /Kategorie:Offline_překladatelský_software/
  - /Kategorie:Online_překladatelský_software/
has_children: true
nav_exclude: true
---

{% include l10n-cz/table-of-contents.html %}

Lokalizační programy a nástroje, jejich popis a návody, jak je používat.

Výběr správného softwaru se bude odvíjet v první řadě podle projektu,
který překládáte, a použitých [lokalizačních
formátů]({% link _pages/wiki/Formáty_souborů/index.md %}).

## CAT

Zkratka z anglického *computer-assisted translation* nebo
*computer-aided translation*. CAT je forma překladu, kdy se používají
nějaké počítačové pomůcky. Ty se obvykle označují jako *CAT tools*,
tedy *CAT nástroje*. Může jít třeba o návrhy formou strojových překladů,
[kontrolu pravopisu]({% link _pages/wiki/Kontrola_pravopisu/index.md %}) a
gramatiky, nebo [terminologické a výkladové slovníky a databáze v
elektronické podobě]({% link _pages/wiki/Slovníky/Terminologická_a_překladová_paměť.md %}).

## Offline překladatelský software

Pokud dáváte přednost překladům na svém počítači bez závislosti na
internetu, projděte si níže uvedené offline nástroje. Offline nástroje
je dobré používat ve chvíli, pokud máte dobrou znalost používaného
[lokalizačního formátu]({% link _pages/wiki/Formáty_souborů/index.md %}), chcete
provádět hromadné změny v existujících překladech nebo jste jediným
členem překladatelského týmu.

Oproti _online_ nástrojům je výhoda v nezávislosti na internetovém připojení
a také na projektu, který překládáte. S tím souvisí i většinou lepší podpora
pro import externí [paměti]({% link _pages/wiki/Slovníky/Terminologická_a_překladová_paměť.md %}).
Na druhou stranu je složitější paměť sdílet, pokud není integrovaná,
stejné jako provádět změny překladů v jednom souboru více autory
najednou.

## Online překladatelský software

Při překladech softwaru se používají i nejrůznější webové aplikace. Jde
buď o nástroj přímo pro překlad, nebo různé vyhledávače a [online
srovnávací
slovníky]({% link _pages/wiki/Pomůcky_pro_překladatele.md %}#srovnávací-slovníky).

Hlavní výhoda webových překladatelských rozhraní oproti _offline_
nástrojům je v integrovaném odesílání správcům projektu a snazším
sdílení práce s ostatními překladateli. Mívají i podporu nebo přímo
vynucují kontrolu překladů (review) důvěryhodnými překladateli a
hlasování nebo jiný způsob výběru nejvhodnějšího z navržených překladů.
Pro překlady řešené formou čistého
[crowdsourcingu](https://cs.wikipedia.org/wiki/Crowdsourcing) jde o
přímočaré řešení.
