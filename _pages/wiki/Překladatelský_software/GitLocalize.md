---
layout: page
title: GitLocalize
permalink: /wiki/Překladatelský_software/GitLocalize/
redirect_from:
  - /GitLocalize/
parent: Překladatelský software
---

**GitLocalize** je hostovaný systém pro překlady, poskytovaný jako
služba integrovaná s GitHubem. Automaticky v repositáři vyhledává
soubory k překladu a umožňuje překládat např. i soubory typu Markdown,
Kramdown apod.

## Externí odkazy

  - [Domovská stránka GitLocalize](https://gitlocalize.com/)
  - [GitLocalize Help Center](https://docs.gitlocalize.com/)
  - [Projekty GitLocalize na GitHubu](https://github.com/gitlocalize)
