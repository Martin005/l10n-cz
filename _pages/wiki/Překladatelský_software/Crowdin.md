---
layout: page
title: Crowdin
permalink: /wiki/Překladatelský_software/Crowdin/
redirect_from:
  - /Crowdin/
parent: Překladatelský software
---

**Crowdin** je webová služba umožňující lokalizaci softwaru, a to online
přímo ve webovém prohlížeči. Není tedy omezen fungováním na jen na
určitě platformy. Jeho zaměření je na
[crowdsourcing](https://cs.wikipedia.org/wiki/Crowdsourcing) překladů,
kdy se o navržených překladech se hlasuje, dokud je někdo neověří nebo
nezískají dost hlasů. Crowdin používají open source projekty i komerční
firmy. Crowdin sám není open source a funguje jako hostovaná SaaS
služba, která je pro projekty zpoplatněná. Jako překladatel si ale
můžete založit účet zdarma a najít si veřejný projekt, do kterého se
zapojit.

## Projekty používající Crowdin

  - [Joomla]({% link _pages/wiki/Překladatelské_týmy.md %}#joomla)
  - Mastodon
  - Pale Moon
  - uBlock Origin
  - Adblock Plus
  - Avast

## Externí odkazy

  - [Crowdin.com](https://crowdin.com/)
