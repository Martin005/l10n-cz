---
layout: page
title: Gtranslator
permalink: /wiki/Překladatelský_software/Gtranslator/
redirect_from:
  - /Gtranslator/
parent: Překladatelský software
---

*Gtranslator* je editor souborů PO pro prostředí GNOME. Obsahuje řadu
pokročilých funkcí, které vám zjednoduší práci:

  - otevírání více překladů na kartách
  - paměť překladů, která vám ukazuje, jak jste stejný text přeložili
    dříve
  - profily pro vyplňování hlaviček souborů PO
  - zvýrazňování syntaxe, včetně bílých znaků

Podporuje další rozšiřování pomocí zásuvných modulů. Standardní součástí
jsou:

  - Alternativní jazyk - souběžné zobrazení stejného překladu v jiném
    jazyce
  - Slovníky - vyhledávání v různých slovnících
  - Celá obrazovka - režim zobrazení přes celou obrazovku
  - Mapa znaků - vkládání znaků, které nejsou na klávesnici
  - Vkládání značek - kopírování formátovacích značek a proměnných z
    originálního textu
  - Zobrazení zdrojového kódu - otevře zdrojový kód v místě, kde se
    nachází překládaný řetězec

## Externí odkazy

  - [Domovská stránka](https://wiki.gnome.org/Apps/Gtranslator)
