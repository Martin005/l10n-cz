---
layout: page
title: Virtaal
permalink: /wiki/Překladatelský_software/Virtaal/
redirect_from:
  - /Virtaal/
parent: Překladatelský software
---

**Virtaal** je minimalistický editor souborů *.po*
([gettext]({% link _pages/wiki/Formáty_souborů/Gettext.md %})) využívající Python a nástroje [Translate
Toolkit]({% link _pages/wiki/Softwarové_knihovny/Translate_Toolkit.md %}). Podporuje více platforem vč.
Linuxu, Windows a Mac OS X.

## Externí odkazy

  - [Domovská stránka programu
    Virtaal](https://virtaal.translatehouse.org/)
  - [Repositář na GitHubu](https://github.com/translate/virtaal)
