---
layout: page
title: Translatewiki.net
permalink: /wiki/Překladatelský_software/Translatewiki.net/
redirect_from:
  - /Translatewiki.net/
parent: Překladatelský software
---

**Translatewiki.net** je webová služba umožňující lokalizaci softwaru
online ve webovém prohlížeči. Vyvíjena je formou rozšíření do MediaWiki.
Pro překlady svých projektů ji používá přímo MediaWiki a Wikimedia.

## Projekty používající Translatewiki

  - MediaWiki a Wikimedia
  - OpenStreeMap
  - Mantis Bug Tracker
  - Etherpad
  - Kiwix

## Externí odkazy

  - [translatewiki.net](https://translatewiki.net/)
