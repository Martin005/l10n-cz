---
layout: page
title: Pontoon
permalink: /wiki/Překladatelský_software/Pontoon/
redirect_from:
  - /Pontoon/
parent: Překladatelský software
---

{% include l10n-cz/table-of-contents.html %}

**Pontoon** je open-source webová aplikace umožňující lokalizaci
prakticky všech projektů Mozilly. Hlavní funkcí, kterou se odlišuje od
ostatních nástrojů, je možnost lokalizace stránek přímo na webu s
okamžitým náhledem, jak bude stránka ve výsledku v daném jazyce
vypadat. Také má asi nejpokročilejší podporu formátu
[Fluent]({% link _pages/wiki/Formáty_souborů/Fluent.md %}) (také od Mozilly). Pontoon je napsaný v
jazyce [Python](https://www.python.org/) a využívá framework
[Django](https://www.djangoproject.com/).

### Stažení TMX

1.  Otevřete [stránku českého týmu](https://pontoon.mozilla.org/cs/).
2.  Vyberte projekt a zobrazení všech jeho řetězců, nebo [všech řetězců
    v celém
    Pontoonu](https://pontoon.mozilla.org/cs/all-projects/all-resources/).
3.  Vpravo nahoře klepněte na "hamburger menu" nebo svůj avatar, pokud
    jste přihlášeni.
4.  Z nabídky vyberte *Download translation memory*.

Více informací najdete v [dokumentaci
Mozilly](https://mozilla-l10n.github.io/localizer-documentation/tools/pontoon/translate.html#downloading-and-uploading-translations)
a v [tomto příspěvku na
blogu](https://blog.mozilla.org/l10n/2017/05/30/reuse-mozilla-translations/).

## Projekty používající Pontoon

  - [Mozilla]({% link _pages/wiki/Překladatelské_týmy.md %}#mozilla)

## Externí odkazy

  - [Pontoon](https://pontoon.mozilla.org/)
  - [Tutoriál](https://pontoon.mozilla.org/cs/tutorial/playground/)
  - [Česká lokalizace na Pontoonu](https://pontoon.mozilla.org/cs/)
  - [Jak překládat pomocí
    Pontoonu](https://developer.mozilla.org/cs/docs/Mozilla/Localization/Jak_prekladat_pomoci_Pontoonu)
  - [How to use Pontoon - Documentation for Mozilla
    localizers](https://mozilla-l10n.github.io/localizer-documentation/tools/pontoon/)
  - [Jak se zapojit do
    vývoje](https://wiki.mozilla.org/Webdev/GetInvolved/pontoon.mozilla.org)
  - [Zdrojový kód na GitHubu](https://github.com/mozilla/pontoon)
