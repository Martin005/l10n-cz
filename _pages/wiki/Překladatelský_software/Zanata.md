---
layout: page
title: Zanata
permalink: /wiki/Překladatelský_software/Zanata/
redirect_from:
  - /Zanata/
parent: Překladatelský software
---

**Zanata** je (dnes již nevyvíjená) webová aplikace pro lokalizaci
softwaru a její správu, a to online přímo ve webovém prohlížeči, nebo
pomocí řádkového klienta a vlastních offline nástrojů. Zanata je napsaná
v jazyce
[Java](https://cs.wikipedia.org/wiki/Java_\(programovac%C3%AD_jazyk\)).

## Externí odkazy

  - [Domovská stránka aplikace Zanata](http://zanata.org/)
  - [Dokumentace aplikace Zanata](https://docs.zanata.org/)
  - [Návod pro
    překladatele](https://docs.zanata.org/en/release/user-guide/translator-guide/)
  - [Zdrojové kódy na GitHubu](https://github.com/zanata)
