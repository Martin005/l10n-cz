---
layout: page
title: Drupal - Slovník
permalink: /wiki/Drupal/Slovnik/
redirect_from:
  - /Drupal:Slovník/
parent: Drupal
---

## Problematická slova a doporučení, jak je překládat

| Anglický výraz | Schválené doporučení | Návrhy (preferovaný nahoře) | Stav |
| -------------- | -------------------- | --------------------------- | ---- |
| !start-date to !end-date | !start-date — !end-date |  | schváleno |
| accept | přijmout | ... | schváleno |
| account | účet | ... | schváleno |
| adjustment(s) | nastavení | ... | schváleno |
| already | již | ... | schváleno |
| apply | použít | ... | schváleno |
| approve,<br />approval | schválit,<br />schválení | ... | schváleno |
| Are you sure you want (...) | Opravdu chcete (...) | ... | schváleno |
| argument | argument | ... | schváleno |
| at least | alespoň | ... | schváleno |
| attribute | atribut | atribut<br />vlastnost | schváleno |
| authentication | ověření | ... | schváleno |
| autocomplete matching | porovnávání s automatickým dokončováním | ... | schváleno |
| autocomplete, autocomplete field | automatické dokončování, pole s automatickým dokončováním | ... | schváleno |
| batch | dávka | ... | schváleno |
| because | protože | protože<br />neboť | schváleno |
| box | okno | okno<br />pole | schváleno |
| breakpoint | breakpoint | ... | schváleno |
| cache | cache<br />mezipaměť<br />vyrovnávací paměť | ... | schváleno |
| CAPTCHA | CAPTCHA (neskloňovat) | ... | schváleno |
| capture | zaznamenat | ... | schváleno |
| cart duration | platnost košíku | ... | schváleno |
| challenge | test (týká se modulu CAPTCHA) | ... | schváleno |
| clone | klonovat | klonovat<br />kopírovat | schváleno |
| company | společnost | ... | schváleno |
| component | komponenta | ... | schváleno |
| configure | konfigurovat | ... | schváleno |
| confirmation message | potvrzovací zpráva | ... | schváleno |
| credit card | platební karta | platební karta<br />kreditní karta | schváleno |
| cron | cron (a normálně skloňovat) | ... | schváleno |
| custom | vlastní | ... | schváleno |
| customization | vlastní nastavení | ... | schváleno |
| dashboard | ovládací panel | ... | schváleno |
| debugging information | ladicí informace (pozor, je tu krátké "i") | ... | schváleno |
| default | výchozí | ... | schváleno |
| deny | zamítnout | ... | schváleno |
| directory | adresář | ... | schváleno |
| disable, disabled | vypnout, vypnuto | vypnout, vypnuto<br />zakázat, zakázáno | schváleno |
| Display Options | Možnosti zobrazení | ... | schváleno |
| distinct | unikátní | ... | schváleno |
| drag-and-drop | drag-and-drop / v delších textech dávat s vysvětlením v závorce: drag-and-drop (táhni a pusť) | schváleno |  |
| Drupal user | uživatel Drupalu | ... | schváleno |
| e-mail, email | e-mail | ... | schváleno |
| emulate | emulovat | ... | schváleno |
| enable, enabled | zapnout, zapnuto | zapnout, zapnuto<br />povolit, povoleno | schváleno |
| enter | zadat | ... | schváleno |
| error message | chybová zpráva | ... | schváleno |
| example content | ukázkový obsah | ukázkový obsah<br />pokusný obsah<br />příklad obsahu | schváleno |
| exclude from display | nezobrazovat | ... | schváleno |
| exposed | vystavený | ... | schváleno |
| express | expresní | ... | schváleno |
| fatal error | kritická chyba | ... | schváleno |
| feature | vlastnost (! pouze ve větách) | vlastnost<br />funkcionalita<br />funkce | schváleno |
| feed | zdroj | zdroj<br />kanál<br />feed | schváleno |
| fetcher | načítač | ... | schváleno |
| field | pole | ... | schváleno |
| fieldgroup | skupina polí | ... | schváleno |
| format | formát | ... | schváleno |
| formatted | formátovaný | ... | schváleno |
| From date | Od data | ... | schváleno |
| fulfillment | splnění | ... | schváleno |
| generic files | obecné soubory | obecné soubory<br />generické soubory | schváleno |
| granularity | rozlišení | rozlišení<br />dělení<br />granularita<br />přesnost<br />členění<br />rozlišovací schopnost | schváleno |
| HTTP Authorization header | HTTP autorizační hlavička | ... | schváleno |
| hyperlink | odkaz | odkaz<br />hypertextový odkaz | schváleno |
| information | informace | informace<br />údaj(e) | schváleno |
| instance | instance | ... | schváleno |
| invalid | neplatný | ... | schváleno |
| issuer | vystavitel | ... | schváleno |
| is not possible | nelze | nelze<br />není možné | schváleno |
| label | popisek<br />(případně: štítek, název - záleží na kontextu) | popisek<br />štítek | schváleno |
| layout | rozložení | ... | schváleno |
| leave | nechat, nechte | ... | schváleno |
| load | načíst | ... | schváleno |
| log | log | log<br />protokol | schváleno |
| logging | logování | ... | schváleno |
| merchant ID | ID obchodníka | ... | schváleno |
| merchant key | klíč obchodníka | ... | schváleno |
| migrate, migrating (data) | přesun (dat) | ... | schváleno |
| mode (display mode) | režim (režim zobrazení) | ... | schváleno |
| name | jméno | ... | schváleno |
| next to | vedle | ... | schváleno |
| node | uzel | uzel<br />příspěvek | schváleno |
| node form | formulář uzlu | formulář uzlu<br />formulář příspěvku<br />editační formulář | schváleno |
| node type | typ obsahu | ... | schváleno |
| non-latin characters | non-latin znaky | ... | schváleno |
| none available | není k dispozici | ... | schváleno |
| offline | offline | ... | schváleno |
| optional | volitelná<br /> | nepovinný | schváleno |
| options | možnosti | volby | schváleno |
| override | překrýt | překrýt<br />přepsat<br />přetížit<br />změnit<br />override | schváleno |
| package | balík | balík<br />balíček | schváleno |
| packing slip | dodací list | ... | schváleno |
| page | stránka | stránka<br />strana | schváleno |
| parse error | chyba při zpracování | ... | schváleno |
| pattern | vzorek | ... | schváleno |
| payment methods | způsoby platby | ... | schváleno |
| people | uživatelé | ... | schváleno |
| pickup address | adresa pro vyzvednutí | ... | schváleno |
| pipe | svislá čára | svislá čára<br />svislice<br />roura | schváleno |
| plain text | prostý text | ... | schváleno |
| plugin | plugin | plugin<br />plug-in<br />zásuvný modul<br />zásuvný prvek | schváleno |
| popup | vyskakovací / vyskakovací okno | ... | schváleno |
| postal code | PSČ | ... | schváleno |
| predicate | predikát | ... | schváleno |
| prefix | předpona | ... | schváleno |
| previous | předchozí | ... | schváleno |
| privacy policy | ochrana osobních údajů | ... | schváleno |
| product title | název produktu | ... | schváleno |
| progress bar | indikátor průběhu | ... | schváleno |
| publish, published / unpublish, unpublished | zveřejnit, veřejný / skrýt, skrytý | ... | schváleno |
| raw | nezpracovaný | nezpracovaný<br />neupravený<br />neformátovaný<br /> | schváleno |
| refresh | aktualizovat | aktualizovat<br />obnovit | schváleno |
| refund | refundace | ... | schváleno |
| region | region | region<br />oblast | schváleno |
| remove | odebrat | odebrat<br />odstranit | schváleno |
| replacement patterns | nahrazovací vzorky | ... | schváleno |
| resizable | nastavitelná velikost | ... | schváleno |
| revert | obnovit | obnovit<br />vrátit se k uloženému<br />vrátit se k původnimu<br />vrátit se k výchozímu<br /> | schváleno |
| screen readers | čtečky obrazovky | čtečky obrazovky<br />screen reader programy | schváleno |
| send an order email | zaslat e-mail s objednávkou | ... | schváleno |
| session | relace | ... | schváleno |
| site | web | ... | schváleno |
| site-wide | globální | ... | schváleno |
| size | velikost | ... | schváleno |
| split summary | oddělit upoutávku (pod kurzorem) | ... | schváleno |
| star | hvězdička | ... | schváleno |
| status report | hlášení stavu | hlášení stavu<br />hlášení o stavu | schváleno |
| string | řetězec | ... | schváleno |
| submission | odeslaný formulář | odeslaný formulář<br />výsldek<br />záznam | schváleno |
| submit | odeslat | ... | schváleno |
| suffix | přípona | ... | schváleno |
| syslog | syslog | syslog<br />systémové zprávy | schváleno |
| tab | karta, záložka | karta<br />záložka<br />tab | schváleno |
| table name | název tabulky | název tabulky<br />jméno tabulky | schváleno |
| tag | štítek | štítek<br />tag | schváleno |
| teaser | upoutávka | ... | schváleno |
| template | šablona | ... | schváleno |
| text area | textová oblast | ... | schváleno |
| text formats | formáty textu | ... | schváleno |
| theme | téma vzhledu | ... | schváleno |
| themed | stylovaný | ... | schváleno |
| threshold | mezní hodnota<br />&gt; limit | mezní hodnota<br />limit | schváleno |
| throbber | pulzér | ... | schváleno |
| time zone | časové pásmo | ... | schváleno |
| timestamp | časové razítko | časové razítko<br />časová značka<br />timestamp | schváleno |
| to date | do data | ... | schváleno |
| to renew | prodloužit | ... | schváleno |
| token | token | token<br />zástupný znak<br />zástupný řetězec<br />nahrazovací vzorek<br />zástupný symbol<br />symbol<br />symbol k nahrazení | schváleno |
| trailing slash | koncové lomítko | ... | schváleno |
| transaction | transakce | ... | schváleno |
| trigger | spouštěč | spouštěč<br />trigger | schváleno |
| unrecoverable error | neodstranitelná chyba | ... | schváleno |
| updated | aktualizováno | aktualizováno<br />upraveno | schváleno |
| upload | nahrát (případně: nahrát na server, nebo nechat upload) - záleží na kontextu | ! | ... |
| user account | uživatelský účet | ... | schváleno |
| user name | uživatelské jméno | ... | schváleno |
| value is invalid | hodnota je neplatná | ... | schváleno |
| watermark | vodoznak | ... | schváleno |
| when | když &gt; jestliže &gt; pokud | ... | schváleno |
| widget | widget<br />nástroj<br />pomůcka | ... | schváleno |
| you must | je třeba | ... | schváleno |
| your | vaše (váš, vaši...) | ... | schváleno |
