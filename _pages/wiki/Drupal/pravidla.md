---
layout: page
title: Drupal - Pravidla
permalink: /wiki/Drupal/Pravidla/
redirect_from:
  - /Drupal:Pravidla/
parent: Drupal
---

## Pravidla

Často se opakující problémy a doporučení k nim.

| Pravidlo | Popis | Příklad: jak ANO | Příklad: jak NE |
| -------- | ----- | ---------------- | --------------- |
| Nepřekládejte názvy modulů. | Nikdy a nikde. |  |  |
| apod. / atd. | Pozor, v angličtině se před tím píše čárka, ale v češtině ne. | Spravovat bloky, typy obsahu, menu atd.<br />Spravovat bloky, typy obsahu, menu apod. | Spravovat bloky, typy obsahu, menu atd.<br />Spravovat bloky, typy obsahu, menu apod. |
| Snažte se vyhnout skloňování. |  | Pole %name nesmí obsahovat více hodnot než @count. | Pole %name nesmí obsahovat víc než @count hodnot. |
| Rozkazovací způsob | Používejte oužívat infinitiv nebo 2. os. množného čísla, NE 2. os. jednotného čísla (uživateli netykáme) | vložte | vlož |
| Trpný vs. činný rod | Pokud je to možné, dávejte přednost činnému rodu před trpným.<br />Např. “will be shown” &gt;&gt; raději “se zobrazí” než “bude zobrazeno”; mj. si všimněte, že u trpné varianty je třeba specifikovat mužský, ženský nebo střední rod, čemuž se často potřebujeme vyhnout. | zobrazí se | bude zobrazen (zobrazena, zobrazeno) |
| Uvozovky u pojmů | tam, kde angličtina používá pouze pojem s velkým písmenem, se v češtině často hodí uvozovky – pokud ano, tak je tam pište.<br />Příklad:<br />Update the order status to Payment Received. = Aktualizovat stav objednávky na „Platba přijata“. |  |  |
| "datum" - skloňování | Opakovaně (i v Drupalu - "Date and time lookup" / "Vyhledávání datumu a času") zakopáváme o výraz "datumu".<br />Často se v tomto chybuje.<br />Mrkněte sem: <a href="https://prirucka.ujc.cas.cz/?slovo=datum&amp;Hledej=Hledej">datum</a> | Vyhledávání data a času | Vyhledávání datumu a času |
| Neskloňovat číslice | Pokud se číslovky vyjadřují číslicemi, spousta lidí naznačuje tvar dané číslovky tím, že za číslici ještě doplní jakousi koncovku (například 9-ti násobné zvětšení, od 25ti metrů). Správný tvar číslovky totiž vyplývá z kontextu. (Viz třeba: <a href="https://cs.wikipedia.org/wiki/České_číslovky">https://cs.wikipedia.org/wiki/České_číslovky</a>)<br />Několik příladů:<br />do 18 let = do osmnácti let<br />5členný = pětičlenný<br />30letý = třicetiletý<br />5krát = 5× = pětkrát<br />Mezi číslici a označení jednotek se klade mezera. Je třeba odlišit případy, kdy se jedná o odvozené přídavné jméno, kde se naopak mezera nepíše:<br />5 % = pět procent x 5% = pětiprocentní<br />5 m = pět metrů x 5m = pětimetrový<br />Za chybné jsou považovány zápisy jako:<br />do 18-ti let (správně do 18 let)<br />5-ti %-ní roztok (správně 5% roztok)<br />30-ti kapslové balení (správně 30kapslové balení)<br /> |  |  |
| Nepřebírejte anglický slovosled a větnou stavbu | Zkuste si představit, že to není překlad, ale prostě jen česká věta. Zní dobře? |  | Například (i když je to na hranici): "Uživatelské jméno vašeho Flickr účtu", "umožňuje Drupal komunitě"... |
| Pomlčka a spojovník (a mezery) | Citováno z <a href="https://www.ujc.cas.cz/poradna/porfaq.htm">https://www.ujc.cas.cz/poradna/porfaq.htm</a>:<br />Pomlčku bez mezer používáme pro rozsah (viz níže); jinak ji oddělujeme mezerami, ať signalizuje odmlku (Ale to přece – ), vyznačuje vsuvku (A teď – aspoň podle mě – to nejdůležitější) nebo spolu se začátkem odstavce označuje začátek přímé řeči (– Kam se chystáš? zeptala se).<br />Spojovník používáme při dělení slov na konci řádku, na konci slov, která mají pokračování jinde (dvou- a třílůžkové pokoje), a u složenin vyjadřujících podvojnost nebo vzájemnost (Frýdek-Místek; česko-polské vztahy; literárně-hudební, ale srov. trestněprávní; Hanzelka-Zikmund; Havránkova-Jedličkova Česká mluvnice; paní Gabriela Beňačková-Čápová). Ve složených příjmeních žen, které se rozhodly ponechat jak manželovo, tak své původní příjmení, je však podle současného zákona o rodině možná pouze podoba bez spojovníku (Skopalová Horáková).<br />Udáváme-li rozsah, odpovídá slůvku až dlouhá pomlčka bez mezer: oběd 12.00−12.45; návštěvní hodiny: květen−září 9−17, říjen−duben 9−16; cena 770−990 Kč; termín 1.−3. 7. 2006; strana 16−24; trasa Praha−Liberec; v letech 1841−1904 a při víceslovných s mezerami: Praha 1 − Malá Strana, trasa Praha − Hradec Králové; termín 5. 8. − 12. 10. 2006. |  |  |
| Závorky a mezery | Vedle závorky uvnitř se nedělá mezera. | (něco v závorce) | ( něco v závorce ) |
| Interpunkce a mezery | Čárka, tečka, dvojtečka, středník, vykřičník, otazník: mezera patří za, ne před. Navíc: otazník má být vždy jen jeden, stejně jako vykřičník. | Takhle to vypadá, když je to dobře. Vidíte to? Takhle ano! | Nedokážu to skoro ani napsat , jak je to strašný .Vidíte to ??? Takhle fakt ne |
| Neopakujte slovo "který" | Existuje také spojka jenž. Skloňuje se takto: <a href="https://cs.wikipedia.org/wiki/%C4%8Cesk%C3%A1_z%C3%A1jmena#Vzta.C5.BEn.C3">https://cs.wikipedia.org/wiki/%C4%8Cesk%C3%A1_z%C3%A1jmena#Vzta.C5.BEn.C3</a>... |  |  |
| Neopakujte slovo "ale" | Existují také slova "avšak", "však", "nýbrž" aj. |  |  |
| Neopakujte slovo "pokud" |  | Je-li tato volba povolena, Drupal Vás upozorní, pokud budou dostupné nové aktualizace. | Pokud je tato volba povolena, Drupal Vás upozorní, pokud budou dostupné nové aktualizace. |
| Nepoužívejte zbytečně slovo "váš" | V češtině lze (často) prostě vynechat...<br />Když už musíte, tak uprostřed věty vždy s malým písmenem. |  |  |
| Kratší a jednoslovné názvy | Tam, kde je to možné, dávejte přednost kratším a jednoslovným termínům. ALE ne za každou cenu. |  |  |
| Nepřekládejte fráze otrocky |  |  | Příklad: "say Yes here" - "řekněte zde Ano": <em>dotyčný překladatel má trauma ze svatby?</em> |
| Pozor na překlepy | Opravdu, není v silách nikoho opravovat překlepy. Raději přeložte méně řetězců, ale pořádně... |  |  |
