---
layout: page
title: Tor
permalink: /wiki/Tor/
redirect_from:
  - /Tor/
nav_exclude: true
---

{% include l10n-cz/table-of-contents.html %}

O český překlad prohlížeče Tor se stará část [komunity
Mozilly]({% link _pages/wiki/Překladatelské_týmy.md %}#mozilla). Překlad probíhá na
[Transifexu]({% link _pages/wiki/Překladatelský_software/Transifex.md %}) a komunikace s Localization Lab skrze
e-mailovou konferenci nebo Mattermost. Kontaktní osobou na straně
Localization Lab je *Erin McConnell* nebo *Emma Peel*.

## Stav překladu

[\#24325 (Add Czech (cs) localization to Tor browser) – Tor Bug Tracker
& Wiki](https://trac.torproject.org/projects/tor/ticket/24325)

  - k nahlédnutí v 8.5a5 -
    <https://dist.torproject.org/torbrowser/8.5a5/>
  - ETA finální vydání 8.5

### Přeloženo, zkontrolováno

  - Tor Launcher
  - Tor Button
  - Tor Misc
      - animation-titles

### Přeloženo, ke kontrole

  - HTTPS Everywhere
  - BridgeDB
  - TorCheck
  - Release tweets
  - Tor Misc
      - ExoneraTor
      - tor-and-https
  - TorBirdy

## Externí odkazy

  - [Tor - Localization
    Lab](https://wiki.localizationlab.org/index.php/Tor)
  - [Tor Project: Translation
    Overview](https://www.torproject.org/getinvolved/translation-overview.html.en)
  - [Tor Project na
    Transifexu](https://www.transifex.com/otf/torproject/)
  - [Tor Project Support & Community Portal na
    Transifexu](https://www.transifex.com/otf/tor-project-support-community-portal/)
