---
layout: page
title: Translators without Borders
permalink: /wiki/Translators_without_Borders/
redirect_from:
  - /Translators_without_Borders/
nav_exclude: true
---

**Translators without Borders** je nezisková organizace založená za
účelem poskytování překladatelských služeb neziskovým a humanitárním
organizacím. Vznikla v roce 2010 a v roce 2012 její komunita čítala přes
1500 ověřených přispěvatelů. Po dalším růstu a sloučení s The Rosetta
Foundation v roce komunita čítá už přes 26 tisíc profesionálních
překladatelů.

## Externí odkazy

  - [TranslatorsWithoutBorders.org](https://translatorswithoutborders.org/)
  - [Translators Without Borders -
    Wikipedia](https://en.wikipedia.org/wiki/Translators_Without_Borders)
