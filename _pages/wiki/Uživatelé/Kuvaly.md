---
layout: page
title: Kuvaly
permalink: /wiki/Uživatelé/Kuvaly/
redirect_from:
  - /Uživatel:Kuvaly/
parent: Uživatelé
---

<h2 style="padding:0.2em 0.4em; margin:10px 0 10px 0; border:1px solid #a3bfb1; background:#bcee68; color:#000; font-size:120%; font-weight:bold; text-align:left; -moz-border-radius:0.2em;">

Vítejte\!

</h2>

Vítejte na mé uživatelské stránce. V dalších částech stránky naleznete
různé informace.

<h2 style="padding:0.2em 0.4em; margin:10px 0 10px 0; border:1px solid #a3bfb1; background:#bcee68; color:#000; font-size:120%; font-weight:bold; text-align:left; -moz-border-radius:0.2em;">

Kontakt

</h2>

  - **Email:**
      - kuvaly *<zavináč>* seznam *<tečka>* cz

<h2 style="padding:0.2em 0.4em; margin:10px 0 10px 0; border:1px solid #a3bfb1; background:#bcee68; color:#000; font-size:120%; font-weight:bold; text-align:left; -moz-border-radius:0.2em;">

Články

</h2>

  - [Lokalize]({% link _pages/wiki/Překladatelský_software/Lokalize.md %})
  - [Kde se zapojit]({% link _pages/wiki/Chci_aby_bylo_přeloženo.md %})

<h2 style="padding:0.2em 0.4em; margin:10px 0 10px 0; border:1px solid #a3bfb1; background:#bcee68; color:#000; font-size:120%; font-weight:bold; text-align:left; -moz-border-radius:0.2em;">

Mnou překládaný software

</h2>

  - **Součásti GNOME**
      - [Solang](https://projects.gnome.org/solang/)
  - **Samostatný software**
      - [Gimmix](http://gimmix.berlios.de)
      - [Rapid Photo Downloader](https://www.damonlynch.net/rapid)
  - Na tomto seznamu se ještě pracuje a není kompletní

<h2 style="padding:0.2em 0.4em; margin:10px 0 10px 0; border:1px solid #a3bfb1; background:#bcee68; color:#000; font-size:120%; font-weight:bold; text-align:left; -moz-border-radius:0.2em;">

Týmy

</h2>

  - Launchpad Czech Translators
  - GNOME Czech Translators

<h2 style="padding:0.2em 0.4em; margin:10px 0 10px 0; border:1px solid #a3bfb1; background:#bcee68; color:#000; font-size:120%; font-weight:bold; text-align:left; -moz-border-radius:0.2em;">

Pískoviště

</h2>

  - V této sekci momentálně nemám nic rozdělaného
