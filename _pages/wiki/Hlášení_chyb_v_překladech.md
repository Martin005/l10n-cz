---
layout: page
title: Hlášení chyb v překladech
permalink: /wiki/Hlášení_chyb_v_překladech/
redirect_from:
  - /Hlášení_chyb_v_překladech/
nav_order: 1
---

{% include l10n-cz/table-of-contents.html %}

### Kde chybu nahlásit

Pokud narazíte na špatný nebo nekvalitní překlad, můžete pomoci s jeho
napravením dokonce i když sami nepřekládáte. Nahlaste nalezenou chybu a
překladatelské týmy se postarají, aby byla opravena.

Adresu, kde můžete chybu oznámit, lze obvykle nalézt v informacích
o verzi nebo autorech dotyčného programu. Zde jsou vybrané odkazy:

  - [Debian](https://lists.debian.org/debian-l10n-czech/) (poštovní
    konference pro české překlady)
  - GNOME: na
    [GitLabu](https://gitlab.gnome.org/Teams/Translation/cs/issues) nebo
    do [české konference
    překladatelů](https://mail.gnome.org/mailman/listinfo/gnome-cs-list)
  - [KDE](https://bugs.kde.org/enter_bug.cgi?product=i18n&component=cs)
  - LibreOffice: do [konference
    lokalizace@cz.libreoffice.org](https://listarchives.libreoffice.org/cz/lokalizace/)
    (česky) nebo do
    [Bugzilly](https://bugs.documentfoundation.org/enter_bug.cgi?product=LibreOffice&component=Localization)
    (anglicky)
  - Mozilla: do
    [Bugzilly](https://bugzilla.mozilla.org/enter_bug.cgi?product=Mozilla%20Localizations&component=cs%20/%20Czech)
    (česky)
  - [Mageia](https://forum.mageia.cz/index.php/board,27.0.html)
  - [OpenOffice](https://bz.apache.org/ooo/enter_bug.cgi?product=Native-Lang&component=cs)
  - [Red Hat](https://bugzilla.redhat.com/)
  - [SUSE](https://bugzilla.novell.com/)
  - [Translation Project
    (GNU)](https://sourceforge.net/projects/translation/lists/translation-team-cs)
  - [Ubuntu](https://bugs.launchpad.net/ubuntu/+source/language-pack-cs/+filebug)

Chcete zde mít umístěn i odkaz na Bugzillu svého projektu? Není nic
jednoduššího než odkaz vložit.

### Jak správně nahlásit chybu v překladu

Je-li chyba hlášena do systému hlášení chyb většího projektu, je vhodné
a většinou nutné použít angličtinu. V popisu by neměl chybět přesný
název programu nebo jeho části, pokud je hlásícímu znám. Pokud nikoliv,
měl by být poskytnut přesný postup, jak lze chybu zobrazit. Neměla by
také chybět informace o verzi programu, jeho části, či o pracovním
prostředí a operačním systému, ve kterém je program s chybou
provozován. Přiložení snímku obrazovky zachycujícího chybu není
vyžadováno, ale napomůže opravujícímu chybu identifikovat.
