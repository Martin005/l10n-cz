---
layout: page
title: Aspell
permalink: /wiki/Kontrola_pravopisu/Aspell/
redirect_from:
  - /Aspell/
parent: Kontrola pravopisu
---

**GNU Aspell** je svobodný nástroj pro kontrolu pravopisu. Vznikl jako
náhrada za [Ispell]({% link _pages/wiki/Kontrola_pravopisu/Ispell.md %}).

## Externí odkazy

  - [Domovská stránka GNU Aspell](http://aspell.net/)
