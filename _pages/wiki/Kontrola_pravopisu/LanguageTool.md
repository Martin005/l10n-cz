---
layout: page
title: LanguageTool
permalink: /wiki/Kontrola_pravopisu/LanguageTool/
redirect_from:
  - /LanguageTool/
parent: Kontrola pravopisu
---

**LanguageTool** je svobodný nástroj pro kontrolu gramatiky a pravopisu.
Chyby jsou odhalovány na základě vytvářených pravidel. V tuto chvíli
bohužel nepodporuje češtinu, ale má např. podporu slovenštiny.

LanguageTool je jako samostatný nástroj, webová aplikace, i jako modul
či doplněk do mnoha programů včetně LibreOffice nebo Firefoxu.

## Externí odkazy

  - [Domovská stránka LanguageTool](https://languagetool.org/)
  - [Podpora slovenštiny pro
    LanguageTool](https://community.languagetool.org/?lang=sk)
  - [Přehled pro vývojáře
    LanguageTool](https://dev.languagetool.org/development-overview)
  - [Jak přidat nový jazyk do
    LanguageTool](https://dev.languagetool.org/adding-a-new-language)
  - [LanguageTool na
    GitHubu](https://github.com/languagetool-org/languagetool)
