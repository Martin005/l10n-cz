---
layout: page
title: Ispell
permalink: /wiki/Kontrola_pravopisu/Ispell/
redirect_from:
  - /Ispell/
parent: Kontrola pravopisu
---

**Ispell** je svobodný nástroj pro kontrolu pravopisu. Jako náhrada za
něj vznikl [Aspell]({% link _pages/wiki/Kontrola_pravopisu/Aspell.md %}).

## Externí odkazy

  - [Domovská stránka Ispell](https://www.cs.hmc.edu/~geoff/ispell.html)
