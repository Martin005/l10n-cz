---
layout: page
title: Nový slovník
permalink: /wiki/Kontrola_pravopisu/Nový_slovník/
redirect_from:
  - /Kontrola_pravopisu:Nový_slovník/
parent: Kontrola pravopisu
---

{% include l10n-cz/table-of-contents.html %}

## Situace konec roku 2020

  - Hunspell pod lincencí CC0:
    <https://gitlab.com/strepon/czech-cc0-dictionaries> pro LibreOffice
    a aplikace Mozilly
  - [přednáška Stanislava Horáčka na
    OpenAltu 2020](https://youtu.be/b1kQJs4Y1tg)

## Situace podzim 2019

  - Český tvarotvorný slovník (licence Public Domain):
    <https://github.com/plin/slovnik>
  - České CC0 slovníky: <https://ceskeslovniky.cz/>

Na serveru YouTube je k dispozici [přednáška Stanislava
Horáčka](https://www.youtube.com/watch?v=5LITQ8Ygtzo) z konference
LinuxDays 2019, která shrnuje vývoj kolem nového otevřeného Českého
tvarotvorného slovníku a představuje na něj navazující projekt
ceskeslovniky.cz.

Viz také
[zprávička]({% post_url 2019-10-13-novy-cesky-slovnik-pro-kontrolu-pravopisu %}).

**Informace o slovníku také v [padu
OpenAltu](https://pad.openalt.org/om_korektor).**

## Situace podzim 2018

Během [setkání na
LinuxDays 2018]({% post_url 2016-10-13-report-z-linuxdays %}) jsme
jako jeden z hlavních problémů identifikovali nevyhovující stav
standardních open source slovníků pro kontrolu překlepů a skutečnost, že
je je šířen pod [copyleftovou](https://en.wikipedia.org/wiki/Copyleft)
licencí ([GNU GPL](https://www.gnu.org/licenses/gpl-2.0.html)). Současný
slovník je neaktuální, neudržovaný a nemá žádného správce. [Ispellový
slovník](ftp://ftp.tul.cz/pub/unix/ispell/) slouží/posloužil jako základ
[aspellovému slovníku](ftp://ftp.gnu.org/gnu/aspell/dict/cs/). Z
aspellové verze jsou generovány ostatní formáty (myspell, hunspell).
Netrpí tak jen open source projekty, jejichž lokalizaci zajišťují
komunitní týmy, ale všechny projekty a produkty, které tyto slovníky
používají (např. kontrola pravopisu na Email.cz).

Jako vhodné řešení se jeví vytvořit nový slovník. Komunita překladatelů
L10N.cz tak hledá zdroj dat pro nový slovník, kterým by mohla být data
ÚJČ AV ČR nebo některá z českých vysokých škol. Nový slovník by
komunita překladatelů L10N.cz chtěla uvolnit pod svobodnější licencí
(nikoliv copyleft) a počítá se i s možností zpětné vazby a zadávání
nových dat od překladatelů i koncových uživatelů.

## Skupina komunitních překladatelů L10n.cz

Skupina komunitních překladatelů L10n.cz je
neformálním uskupením technických překladatelů a přispěvatelů do open
source projektů. Cílem skupiny je zpřístupnění velkého množství open
source softwaru rozličné povahy a zaměření českým uživatelům a
přizpůsobení tohoto softwaru českému prostředí. Mezi oblasti zájmu
skupiny tak patří i otevřené české slovníky a lexikografické databáze.
Skupina L10n.cz je pro potřeby tvorby nových otevřených lexikografických
databází a slovníků zaštítěna spolkem [OpenAlt
z.s.](https://www.openalt.org/o-spolku).

## Cíle

Pro softwarové projekty a zainteresované produkty plánuje komunita
překladatelů L10N.cz získat přístup k databázím, které umožní tvorbu a
distribuci otevřených slovníků, jejichž zaměření a vlastnosti jsou
podrobněji rozebrány v následujících sekcích. Mezi zainteresované
produkty mohou patřit komunitní i komerční, open source i proprietární
řešení, pokud to licenční podmínky budou umožňovat.

Poskytnutí otevřených dat ve formě slovníků co nejširšímu okruhu zájemců
a konzumentů umožní překonat zjevnou technologickou propast mezi
dostupnými otevřenými českými slovníky a nabídkou některých komerčních
dodavatelů proprietárního kancelářského softwaru.

### Slovník pro kontrolu překlepů

Základní slovník pro kontrolu překlepů založený na otevřené databázi,
který bude:

  - průběžné aktualizovaný,
  - bude svým obsahem více vyhovovat současným potřebám českých
    uživatelů,
  - dostupný pod svobodou a otevřenou licencí, která umožní jeho další
    údržbu a přebírání do co nejširšího okruhu projektů a produktů
    (tedy méně restriktivní licencí než copyleft),
  - zcela či částečně automatizovaně generovat ve všech potřebných
    formátech ze společného zdroje.

### Další slovníky

Mimo vytvoření a distribuci základního slovníku pro kontrolu překlepů je
dalším cílem tvorba slovníku synonym a kontroly gramatiky. Tyto by měly
být sémanticky založené a splňovat stejné obsahové a licenční požadavky
jako slovník pro kontrolu překlepů.

### Vývoj a údržba slovníků

Pro údržbu slovníku plánuje komunita překladatelů L10N.cz nasadit
nástroj pro sběr zpětné vazby od uživatelů, který umožní jednoduše
lexikografické návrhy spravovat a slovník jimi aktualizovat, nebo naopak
z něj lexikografická data odebírat. Možnosti implementace jsou:

  - webová stránka s formulářem,
  - automatický program či skript na odeslání lexikografických dat
    zadaných uživatelem do osobního slovníku v jednotlivých programech,
  - softwarová rozšíření pro nejpoužívanější software.

### Co může skupina L10n.cz nabídnout spolupracující instituci?

Zásadním pro naplnění výše předestřených cílů je aktivní a intenzivní
spolupráce s některou z institucí, které mají přístup k potenciálním
zdrojům dat pro otevřené databáze a slovníky.

Členové skupiny L10n.cz mohou spolupracující instituci nabídnout bohaté
zkušenosti s managementem otevřených dat, znalosti v oblasti zpracování
přirozeného jazyka, softwarové lokalizace a inženýrství a systémové
administrace, dále pak kontakty na a zapojení do práce velkého množství
mezinárodních technických komunit zabývajících se vývojem open source
softwaru.

### Potenciální zdroje dat pro databáze

  - ÚJČ AV ČR
  - další akademická pracoviště a jejich projekty (např.
    <https://www.korpus.cz/>)
  - [Wikislovník](https://cs.wiktionary.org/)
  - další existující slovníky, viz [stránka Pomůcky pro
    překladatele]({% link _pages/wiki/Pomůcky_pro_překladatele.md %})
  - volně dostupná literární díla v digitalizované podobě

### Očekávaný průběh získání slovníku a použití

  - postupné oslovení jednotlivých zdrojů (viz výše) a zjištění
    dostupnosti dat pod vhodnou licencí
  - implementace nástrojů pro zpracování dat a vygenerování slovníku
  - uvedení slovníku do produktů
  - implementace nástrojů pro získávání návrhů od uživatelů a jejich
    správu
  - následná údržba a doplňování z návrhů uživatelů a případně z
    aktualizovaných zdrojových dat

## Další odkazy

  - [Výzva v konferenci ze
    dne 10. 8. 2009](https://lists.ubuntu.cz/pipermail/diskuze/2009-August/000730.html)
  - [Diskuze po
    LinuxDays](https://lists.l10n.cz/pipermail/diskuze/2016-October/002244.html)
  - [Verze slovníku pro ispell od Tomáše
    Vondry](https://github.com/tvondra/ispell_czech)
  - [Whitelist slov z překladu
    KDE](https://websvn.kde.org/trunk/l10n-support/pology/lang/cs/),
    ([diskuze](https://lists.l10n.cz/pipermail/diskuze/2016-October/002270.html))
  - [Slovník OpenOffice.org na webu
    Liberix.cz](https://liberix.cz/doplnky/slovniky/ooo/)
  - [ÚJČ AV ČR](https://www.ujc.cas.cz/)
  - [Wikislovník](https://cs.wiktionary.org/wiki/Wikislovn%C3%ADk:Hlavn%C3%AD_strana)
  - [Eurovoc, mnohojazyčný tezaurus Evropské
    unie](https://op.europa.eu/en/web/eu-vocabularies)
