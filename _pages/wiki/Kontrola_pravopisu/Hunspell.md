---
layout: page
title: Hunspell
permalink: /wiki/Kontrola_pravopisu/Hunspell/
redirect_from:
  - /Hunspell/
parent: Kontrola pravopisu
---

**Hunspell** je svobodný nástroj pro kontrolu pravopisu. Využívá ho
např. balík LibreOffice, většina webových prohlížečů a svobodného
softwaru. Původně vznikl pro maďarštinu a je zpětně kompatibilní se
slovníky pro starší MySpell.

## Externí odkazy

  - [Domovská stránka Hunspellu](https://hunspell.github.io/)
  - [Hunspell na GitHubu](https://github.com/hunspell/hunspell)
