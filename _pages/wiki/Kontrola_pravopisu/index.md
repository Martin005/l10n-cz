---
layout: page
title: Kontrola pravopisu
permalink: /wiki/Kontrola_pravopisu/
redirect_from:
  - /Kontrola_pravopisu/
  - /Kategorie:Kontrola_pravopisu/
has_children: true
nav_exclude: true
---

Kontrola pravopisu je základní a o to užitečnější nástroj překladatele.
Odhalí jednoduché chyby a překlepy, které vznikají z nepozornosti,
rychlým psaním nebo úpravami už přeloženého textu.

Níže uvedené nástroje jsou spíše knihovny. Jak zapnout kontrolu
pravopisu v konkrétním offline nástroji (a při použití webových nástrojů
ve vašem prohlížeči) se podívejte do jeho nastavení nebo návodu.
