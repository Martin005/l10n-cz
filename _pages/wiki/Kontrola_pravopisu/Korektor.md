---
layout: page
title: Korektor
permalink: /wiki/Kontrola_pravopisu/Korektor/
redirect_from:
  - /Korektor/
parent: Kontrola pravopisu
---

**Korektor** je online nástroj pro kontrolu pravopisu a diakritiky
vytvořený na MatFyz UK.

## Externí odkazy

  - [Stránky Korektoru](https://ufal.mff.cuni.cz/korektor)
  - [Online verze](https://lindat.mff.cuni.cz/services/korektor/)
  - [Doplněk pro
    Firefox](https://addons.mozilla.org/cs/firefox/addon/korektor-spellchecker/)
  - [Doplněk pro Google
    Chrome](https://chrome.google.com/webstore/detail/korektor-spellchecker/clbjpfehpgcfakbadjendaebmhooiecl)
