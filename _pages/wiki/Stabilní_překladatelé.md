---
layout: page
title: Stabilní překladatelé
permalink: /wiki/Stabilní_překladatelé/
redirect_from:
  - /Stabilní_překladatelé/
nav_exclude: true
---

{% include l10n-cz/table-of-contents.html %}

Existují překladatelé (mimo [překladatelské
týmy]({% link _pages/wiki/Překladatelské_týmy.md %})), kteří si "svůj" program
hlídají a pravidelně jeho překlad aktualizují. Zapisujte abecedně podle
názvu programu.

## A

  - [Audacious](https://audacious-media-player.org/): Roman Horník,
    [Transifex](https://www.transifex.com/audacious/audacious/)

## B

  - [BleachBit](https://www.bleachbit.org/): Roman Horník,
    [Launchpad](https://translations.launchpad.net/bleachbit)
  - [Bluefish](https://bluefish.openoffice.nl/): Marek Černocký

## D

  - [Decentraleyes](https://decentraleyes.org/): Michal Stanke,
    [Crowdin](https://crowdin.com/project/decentraleyes)
  - [Django](https://www.djangoproject.com/): Vladimír Macek
    <macek AT scripteo DOT cz>, django-i18n@googlegroups.com (anglicky
    pro všechny l10n),
    [Transifex](https://www.transifex.com/django/django/)

## E

  - [Evolution](https://wiki.gnome.org/Apps/Evolution): Jiří Eischmann,
    viz [GNOME]({% link _pages/wiki/Překladatelské_týmy.md %}#gnome)

## F

  - [FileZilla Client 3](https://filezilla-project.org/): Michal
    Molhanec <michal AT molhanec DOT net>,
    [FileZilla-project.org](https://filezilla-project.org/translations.php)

## G

  - [GIMP Animation Package](https://www.gimp.org/): viz
    [GNOME]({% link _pages/wiki/Překladatelské_týmy.md %}#gnome)

## I

  - [iD Editor](https://ideditor.com/): Roman Horník,
    [Transifex](https://www.transifex.com/openstreetmap/id-editor/)

## M

  - [MATE Desktop Environment](https://mate-desktop.org/): Roman Horník,
    [Transifex](https://www.transifex.com/mate/MATE/)
  - [MKVToolNix](https://mkvtoolnix.download/): Roman Horník,
    [Transifex](https://www.transifex.com/moritz-bunkus/mkvtoolnix/)

## N

  - [NFO Viewer](https://otsaloma.io/nfoview/): Roman Horník,
    [Transifex](https://www.transifex.com/otsaloma/nfoview/)

## O

  - [OSMbot](https://github.com/Xevib/osmbot): Roman Horník,
    [Transifex](https://www.transifex.com/osm-catala/osmbot/)
  - [OSMTracker for
    Android™](https://github.com/nguillaumin/osmtracker-android):
    Roman Horník,
    [Transifex](https://www.transifex.com/labexp/osmtracker-android/)

## P

  - [pgAdmin](https://www.pgadmin.org/): Marek Černocký
  - [Pidgin](https://pidgin.im/): Roman Horník,
    [Transifex](https://www.transifex.com/pidgin/pidgin/)

## R

  - [Rapid Photo Downloader](https://www.damonlynch.net/rapid/): Tomáš
    Novák, [Launchpad](https://translations.launchpad.net/rapid)
  - [Rawtherapee](https://rawtherapee.com/): Marián Kyral,
    <https://discuss.pixls.us/t/localization-how-to-translate-rawtherapee-and-rawpedia/2594>

## S

  - [Scribes](https://scribes.sourceforge.net/index.html): Petr Pulc
    \<petrpulc@gmail.com\>,
    [Launchpad](https://translations.launchpad.net/scribes)
  - [Scribus](https://www.scribus.net/): Vlastimil Ott,
    [Transifex](https://www.transifex.com/scribus/scribus/)
  - [Spek](http://spek.cc/): Roman Horník,
    [Transifex](https://www.transifex.com/spek/spek/)

## T

  - [Tor]({% link _pages/wiki/Tor.md %}): [tým
    Mozilla]({% link _pages/wiki/Překladatelské_týmy.md %}#mozilla),
    [Transifex](https://www.transifex.com/otf/torproject/)

## V

  - [VLC](https://www.videolan.org/vlc/): Tomáš Chvátal, [VLC
    i18n](https://www.videolan.org/developers/i18n/),
    [Transifex](https://www.transifex.com/projects/p/vlc-trans/)

## X

  - [Xfce](https://docs.xfce.org/contribute/translate/start): Michal
    Várady \<miko.vaji@gmail.com\>, [Xfce
    Docs](https://docs.xfce.org/contribute/translate/start)

## Y

  - [youtube-dl-gui](https://github.com/MrS0m30n3/youtube-dl-gui): Roman
    Horník,
    [Transifex](https://www.transifex.com/youtube-dl-gui/youtube-dl-gui/)

## Z

  - [Zim](https://zim-wiki.org): Vlastimil Ott,
    [Launchpad](https://translations.launchpad.net/zim)
