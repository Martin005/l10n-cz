---
layout: page
title: Překladatelské týmy
permalink: /wiki/Překladatelské_týmy/
redirect_from:
  - /Překladatelské_týmy/
  - /Puppy_Linux/
  - /Kategorie:Mozilla/
nav_order: 2
---

{% include l10n-cz/table-of-contents.html %}

## Debian

**Web**: <https://www.debian.cz/develop/translation.php>,
<https://www.debian.cz/develop/people.php>

**Konference**: debian-l10n-czech zavináč lists.debian.org
(https://lists.debian.org/debian-l10n-czech/)

**Statistiky**:
<https://www.debian.org/international/l10n/po-debconf/cs>

**Koordinátor týmu**: Michal Šimůnek, michal.simunek zavináč gmail.com

## Drupal

**Web**: <https://www.drupal.cz/clanky/preklady-lokalizace-drupalu>

**Konference**: <https://www.drupal.cz/kontakt> (Slack)

**Statistiky a aplikace**:
<https://localize.drupal.org/translate/languages/cs/>

## Fedora

**Web**: <https://fedoraproject.org/wiki/L10N_Czech_Team>

**Konference**: cs-users zavináč lists.fedoraproject.org
(https://lists.fedoraproject.org/pipermail/cs-users/)

**Statistiky** ([Weblate]({% link _pages/wiki/Překladatelský_software/Weblate.md %})):
<https://translate.fedoraproject.org/languages/cs/>

**Koordinátor týmu**: Adam Přibyl, pribyl zavináč lowlevel.cz

## GNOME

**Web**: <https://wiki.gnome.org/Czech>

**Konference**: gnome-cs-list zavináč gnome.org
(https://mail.gnome.org/mailman/listinfo/gnome-cs-list)

**Statistiky**: <https://l10n.gnome.org/languages/cs/>

**Koordinátor týmu**: Marek Černocký, marek at manet dot cz

[Více informací o překládání GNOME]({% link _pages/wiki/Návody/Jak_překládat_Gnome.md %}).

## Joomla

**Web**: <https://www.joomlaportal.cz/>

**Kontakt**: <https://www.joomlaportal.cz/index.php/kontakt>,
<https://www.joomlaportal.cz/forum>

**Statistiky a aplikace** ([Crowdin]({% link _pages/wiki/Překladatelský_software/Crowdin.md %})):
<https://crowdin.com/project/joomla-cms>

## KDE

**Web**: <https://l10n.kde.org/team-infos.php?teamcode=cs>,
<https://kde-czech.sourceforge.net> (staré stránky, neaktuální)

**Konference**: kde-czech-apps zavináč lists.sourceforge.net
(https://sourceforge.net/projects/kde-czech/lists/kde-czech-apps)

**Statistiky**: <https://l10n.kde.org/stats/gui/trunk-kf5/team/cs/>

**Koordinátor týmu**: Vít Pelčák, vit zavináč pelcak.org

[Více informací o překládání KDE]({% link _pages/wiki/Návody/Jak_překládat_Kde.md %}).

## Launchpad Translators

**Web**: <https://launchpad.net/~lp-l10n-cs>,
<https://translations.launchpad.net/+languages/cs>,
<https://help.launchpad.net/Translations/Czech>

**Konference**: lp-l10n-cs zavináč lists.launchpad.net (e-mailový alias)

**Statistiky**: Na stránkách překladů jednotlivých projektů.

**Koordinátor týmu**: Tomáš Novák

## LibreOffice

**Konference**: lokalizace zavináč cz.libreoffice.org
([info](https://wiki.documentfoundation.org/Local_Mailing_Lists#Czech_.2F_.C4.8Ce.C5.A1tina),
[archiv](https://listarchives.libreoffice.org/cz/lokalizace/))

**Webová aplikace** ([Weblate]({% link _pages/wiki/Překladatelský_software/Weblate.md %})):
<https://weblate.documentfoundation.org/languages/cs/>

**Kontakt**: Stanislav Horáček, stanislav.horacek zavináč gmail.com

## Linux Mint

**Web**: <https://translations.launchpad.net/linuxmint/latest/+lang/cs>

**Koordinátor týmu**: [Pavel
Borecki](https://launchpad.net/~pavel-borecki)

## Mageia

**Web**:
<https://wiki.mageia.cz/wiki:rozcesti_wikeru#rozcesti_prekladatelu>

**Webová aplikace** ([Transifex]({% link _pages/wiki/Překladatelský_software/Transifex.md %})):
<https://www.transifex.com/MageiaLinux/mageia/>

## Mozilla

**Web**: <https://www.mozilla.cz/>

**Kontakt**: <https://wiki.mozilla.org/L10n:Teams:cs>

**Statistiky a aplikace** ([Pontoon]({% link _pages/wiki/Překladatelský_software/Pontoon.md %})):
<https://pontoon.mozilla.org/cs/>

[Více informací](https://www.mozilla.cz/zapojte-se/lokalizace/)

[Překlad Toru]({% link _pages/wiki/Tor.md %})

## openSUSE

**Web**: <https://cs.opensuse.org/Lokalizace> a
<https://en.opensuse.org/openSUSE:Localization_guide>

**Konference**: opensuse-cz zavináč opensuse.org
(https://lists.opensuse.org/opensuse-cz/) a opensuse-translation zavináč
opensuse.org (https://lists.opensuse.org/opensuse-translation/)

**Statistiky** ([Weblate]({% link _pages/wiki/Překladatelský_software/Weblate.md %})):
<https://l10n.opensuse.org/languages/cs/>

**Koordinátor týmu**: Vojtěch Zeisek, vojtech.zeisek zavináč
opensuse.org a Jan Papež, honyczek zavináč centrum.cz

## Translation Project

**Web**: <https://translationproject.org/team/cs.html>

**Konference**: translation-team-cs zavináč lists.sourceforge.net
(https://sourceforge.net/projects/translation/lists/translation-team-cs)

**Statistiky**: <https://translationproject.org/team/cs.html>

**Koordinátor týmu**: Vladimír Michl, vladimir.michl zavináč seznam.cz

## Ubuntu

**Web**: <https://launchpad.net/~ubuntu-l10n-cs>,
<https://wiki.ubuntu.cz/lokalizace>

**Statistiky** ([Launchpad Rosetta]({% link _pages/wiki/Překladatelský_software/Launchpad_Rosetta.md %})):
<https://translations.launchpad.net/ubuntu/+translations>

**Koordinátor týmu**: Martin Šácha, sachy zavináč s0c4.net

## WordPress

**Web**: <https://cs.wordpress.org/preklady/>

**Konference** (Slack): <https://cs.wordpress.org/preklady/slack/>

**Statistiky a aplikace** ([GlotPress]({% link _pages/wiki/Překladatelský_software/GlotPress.md %})):
<https://translate.wordpress.org/locale/cs>

**Koordinátor týmu**: Michal Janata -
<https://profiles.wordpress.org/kalich5/>

[Více informací o překládání
WordPressu](https://cs.wordpress.org/preklady/#zdroje-pro-preklad)

## Další záznamy

  - Elementary OS: [Weblate](https://l10n.elementary.io/languages/cs/)
  - Jolla: [Twitter](https://twitter.com/Jolla_CZ_SK),
    [Pootle](https://translate.sailfishos.org/cs/)
  - LXQt: [Weblate](https://translate.lxqt-project.org/languages/cs/)
  - NextCloud: [fórum](https://help.nextcloud.com/c/translations),
    [Transifex](https://www.transifex.com/nextcloud/nextcloud/)
  - ownCloud: [fórum](https://central.owncloud.org/categories),
    [Transifex](https://www.transifex.com/owncloud-org/owncloud/)
  - Pale Moon: [Crowdin](https://crowdin.com/project/pale-moon)
  - phpBB.cz: [Tým](https://www.phpbb.cz/tym)
  - Vivaldi:
    [fórum](https://forum.vivaldi.net/category/70/%C4%8Desky-czech)
  - ...další [Stabilní překladatelé]({% link _pages/wiki/Stabilní_překladatelé.md %})
