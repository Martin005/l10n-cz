---
layout: page
title: Související projekty
permalink: /wiki/Související_projekty/
redirect_from:
  - /Související_projekty/
nav_exclude: true
---

{% include l10n-cz/table-of-contents.html %}

V tomto prostoru můžete představit projekt, na kterém pracujete,
popřípadě vložit odkaz. Předpokládá se, že váš projekt má něco
společného s lokalizací.
Zjednodušeně lze tuto sekci označit jako bezplatnou inzertní plochu pro
projekty, které zde o sobě chtějí dát vědět, popřípadě hledají
spolupracovníky.
Informaci můžete vložit přímo na tuto stránku, popřípadě vytvořit
zvláštní stránku na wiki a sem jen vložit odkaz. <strong>Nejnovější
záznam by měl být vždy nahoře.</strong>

---

### Slovník synonym

#### Nový slovník v LibreOffice

V LibreOffice je od verze 5.2 (léto 2016) nový slovník synonym
vygenerovaný z [GNU/FDL Anglicko-českého
slovníku](http://slovnik.zcu.cz/uvod.php) (viz [příslušné soubory v
repozitáři](https://cgit.freedesktop.org/libreoffice/dictionaries/tree/cs_CZ/thesaurus)).
Zájemci o další rozvoj a vylepšování tohoto slovníku jsou vítáni\!

#### Výzva z roku 2007

Rád bych vytvořil volně dostupný slovník synonym (thesaurus). Bohužel
narazil jsem na jisté problémy, které jsem nebyl schopen triviálně
vyřešit, a ostatní zájmy mi odsunuly projekt do pozadí. Hledám tedy
někoho, kdo by byl ochoten pomoci; nejlépe s většími zkušenostmi s PHP
a dobrými znalostmi češtiny - ani jedno však není nutnou podmínkou
(je-li ochota se učit).

Zájemci nechť se ozvou mailem na <kavol AT SPAMFREE seznam DOT cz> -
prosím jen vážní, půjde o hodně práce.

Vloženo 30.7.2007

### [Správa českého slovníku]({% link _pages/wiki/Kontrola_pravopisu/Nový_slovník.md %})

### KDE l10n tools

Repozitář se skripty, které používá překladatelský tým KDE na správu
překladů, lze nalézt na adrese:
<https://github.com/vpelcak/kde-scripts>

### Úplná česká klávesnice pro X11

[V současnosti](https://cgit.freedesktop.org/xkeyboard-config/tree/symbols/cz)
(xkeyboard-config-1.6) české rozložení klávesnice pro X11 neumožňuje
zapsat všechny znaky vyskytující se v běžném českém textu (například
uvozovky „ “ nebo pomlčku –). Naopak nabízí znaky, které se v češtině
vůbec nevyskytují.

Tento neutěšený stav vedl ke vzniku několika nezávislých úprav, které
měly uspokojit své autory:

  - [Cohen: „Typografické“ rozložení
    klávesnice](https://www.abclinuxu.cz/blog/Drobnosti/2007/9/-typograficke-rozlozeni-klavesnice)
  - [Petr Písař: xkb-cz-typo](http://xpisar.wz.cz/xkb-cz-typo/)
  - [Vogo: vok – us/cz typografická klávesnice a vok_sk
    (v 1.0)](https://www.abclinuxu.cz/blog/origami/2006/12/21/162644)

Vzhledem k tomu, že dosud nedošlo k širší shodě, nebyly tyto změny
zapracovány do hlavního Xorg stromu. Jakékoliv snahy tímto směrem jsou
vítány.

#### Návrh změn

Na základě
[diskuze](https://lists.ubuntu.cz/pipermail/diskuze/2009-June/000676.html)
vznikl následující návrh, jak upravit českou klávesovou mapu:

Klávesy jsou uspořádány tak, jak jdou na klávesnici PC-105 ve směru
čtení.

| Klávesa (základní anglický symbol) | Žádný modifikátor | Shift             | AltGr                             | AltGr-Shift                       |
| ---------------------------------- | ----------------- | ----------------- | --------------------------------- | --------------------------------- |
| 2 (2)                              |                   |                   |                                   | druhá mocnina (²)                 |
| spojovník (-)                      |                   |                   | minus (−)                         |                                   |
| S (s)                              |                   |                   | ostré es (ß)                      |                                   |
| D (d)                              |                   |                   | malé přeškrtnuté d (đ)            |                                   |
| středník (;)                       |                   |                   | dvojité otevírací uvozovky („)    | dvojité uzavírací uvozovky (“)    |
| apostrof (')                       |                   |                   | jednoduchá otevírací uvozovka (‚) | jednoduchá uzavírací uvozovka (‘) |
| tečka (.)                          |                   |                   |                                   | výpustka (…)                      |
| lomítko (/)                        |                   |                   | n-pomlčka (–)                     | m-pomlčka (—)                     |
| mezerník ( )                       |                   | zúžená mezera ( ) | nezlomitelná mezera ( )           | nezlomitelná zúžená mezera ( )    |

Tyto znaky byly předneseny, ale zatím se pro ně nenašlo místo:

  - Třetí mocnina – U+00B3 (³) SUPERSCRIPT THREE

Následující znaky již ve stávající mapě existují:

  - Český apostrof – U+2019 (’) RIGHT SINGLE QUOTATION MARK –
    AltGr-Shift-b

### Jabberová místnost lokalizace@jabber.divido.cz

Vlastimil Ott
[nabídl](https://lists.ubuntu.cz/pipermail/diskuze/2009-March/000156.html)
pro rychlejší výměnu názorů již existující jabberovou místnost
*lokalizace* na serveru *jabber.divido.cz*.
