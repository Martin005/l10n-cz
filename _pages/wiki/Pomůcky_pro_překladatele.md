---
layout: page
title: Pomůcky pro překladatele
permalink: /wiki/Pomůcky_pro_překladatele/
redirect_from:
  - /Pomůcky_pro_překladatele/
nav_order: 5
---

{% include l10n-cz/table-of-contents.html %}

V rámci projektu L10N.cz je udržován společný [Překladatelský
slovník]({% link _pages/wiki/Slovníky/Překladatelský_slovník.md %}) a [Stylistická
příručka]({% link _pages/wiki/Slovníky/Stylistická_příručka.md %}). Slovníky jednotlivých
[překladatelských týmů]({% link _pages/wiki/Překladatelské_týmy.md %}) naleznete na
[zvláštní stránce]({% link _pages/wiki/Slovníky/index.md %}).

Neváhejte na tuto stránku přidat další zdroje, které mohou usnadnit
překladatelům práci při lokalizaci.

## Online překladové slovníky

Překladové slovníky (vícejazyčné) můžete použít pro dohledání českého
(nebo jiného překladu) jednotlivých slov nebo krátkých sousloví.

  - [Google Překladač](https://translate.google.com/?tl=cs)
  - [Lingea.cz](https://slovniky.lingea.cz/)
  - [Seznam.cz](https://slovnik.seznam.cz/)
  - [Slovnik.cz](https://www.slovnik.cz/)
  - [Web MetaTrans](https://metatrans.fi.muni.cz/) – agreguje několik
    slovníků

## Online výkladové slovníky

Výkladové slovník (jednojazyčné) slovníky se hodí pro dohledání definice
a přesného významu slova v daném jazyce.

### Čeština

  - [Encyklopedický slovník češtiny](https://www.czechency.org/slovnik/)
  - [Neologismy \| Neomat](http://neologismy.cz/)
  - [Čeština 2.0 - zajímavá slova ze všech zákoutí mateřského
    jazyka](https://cestina20.cz/)
  - [Slo(v)níček Péhápkáři.cz](https://pehapkari.cz/slovnicek/)
  - [Výkladový slovník ABC Linuxu](https://www.abclinuxu.cz/slovnik)
  - [Anglicko - český / česko - anglický slovník matematické
    terminologie](https://www.umat.feec.vutbr.cz/~novakm/slovnik_matematicke_terminologie/index_cz.html)
  - [Matematický slovník](https://www.ped.muni.cz/wmath/dictionary/)
  - [Wikislovník](https://cs.wiktionary.org/)

### Angličtina

  - [OneLook Dictionary Search - vyhledávač ve více
    slovnících](https://www.onelook.com/)
  - [Cambridge Dictionary](https://dictionary.cambridge.org/)
  - [Longman Dictionary of Contemporary
    English](https://www.ldoceonline.com/)
  - [Merriam Webster](https://www.merriam-webster.com/)
  - [Oxford Dictionaries](https://www.oxforddictionaries.com/)
  - [Collins Dictionary](https://www.collinsdictionary.com/)
  - [Urban Dictionary](https://www.urbandictionary.com/)
  - [Wiktionary](https://en.wiktionary.org/)

### Angličtina (specializované)

  - [Free On-Line Dictionary Of Computing](https://foldoc.org/)
  - [The Free Dictionary](https://www.thefreedictionary.com/)
  - [Webopedia: Online Tech Dictionary for IT
    Professionals](https://www.webopedia.com/)

## Srovnávací slovníky

Srovnávací slovník, respektive překladový vyhledávač, slouží k dohledání
překladu konkrétního textu použitého v lokalizovaném projektu.

  - [amaGama.locamotion.org](https://amagama-live.translatehouse.org/)
  - [Eurovoc, mnohojazyčný tezaurus Evropské
    unie](https://op.europa.eu/en/web/eu-vocabularies)
  - [Microsoft Language
    Portal](https://www.microsoft.com/en-us/language/)
  - [Pontoon]({% link _pages/wiki/Překladatelský_software/Pontoon.md %}) Machinery a vyhledávání (Mozilla)
  - [Transvision]({% link _pages/wiki/Překladatelský_software/Transvision.md %}) (Mozilla)
  - [Translation Consistency -
    WordPress.org](https://translate.wordpress.org/consistency/)

## Stylistické příručky cizích projektů

viz [Stylistická příručka]({% link _pages/wiki/Slovníky/Stylistická_příručka.md %})

## Pravopis

Pravopisné příručky a korektory vám pomohou s dodržováním české
gramatiky, používáním interpunkce, tvarosloví a odhalí i obyčejné
překlepy.

  - [Kontrola pravopisu]({% link _pages/wiki/Kontrola_pravopisu/index.md %})
  - [Internetová jazyková příručka](https://prirucka.ujc.cas.cz/)
    (ÚJČ AV ČR)
  - [Jazyková poradna Ústavu pro jazyk český AV
    ČR](https://www.ujc.cas.cz/jazykova-poradna/)
  - [Morfologický analyzátor
    Ajka](https://nlp.fi.muni.cz/projects/wwwajka/WwwAjkaSkripty/morph.cgi)
    - webové rozhraní k české databázi (umí určit slovní druh, jeho
    kategorie a nalézt vzor, rozepsat slovo na předpony, kmen, přípony a
    koncovky)
  - [Morfo webové
    rozhraní](http://quest.ms.mff.cuni.cz/cgi-bin/zeman/morfo/index.pl)
    (pro lemma umí rozepsat skloňování)
  - [Pravidla českého pravopisu](https://www.pravidla.cz/) (neoficiální)
  - [Slovník spisovaného jazyka českého](https://ssjc.ujc.cas.cz/)

## Korpusy

Jazykové korpusy obsahují jazyková data a texty používaná pro výzkum
daného jazyka, tvorbu slovníků, automatických korektorů, překladačů
nebo jen obyčejnému vyhledávání příkladů vět a použití slov.

  - [Český národní korpus](https://korpus.cz/)
  - [DEB: Prohlížeč a editor
    slovníků](https://deb.fi.muni.cz/index-cs.php)
  - [Ústav Českého národního korpusu](https://ucnk.ff.cuni.cz/)
  - [SyD: Korpusový průzkum variant](https://syd.korpus.cz/)
