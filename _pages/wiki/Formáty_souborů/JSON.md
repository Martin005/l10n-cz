---
layout: page
title: JSON
permalink: /wiki/Formáty_souborů/JSON/
redirect_from:
  - /JSON/
parent: Formáty souborů
---

Soubory *.json* obecně slouží k ukládání a přenosu různých druhů dat,
hlavně u webových aplikací nebo JavaScriptu. Pro lokalizaci se používají
např. u doplňků do webových prohlížečů, z nichž Chrome extensions a
WebExtensions Firefoxu používají shodný formát souborů nazvaných
*messages.json*.

```json
{
  "extensionName": {
    "message": "Moje rozšíření",
    "description": "Name of the extension."
  },

  "notificationContent": {
    "message": "Klikli jste na odkaz $URL$.",
    "description": "Tells the user which link they clicked.",
    "placeholders": {
      "url" : {
        "content" : "$1",
        "example" : "`<https://developer.mozilla.org>`"
      }
    }
  }
}
```

## Editory a nástroje

  - [Textový editor]({% link _pages/wiki/Překladatelský_software/Textový_editor.md %})
  - použitelné mohou být i vývojářské nástroje prohlížečů
  - [Crowdin]({% link _pages/wiki/Překladatelský_software/Crowdin.md %})
  - [Transifex]({% link _pages/wiki/Překladatelský_software/Transifex.md %})

## Externí odkazy

  - [JavaScript Object Notation –
    Wikipedie](https://cs.wikipedia.org/wiki/JavaScript_Object_Notation)
  - [Locale-specific message reference - Mozilla |
    MDN](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/i18n/Locale-Specific_Message_reference)
  - [Formats: Locale-Specific Messages - Google
    Chrome](https://developer.chrome.com/extensions/i18n-messages)
