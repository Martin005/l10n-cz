---
layout: page
title: PHP array
permalink: /wiki/Formáty_souborů/PHP_array/
redirect_from:
  - /PHP_array/
parent: Formáty souborů
---

Array (česky pole) je datová struktura. Slouží obvykle pro ukládání
nějakých seznamů, v jazyce **PHP** má podobu mapy *klíč*
(identifikátor) *hodnota* (v našem případě původní nebo přeložený
text). I když se někdy používá pro lokalizaci, není to v pravém smyslu
lokalizační formát. Ve skutečnosti jde přímo o kus kódu v programovacím
jazyce PHP.

```php
<?php

$strings = array(
    'button.ok' => 'OK',
    'button.cancel' => 'Zrušit',
);

?>
```

## Editory

  - [Textový editor]({% link _pages/wiki/Překladatelský_software/Textový_editor.md %})

## Externí odkazy

  - [PHP: Arrays -
    Manual](https://php.net/manual/language.types.array.php)
  - [Pole - Český PHP manuál na
    ITnetwork.cz](https://www.itnetwork.cz/php/manual/pole/pole-cesky-php-manual)
  - [PHP - Pole - Linux
    Software](http://www.linuxsoft.cz/article.php?id_article=179)
  - [PHP pole - tvorba-webu.cz](https://www.tvorba-webu.cz/php/pole.php)
