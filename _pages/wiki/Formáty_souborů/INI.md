---
layout: page
title: INI
permalink: /wiki/Formáty_souborů/INI/
redirect_from:
  - /INI/
parent: Formáty souborů
---

Soubory typu *.ini* jsou velmi podobné formátu
[Properties]({% link _pages/wiki/Formáty_souborů/Properties.md %}). Používají se pro konfiguraci
aplikací. Obsah souboru *.ini* vypadá približně takto:

```ini
; comment text
[section]
button_ok = "OK"
button_cancel = "Cancel"
```

Soubor obsahuje na každém řádku přiřazení `IDENTIFIKATOR = "ŘETĚZEC"`.
Řádky začínající znakem „;“ (někdy „\#“) reprezentují komentáře, které
nepřekládáme. Pomocí popisků v hranatých závorkách je možné ještě soubor
dělit na sekce.

V souboru *.ini* překládáme pouze hodnoty (řetězce) v jednotlivých
přiřazeních, tedy na každém řádku vše za prvním znakem „=“, pokud
nejde o komentář.

## Editory

  - [Textový editor]({% link _pages/wiki/Překladatelský_software/Textový_editor.md %})

## Externí odkazy

  - [INI file - Wikipedia](https://en.wikipedia.org/wiki/INI_file)
