---
layout: page
title: XML
permalink: /wiki/Formáty_souborů/XML/
redirect_from:
  - /XML/
parent: Formáty souborů
---

značkovací jazyk *XML* slouží pro ukládání a přenos různých druhů dat.
Např. HTML pro tvorbu webových stránek je XML hodně podobné. Pro
překlady se XML používá v podobně formátu [XLIFF]({% link _pages/wiki/Formáty_souborů/XLIFF.md %}),
[TS]({% link _pages/wiki/Formáty_souborů/TS.md %}) nebo u aplikací pro Android, kde se překlady
ukládají do souborů *strings.xml*.

```xml
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <string name="appName" comment="Name of the app">Moje aplikace</string>        
    <plurals name="numberOfDays">
        <item quantity="one">jeden den</item>
        <item quantity="few">%d dny</item>
        <item quantity="other">%d dní</item>
    </plurals>
    <string-array name="daysOfTheWeek">
        <item>pondělí</item>
        <item>úterý</item>
        <item>středa</item>
        <item>čtvrtek</item>
        <item>pátek</item>
        <item>sobota</item>
        <item>neděle</item>
    </string-array>
</resources>
```

## Editory

  - [Textový editor]({% link _pages/wiki/Překladatelský_software/Textový_editor.md %})

## Externí odkazy

  - [Extensible Markup Language (XML) –
    Wikipedie](https://cs.wikipedia.org/wiki/Extensible_Markup_Language)
  - [Localize your app - Android
    Developers](https://developer.android.com/guide/topics/resources/localization)
  - [String resources - Android
    Developers](https://developer.android.com/guide/topics/resources/string-resource)
