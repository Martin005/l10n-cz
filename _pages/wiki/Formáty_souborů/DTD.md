---
layout: page
title: DTD
permalink: /wiki/Formáty_souborů/DTD/
redirect_from:
  - /DTD/
parent: Formáty souborů
---

Soubory typu DTD (Document Type Definition) slouží k popisu struktury
XML dokumentů a definování entit. Obsah souborů *.dtd* použitého pro
lokalizaci produktů Mozilly vypadá asi takto:

```xml
<!-- CONTEXT MENU -->
<!ENTITY context.options.label "Options...">
<!ENTITY context.options.accesskey "O">
```

Jedná se o sadu entit v kódování UTF-8, kterou můžeme jednoduše
editovat.

  - text mezi `<!--` `-->` je komentář, který nepřekládáme,
  - *context.options.label* a *context.options.accesskey* jsou názvy
    entit přiřazené v kódu aplikace/doplňku konkrétním textům,
  - a text „Options…“ a „O“ je to, co nás zajímá a co budeme překládat.

*Upozornění:* Při úpravách dejte pozor, aby byla zachována syntaktická
správnost souboru (validita) – např. zapomenuté uvozovky či neuzavřená
ostrá závorka („\>“) budou mít po instalaci fatální dopad na funkčnost
celé aplikace\!

Přeložené entity výše tedy budou vypadat takto:

```xml
<!-- CONTEXT MENU -->
<!ENTITY context.options.label "Možnosti...">
<!ENTITY context.options.accesskey "M">
```

Pozn.: Jednopísmenné texty proměnných končící slovem „accesskey“
označují přístupovou klávesu – podtržené písmenko v podobné entitě
končící slovem „label“. Písmeno v překladu „accesskey“ tedy musí být
obsaženo v odpovídajícím „label“, být dostupné na české klávesnici a
také být v dané nabídce pokud možno unikátní.

## Editory a nástroje

  - [Textový editor]({% link _pages/wiki/Překladatelský_software/Textový_editor.md %})
  - [Pontoon]({% link _pages/wiki/Překladatelský_software/Pontoon.md %})
  - [Pootle]({% link _pages/wiki/Překladatelský_software/Pootle.md %})
  - [Transifex]({% link _pages/wiki/Překladatelský_software/Transifex.md %})
  - [Crowdin]({% link _pages/wiki/Překladatelský_software/Crowdin.md %})

## Externí odkazy

  - [Document Type Definition (DTD) –
    Wikipedie](https://cs.wikipedia.org/wiki/Document_Type_Definition)
  - [Extensible Markup Language (XML) –
    Wikipedie](https://cs.wikipedia.org/wiki/Extensible_Markup_Language)
