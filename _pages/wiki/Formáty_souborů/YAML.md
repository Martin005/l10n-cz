---
layout: page
title: YAML
permalink: /wiki/Formáty_souborů/YAML/
redirect_from:
  - /YAML/
parent: Formáty souborů
---

Soubory typu *.yaml* se používají převážně pro konfiguraci aplikací
napsaných v jazyce Ruby a dalších, zřídka i pro lokalizaci. Obsahem jsou
podobné [Properties]({% link _pages/wiki/Formáty_souborů/Properties.md %}) s hierarchií. Obsah YAMLu
vypadá asi takto:

```yaml
# comment
buttons:
  ok.label: OK
  cancel.label: Cancel
hello: "Hello, world!"
```

## Editory

  - [Textový editor]({% link _pages/wiki/Překladatelský_software/Textový_editor.md %})

## Externí odkazy

  - [YAML - Wikipedie](https://cs.wikipedia.org/wiki/YAML)
