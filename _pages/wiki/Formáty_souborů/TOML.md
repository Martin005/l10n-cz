---
layout: page
title: TOML
permalink: /wiki/Formáty_souborů/TOML/
redirect_from:
  - /TOML/
parent: Formáty souborů
---

Soubory typu *.toml* se používají převážně pro konfiguraci, ale můžete
je potkat i při lokalizaci. TOML soubor je rozdělený i do vícestupňových
sekcí, které obsahují jednotlivá přiřazení `IDENTIFIKATOR = "ŘETĚZEC"`.

```
# comment
[buttons]

[buttons.ok]
label = "OK"

[buttons.cancel]
label = "Cancel"
```

## Editory

  - [Textový editor]({% link _pages/wiki/Překladatelský_software/Textový_editor.md %})

## Externí odkazy

  - [TOML - Wikipedia](https://en.wikipedia.org/wiki/TOML)
