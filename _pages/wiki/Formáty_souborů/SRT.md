---
layout: page
title: SRT
permalink: /wiki/Formáty_souborů/SRT/
redirect_from:
  - /SRT/
parent: Formáty souborů
---

Soubory ve formátu *.srt* (SubRip) obsahují titulky k videím. Formát
souboru je trochu složitější než u [SUB]({% link _pages/wiki/Formáty_souborů/SUB.md %}), ale pořád
dostatečně jednoduchý. Na první řádku je pořadí titulku, na druhém
rozpětí časů, kdy se má ve videu zobrazit, pod ním je samotný text a
následně jeden prázdný řádek.

```
1
00:02:25,600 --> 00:02:29,400
V pěti čtvrtích New Yorku
žije zhruba 8 milionů lidí.

2
00:02:29,600 --> 00:02:32,080
V celé oblasti 12 milionů.
```

## Editory a nástroje

  - [Textový editor]({% link _pages/wiki/Překladatelský_software/Textový_editor.md %})
  - [Amara](https://amara.org/)
  - [Transifex]({% link _pages/wiki/Překladatelský_software/Transifex.md %})
  - [Tvorba a editace titulků -
    Slunečnice.cz](https://www.slunecnice.cz/video/tvorba-a-editace-titulku/)

## Externí odkazy

  - [SubRip - Wikipedie](https://cs.wikipedia.org/wiki/SubRip)
