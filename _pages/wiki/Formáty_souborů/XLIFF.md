---
layout: page
title: XLIFF
permalink: /wiki/Formáty_souborů/XLIFF/
redirect_from:
  - /XLIFF/
parent: Formáty souborů
---

Soubory typu XLIFF (XML Localisation Interchange File Format) jsou
standardizované [XML]({% link _pages/wiki/Formáty_souborů/XML.md %}) soubory pro lokalizaci. Řetězce
(bez obalovacích prvků) v souborech *.xliff*, používaných často pro
lokalizaci aplikací pro iOS, vypadají asi takto:

```xml
<trans-unit id="Delete">
  <source>Delete</source>
  <note>Action button for deleting bookmarks in the bookmarks panel.</note>
</trans-unit>
```

Stejně jako každý XML soubor je i XLIFF dobře čitelný. Na řádku `<source>` je původní řetězec k překladu a na řádku `<note>` poznámka k řetězci (obvykle, kde se v aplikaci nachází). Ruční překlad je ale trochu složitější, protože kromě překladu je potřeba přidat i část XML (řádek `<target>`). I proto je vhodnější používat nějaký specializovaný editor nebo webovou aplikaci (v případě aplikací od [Mozilly]({% link _pages/wiki/Překladatelské_týmy.md %}#mozilla) je jí [Pontoon]({% link _pages/wiki/Překladatelský_software/Pontoon.md %}).

```xml
<trans-unit id="Delete">
  <source>Delete</source>
  <target>Smazat</target>
  <note>Action button for deleting bookmarks in the bookmarks panel.</note>
</trans-unit>
```

## Editory a nástroje

  - [Poedit]({% link _pages/wiki/Překladatelský_software/Poedit.md %})
  - [Textový editor]({% link _pages/wiki/Překladatelský_software/Textový_editor.md %}) (ne příliš vhodný kvůli
    relativně komplexní syntaxi)
  - [Pontoon]({% link _pages/wiki/Překladatelský_software/Pontoon.md %})
  - [Pootle]({% link _pages/wiki/Překladatelský_software/Pootle.md %})
  - [Weblate]({% link _pages/wiki/Překladatelský_software/Weblate.md %})
  - [Transifex]({% link _pages/wiki/Překladatelský_software/Transifex.md %})
  - [Crowdin]({% link _pages/wiki/Překladatelský_software/Crowdin.md %})
  - [Virtaal]({% link _pages/wiki/Překladatelský_software/Virtaal.md %})
  - [OmegaT]({% link _pages/wiki/Překladatelský_software/OmegaT.md %})

## Externí odkazy

  - [OASIS XML Localisation Interchange File Format (XLIFF) TC |
    OASIS](https://www.oasis-open.org/committees/xliff/)
  - [Localizing XLIFF files for iOS - Mozilla |
    MDN](https://developer.mozilla.org/en-US/docs/Mozilla/Localization/Localizing_XLIFF_files)
  - [XLIFF – Wikipedia](https://en.wikipedia.org/wiki/XLIFF)
  - [Extensible Markup Language (XML) –
    Wikipedie](https://cs.wikipedia.org/wiki/Extensible_Markup_Language)
