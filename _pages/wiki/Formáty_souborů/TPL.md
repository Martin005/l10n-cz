---
layout: page
title: TPL
permalink: /wiki/Formáty_souborů/TPL/
redirect_from:
  - /TPL/
parent: Formáty souborů
---

Koncovka *.tpl* (z anglického *template*) označuje soubory šablonovacího
PHP frameworku Smarty, nebo některých dalších. V případě Smarty nejde v
pravém smyslu o lokalizační formát, ale dají se tak také využít.

Šablony Smarty kombinují
[HTML](https://cs.wikipedia.org/wiki/Hypertext_Markup_Language) s
vlastním šablonovacím jazykem v chlupatých/kudrnatých/složených
závorkách. Tento značkovací jazyk může odkazovat na některé proměnné a
dále s nimi pracovat (formátování, escapování, zkracování, ...).

```html
<!DOCTYPE html>
<html lang="cs">
  {* Toto je komentář, který se zobrazí jen v souboru ".tpl" a součástí výstupu nebude. *}
  <head>
    <meta charset="utf-8">
    <title>
    {$title_text|escape}
    </title>
    {\* Vložení titulku stránky s ošetřením proti vložení HTML kódu pomocí makra "escape". \*}
  </head>
  <body>
    {$body_html} {* Vložení HTML těla webové stránky, tedy bez escapování. *}
  </body>
</html>
```

## Editory

  - [Textový editor]({% link _pages/wiki/Překladatelský_software/Textový_editor.md %})

## Externí odkazy

  - [Smarty - oficiální stránky](https://www.smarty.net/)
  - [Dokumentace Smarty - Part II. Smarty For Template Designers |
    Smarty](https://www.smarty.net/docsv2/en/smarty.for.designers)
  - [Smarty (template engine) -
    Wikipedia](https://en.wikipedia.org/wiki/Smarty_\(template_engine\))
