---
layout: page
title: TS
permalink: /wiki/Formáty_souborů/TS/
redirect_from:
  - /TS/
parent: Formáty souborů
---

Soubory s koncovkou *.ts* jsou příkladem [XML]({% link _pages/wiki/Formáty_souborů/XML.md %}) a
používají se pro lokalizaci aplikací napsaných ve frameworku Qt.
Soubory *.ts* obsahují řetězce reprezentované jednotlivými XML prvky s
dodatečnými informacemi, jako originální text, komentář nebo stav
překladu.

```xml
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <message>
        <source>OK</source>
        <extracomment>Label for OK button in OK/Cancel dialog.</extracomment>
        <translation type="unfinished">OK</translation>
    </message>
    <message>
        <source>Cancel</source>        
        <translation type="obsolete">Storno</translation>
    </message>
</context>
</TS>
```

## Editory

  - [Textový editor]({% link _pages/wiki/Překladatelský_software/Textový_editor.md %})

## Externí odkazy

  - [Qt Localization - Qt Wiki](https://wiki.qt.io/Qt_Localization)
