---
layout: page
title: SUB
permalink: /wiki/Formáty_souborů/SUB/
redirect_from:
  - /SUB/
parent: Formáty souborů
---

Soubory ve formátu *.sub* (SubViever) obsahují titulky k videím. Formát
souboru je velmi prostý. Na první řádku je vždy rozpětí časů, kdy se
konkrétní titulek má ve videu zobrazit, pod ním je samotný text a
následně jeden prázdný řádek.

```
00:02:25.60,00:02:29.40
V pěti čtvrtích New Yorku
žije zhruba 8 milionů lidí.

00:02:29.60,00:02:32,08
V celé oblasti 12 milionů.
```

## Editory

  - [Textový editor]({% link _pages/wiki/Překladatelský_software/Textový_editor.md %})
  - [Amara](https://amara.org/)
  - [Tvorba a editace titulků -
    Slunečnice.cz](https://www.slunecnice.cz/video/tvorba-a-editace-titulku/)

## Externí odkazy

  - [SubViewer - Wikipedia](https://en.wikipedia.org/wiki/SubViewer)
