---
layout: page
title: Gettext
permalink: /wiki/Formáty_souborů/Gettext/
redirect_from:
  - /Gettext/
parent: Formáty souborů
---

**GNU gettext** je internacionalizační (i18n) a lokalizační (l10n)
knihovna umožňující provozování lokalizovaného softwaru v Unixu a na
dalších platformách. Je součástí projektu GNU. Jako editor můžete
použít například program [Poedit]({% link _pages/wiki/Překladatelský_software/Poedit.md %}).

Gettext používá několik souborů - *.pot* (textová šablona s anglickými
řetězci), *.po* (textový soubor s překlady, pro každý jazyk
samostatný), *.mo* (binární podoba odpovídajícího *.po* souboru).
Soubor s překladem (*.po*) začíná obvykle informacemi o autorech
překladu a zejména hlavičkou. Pro češtinu musí hlavička vždy obsahovat
následující definici kódování a množných čísel:

```
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms:  nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
```

Tato hlavička říká, že přeložené texty jsou v kódování UTF-8, aby
správně fungovaly háčky a čárky, a že čeština má tři formy pro množná
čísla (1, 2-4, ostatní). Za hlavičkou už následují jednotlivé řetězce,
které vypadají takto:

```
#* src/name.c:36
msgid "My name is %s.\n"
msgstr "Jmenuji se %s.\n"
```

První řádek (komentář začínající znakem *\#*) informuje o tom, kde se
řetězec nachází ve zdrojovém kódu. Následuje identifikátor řetězce
*msgid*, který je zároveň originálním textem k překladu, a hned pod ním
je samotný překlad *msgstr*.

Speciální význam v řetězcích má znak podtržítko (_), které označuje
akcelerátor / access key. Informace o nich najdete na [samostatné
stránce]({% link _pages/wiki/Návody/Akcelerátory.md %}).

## Editory a nástroje

  - [Gtranslator]({% link _pages/wiki/Překladatelský_software/Gtranslator.md %})
  - [Lokalize]({% link _pages/wiki/Překladatelský_software/Lokalize.md %})
  - [Poedit]({% link _pages/wiki/Překladatelský_software/Poedit.md %})
  - [Virtaal]({% link _pages/wiki/Překladatelský_software/Virtaal.md %})
  - [Crowdin]({% link _pages/wiki/Překladatelský_software/Crowdin.md %})
  - [GlotPress]({% link _pages/wiki/Překladatelský_software/GlotPress.md %})
  - [Pontoon]({% link _pages/wiki/Překladatelský_software/Pontoon.md %})
  - [Pootle]({% link _pages/wiki/Překladatelský_software/Pootle.md %})
  - [Transifex]({% link _pages/wiki/Překladatelský_software/Transifex.md %})
  - [Weblate]({% link _pages/wiki/Překladatelský_software/Weblate.md %})

## Externí odkazy

  - [GNU gettext – Wikipedie](https://cs.wikipedia.org/wiki/GNU_gettext)
  - [Domovská stránka knihovny
    gettext](https://www.gnu.org/software/gettext/)
