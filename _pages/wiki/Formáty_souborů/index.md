---
layout: page
title: Formáty souborů
permalink: /wiki/Formáty_souborů/
redirect_from:
  - /Kategorie:Formáty_souborů/
has_children: true
nav_exclude: true
---

{% include l10n-cz/table-of-contents.html %}

Zdrojové texty i jejich české a další překlady se typicky ukládají do
textových souborů, odkud je programy za běhu načítají. Pro tyto soubory
se používají více či méně standardizované **lokalizační formáty**, ke
kterým existují [knihovny pro vývojáře
softwaru]({% link _pages/wiki/Softwarové_knihovny/index.md %}) i [nástroje pro
překladatele]({% link _pages/wiki/Překladatelský_software/index.md %}).

## Rozdělení souborů podle obsahu

### Jednojazyčné soubory

Jednojazyčné soubory (anglicky *monoligual file*) obsahují texty pouze v
jednom jazyce, typicky tedy buď anglické zdrojové texty nebo jejich
překlady do konkrétního jazyka. Pro jejich párování nebo odkazování se
používají strojově čitelné textové identifikátory. Příkladem takového
formátu je [Fluent]({% link _pages/wiki/Formáty_souborů/Fluent.md %}).

### Vícejazyčné soubory

Vícejazyčné soubory (anglicky *multilingual file*) nejčastěji obsahují
texty v jazyce zdrojovém (angličtina) i cílovém (čeština). Pro jejich
párování programem se používá celý zdrojový text. Příkladem je
[Gettext]({% link _pages/wiki/Formáty_souborů/Gettext.md %}).

## Související dokumentace

Informace a příklady dalších formátů můžete najít například

  - v [dokumentaci
    Weblate](https://docs.weblate.org/en/latest/formats.html)
  - v [dokumentaci Translate
    Toolkitu](https://docs.translatehouse.org/projects/translate-toolkit/en/latest/formats/index.html)
