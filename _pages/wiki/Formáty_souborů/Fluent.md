---
layout: page
title: Fluent
permalink: /wiki/Formáty_souborů/Fluent/
redirect_from:
  - /Fluent/
parent: Formáty souborů
---

{% include l10n-cz/table-of-contents.html %}

Fluent (dříve L20n) je celý lokalizační systém navržený Mozillou, který
umožňuje překlady maximálně přizpůsobit podle cílového jazyka. Soubory
používají příponu *.ftl*.

## Syntaxe

Úplně nejzákladnější podoba *.ftl* souboru není nepodobná
[Properties]({% link _pages/wiki/Formáty_souborů/Properties.md %}) (`IDENTIFIKATOR = RETEZEC`). Navíc ale
umožňuje reference na další řetězce nebo dodatečné atributy.

```
menu-settings = Settings
help-menu-settings = Click { menu-settings } to save the file.
login-button =
    .label = Login
    .accesskey = L
```

Základní syntaxe `IDENTIFIKATOR = RETEZEC` je velmi jednoduchá, ale pro
plné využití možností Fluentu se předpokládá použití nějakého editoru.
Jelikož jde o poměrně nový formát, který se pomalu prosazuje v
překladech [Mozilly]({% link _pages/wiki/Překladatelské_týmy.md %}#mozilla), nejdále
je podpora zřejmě v jejím webovém editoru [Pontoon]({% link _pages/wiki/Překladatelský_software/Pontoon.md %}).

```
emails = { $unreadEmails ->
        [one] You have one unread email.
        [42] You have { $unreadEmails } unread emails. So Long, and Thanks for All the Fish.
       *[other] You have { $unreadEmails } unread emails.
    }
```

Toto je příklad selektorů pro množná čísla. Fluent podporuje kromě
standardních [CLDR
kategorií](https://www.unicode.org/cldr/charts/30/supplemental/language_plural_rules.html),
také konkrétní čísla, takže je možné překlad libovolně vyladit pro
konkrétní situaci. Hvězdička označuje výchozí variantu řetězce (v
příkladu tedy jakékoliv číslo kromě jedničky a čtyřicet dvojky).
Selektory je možné používat i v kombinaci s některými vestavěnými
[funkcemi](https://mozilla-l10n.github.io/localizer-documentation/tools/fluent/functions.html).

### Skloňování

Pokročilejší syntax Fluentu lze použít k [podpoře skloňování podstatných
jmen](https://mozilla-l10n.github.io/localizer-documentation/tools/fluent/basic_syntax.html#terms-and-parameterized-terms)
definovaných v samostatných entitách. Typicky jde o názvy produktů nebo
služeb.

```
-vendor-short-name =
    { $case ->
       *[nom] Mozilla
        [gen] Mozilly
        [dat] Mozille
        [acc] Mozillu
        [voc] Mozillo
        [loc] Mozille
        [ins] Mozillou
    }
```

Tady vidíme překlad slova *Mozilla* v entitě s názvem
`-vendor-short-name`. Hvězdička označuje výchozí pád, pokud by žádný
nebyl explicitně vybraný. Všimněte si také speciálního typu entity
začínající pomlčkou. Označuje se jako *termín*. Jejich použití se pak
rovněž trochu liší (nepoužívá se znak dolaru).

```
learn-more-blog = Podrobnosti najdete na blogu { -vendor-short-name(case: "gen") }.
```

Výsledný text bude

> Podrobnosti najdete na blogu Mozilly

## Editory a nástroje

  - [Textový editor]({% link _pages/wiki/Překladatelský_software/Textový_editor.md %}) (ne příliš vhodný kvůli
    relativně komplexní syntaxi)
  - [Pontoon]({% link _pages/wiki/Překladatelský_software/Pontoon.md %})

## Knihovny

Fluent poskytuje knihovny pro internacionalizaci softwaru v jazycích
Rust, JavaScript, Python a Mozilla ho používá i pro lokalizaci Firefoxu
v C/C++ a výhledově i aplikací v Javě pro Android.

## Externí odkazy

  - [Oficiální stránky Project Fluent](https://projectfluent.org/)
  - [projectfluent/fluent
    Wiki](https://github.com/projectfluent/fluent/wiki)
  - [Project Fluent Playground](https://projectfluent.org/play/)
  - [Fluent for
    localizers](https://mozilla-l10n.github.io/localizer-documentation/tools/fluent/)
  - [Fluent Syntax Guide](https://projectfluent.org/fluent/guide/)
  - [Repositáře na GitHubu](https://github.com/projectfluent)
