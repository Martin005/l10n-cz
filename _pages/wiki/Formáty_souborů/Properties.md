---
layout: page
title: Properties
permalink: /wiki/Formáty_souborů/Properties/
redirect_from:
  - /Properties/
parent: Formáty souborů
---

{% include l10n-cz/table-of-contents.html %}

Soubory typu *.properties* se používají převážně pro konfiguraci
aplikací napsaných v jazyce Java, nebo pro lokalizaci. Najdeme je
použité třeba v produktech Mozilly pro počítač nebo Android. Obsah
souboru *.properties* vypadá asi takto:

```
# COMMON
common.proxy.none = None
common.buttons.cancel = Cancel
common.buttons.load = Load
```

Soubor v UTF-8 obsahuje na každém řádku přiřazení `IDENTIFIKATOR =
RETEZEC` nebo `IDENTIFIKATOR : RETEZEC`, kde identifikátor neobsahuje
mezery a řetězec je text zapsaný bez uvozovek. Pokud by v uvozovkách
byl, staly by se jeho součástí. Řádky začínající znakem „\#“ nebo „\!“
reprezentují komentáře, které nepřekládáme. Přeložíme tedy na každém
řádku vše za prvním znakem „=“.

```
# COMMON
common.proxy.none = Žádný
common.buttons.cancel = Zrušit
common.buttons.load = Načíst
```

## Množná čísla

Při překladu řetězců v projektech Mozilly závislých na počtu položek
(např. "máte %d nepřečtenou zprávu"/"máte %d nepřečtené zprávy"/"máte %d
nepřečtených zpráv") můžete narazit na jeden řetězec s tvary oddělenými
středníky (bez mezer). Každý tvar je pro jiný počet položek. Nejde o
standart používaný v rámci souborů *.properties*, ale o způsob, jakým do
jednoho řetězce Mozilla zahrne více forem množného čísla. Pokud si
nejste jistí, pro jaké počty je který tvar použit, najdete tuto
informaci v dokumentu [Localization and
Plurals](https://developer.mozilla.org/docs/Mozilla/Localization/Localization_and_Plurals#Plural_rule_8_%283_forms%29)
na MDN.

Příklad takového řetězce by byl:

```
# LOCALIZATION NOTE (messages.unread): Semi-colon list of plural forms.
#  See: `<https://developer.mozilla.org/en/docs/Localization_and_Plurals>
#  #1 is the number of unread messages
messages.unread = You have #1 unread message.;You have #1 unread messages.
```

Protože v češtině máme množné číslo složitější, je potřeba jeden tvar
dokonce přidat a výsledek pak bude vypadat takto:

```
# LOCALIZATION NOTE (messages.unread): Semi-colon list of plural forms.
#  See: `<https://developer.mozilla.org/en/docs/Localization_and_Plurals>
#  #1 is the number of unread messages
messages.unread = Máte #1 nepřečtenou zprávu.;Máte #1 nepřečtené zprávy.;Máte #1 nepřečtených zpráv.
```

Pokud uvidíte více po sobě jdoucích samostatných řetězů lišících se
pouze zakončením jejich identifikátoru v hranatých závorkách, jde
konkrétně o použití [Unicode
CLDR](https://cs.wikipedia.org/wiki/Common_Locale_Data_Repository).
Vysvětlení takových řetězců pak najdete v dokumentu [Localization -
Unicode CLDR plural
forms](https://developer.mozilla.org/Add-ons/SDK/Tutorials/l10n#Unicode_CLDR_plural_forms)
na MDN.

## Editory a nástroje

  - [Textový editor]({% link _pages/wiki/Překladatelský_software/Textový_editor.md %})
  - [Pontoon]({% link _pages/wiki/Překladatelský_software/Pontoon.md %})
  - [Pootle]({% link _pages/wiki/Překladatelský_software/Pootle.md %})
  - [Transifex]({% link _pages/wiki/Překladatelský_software/Transifex.md %})
  - [Crowdin]({% link _pages/wiki/Překladatelský_software/Crowdin.md %})

## Externí odkazy

  - [.properties - Wikipedia](https://en.wikipedia.org/wiki/.properties)
