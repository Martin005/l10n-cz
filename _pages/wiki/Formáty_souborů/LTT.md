---
layout: page
title: LTT
permalink: /wiki/Formáty_souborů/LTT/
redirect_from:
  - /LTT/
parent: Formáty souborů
---

Formát [LTT](https://pervoj.cz/ltt-translation-system/) je postavený na
formátu [INI]({% link _pages/wiki/Formáty_souborů/INI.md %}). Používá dvě koncovky, *.ltt* (obsahuje
přeložené texty) a *.lttp* (obsahuje originální texty). Syntaxe souborů
je popsána v [oficiální
wiki](https://github.com/pervoj/ltt-translation-system/wiki). Ukázka
syntaxe:

```
; Language:English
; Code:en
; Author:John Doe <j.doe@example.com>
; Pattern:DEFAULT.lttp

;.title:A title of the app
;.title:Please, don't translate
title=My App

;.description:A short info about app
description=My first application, which is translated to various languages.
```

## Editory

  - [Textový editor]({% link _pages/wiki/Překladatelský_software/Textový_editor.md %})
  - [LTT Edit]({% link _pages/wiki/Překladatelský_software/LTT_Edit.md %})
