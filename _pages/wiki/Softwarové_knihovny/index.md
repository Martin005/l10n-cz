---
layout: page
title: Softwarové knihovny
permalink: /wiki/Softwarové_knihovny/
redirect_from:
  - /Kategorie:Softwarové_knihovny/
has_children: true
nav_exclude: true
---

Pro snadnou práci s překlady ze strany vývojářů softwaru se používají
standardní knihovny nebo knihovny jednotlivých [lokalizačních
formátů]({% link _pages/wiki/Formáty_souborů/index.md %}). Často zastanou práci
jak pro internacionalizaci konkrétního softwaru, tak při tvorbě
[softwaru a nástrojů pro
překladatele]({% link _pages/wiki/Překladatelský_software/index.md %}).
