---
layout: page
title: LTTGetter
permalink: /wiki/Softwarové_knihovny/LTTGetter/
redirect_from:
  - /LTTGetter/
parent: Softwarové knihovny
---

LTTGetter je oficiální knihovna pro systém překladů
[LTT]({% link _pages/wiki/Formáty_souborů/LTT.md %}) použitelná v mnoha programovacích jazycích.

Zpravidla obsahuje třídu *LTTGetter*, na které se volá metoda
*getText()*. Více informací v [oficiální wiki
projektu](https://github.com/pervoj/ltt-translation-system/wiki).

Ukázka v jazyce Java:

```java
LTTGetter ltt = new LTTGetter("langs", "cs");
System.out.println(ltt.getText("application name"));
```