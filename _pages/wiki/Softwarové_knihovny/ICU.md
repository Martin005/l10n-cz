---
layout: page
title: ICU
permalink: /wiki/Softwarové_knihovny/ICU/
redirect_from:
  - /ICU/
parent: Softwarové knihovny
---

ICU (International Components for Unicode) je multiplatformní knihovna
pro podporu Unicode a internacionalizaci softwaru psaného v C/C++ nebo
Javě. Podporuje konverzi z různých znakových sad do Unicode, jazykově
specifické řazení podle abecedy nebo vyhledávání.

Původně bylo ICU vyvinuto firmou IBM, která ho uvolnila jako open-source
a s dalšími společnostmi pokračuje ve vývoji.

## Externí odkazy

  - [Oficiální stránky](https://icu.unicode.org/)
  - [ICU Message
    Format](https://format-message.github.io/icu-message-format-for-translators/)
