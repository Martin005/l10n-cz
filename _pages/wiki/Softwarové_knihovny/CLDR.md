---
layout: page
title: CLDR
permalink: /wiki/Softwarové_knihovny/CLDR/
redirect_from:
  - /CLDR/
parent: Softwarové knihovny
---

**Common Locale Data Repository** (CLDR) jsou data používaná mnoha
knihovnami a lokalizačním softwarem včetně [ICU]({% link _pages/wiki/Softwarové_knihovny/ICU.md %}). Data
obsahují např.:

  - překlady názvů jazyků
  - názvy států, měn
  - názvy měsíců včetně jejich zkratek
  - formáty dat
  - formáty pro parsování čísel
  - názvy barev
  - názvy smajlíků
  - a další

## Externí odkazy

  - [Oficiální stránky](https://cldr.unicode.org/)
  - [Latest Release - CLDR - Unicode Common Locale Data
    Repository](https://cldr.unicode.org/index/downloads/latest)
  - [cldr - SVN:
    /tags/latest](https://www.unicode.org/repos/cldr/tags/latest/)
