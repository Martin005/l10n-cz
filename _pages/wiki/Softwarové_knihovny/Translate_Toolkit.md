---
layout: page
title: Translate Toolkit
permalink: /wiki/Softwarové_knihovny/Translate_Toolkit/
redirect_from:
  - /Translate_Toolkit/
parent: Softwarové knihovny
---

**Translate Toolkit** je sada nástrojů usnadňující a rozšiřující
možnosti správy a nakládání s překlady ve formě souborů
[DTD]({% link _pages/wiki/Formáty_souborů/DTD.md %}), [Properties]({% link _pages/wiki/Formáty_souborů/Properties.md %}), *.po*
([gettext]({% link _pages/wiki/Formáty_souborů/Gettext.md %})), [XLIFF]({% link _pages/wiki/Formáty_souborů/XLIFF.md %}) a dalších.
Využívá ji např. lokalizační program [Virtaal]({% link _pages/wiki/Překladatelský_software/Virtaal.md %}) nebo
[Pootle]({% link _pages/wiki/Překladatelský_software/Pootle.md %}) od stejných autorů.

## Externí odkazy

  - [Domovská stránka sady Translate
    Toolkit](https://toolkit.translatehouse.org/)
  - [Wiki](https://translate.sourceforge.net/wiki/)
  - [Repositář na GitHubu](https://github.com/translate/translate)
