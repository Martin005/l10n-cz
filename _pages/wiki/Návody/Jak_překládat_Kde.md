---
layout: page
title: Jak překládat Kde
permalink: /wiki/Návody/Jak_překládat_Kde/
redirect_from:
  - /Jak_překládat_Kde/
  - /Kategorie:KDE/
parent: Návody
---

{% include l10n-cz/table-of-contents.html %}

**[KDE](https://kde.org/)** je oblíbené grafické prostředí pro mnohé
unixové a linuxové operační systémy. I ono má "upstream", tedy systém
pro správu překladů nezávislý na distribuci. Doporučujeme každému
překladateli, aby návrhy překladů pro veškeré aplikace v KDE směroval
do tohoto systému, čímž pomůže překládat KDE i v ostatních distribucích.

Správa překladů, která je používána v KDE, je založena na systému správy
verzí [SVN](https://subversion.apache.org/). Není zde webové rozhraní
pro překlady, ty jsou přímo nahrávány do SVN po boku zdrojového kódu.
Budoucí překladatelé KDE tedy musí mít alespoň základní znalost překladu
[souborů PO]({% link _pages/wiki/Formáty_souborů/Gettext.md %}).

Pokud chcete vypomoci v překládání KDE, kontaktujte prosím koordinátora
týmu (Víta Pelčáka) přímo nebo pomocí e-mailové konference (kontakty viz
níže).

### Stažení překladů

Začátkem roku 2010 byl založen nový repozitář překladů KDE - Summit. Je
to slitek větví stable (stabilní) a trunk (vývojová), který se překládá,
a pak se zase rozlije do stable i trunk. Takže překladatel v podstatě
překládá oboje větve zároveň. Celý tento proces se děje automaticky
(respektive jej má koordinátor automatizován skripty). Podrobnosti lze
najít na
[techbase.kde.org](https://techbase.kde.org/Localization/Workflows/PO_Summit).

Nasazení je důsledkem překladatelské debaty ohledně zlepšení správy
překladů a zajištění jednotnosti překladů ve větvích stable a trunk
(kvůli tomu, že byly překládány odděleně, docházelo ke vzájemným
odchylkám a duplicitě práce). Nevýhodou je, že pokud překládáte trunk
nebo stable, je potřeba aktualizovat tyto překlady na verze ze Summitu a
překládat už jenom odsud. Jinak práce přijde vniveč, protože bude při
následném rozlití Sumitu do větví přepsána.

Pokud máte svn commit access (práva zápisu do repozitáře, pokud náhodou
nevíte, tak zcela jistě nemáte), použijte ke stažení Summitu příkaz
(musíte mít nainstalovanou Subversion):

```bash
svn co https://svn.kde.org/home/kde/trunk/l10n-support/cs/summit
```

Šablony (soubory POT) jsou k mání zde:

```bash
svn co https://svn.kde.org/home/kde/trunk/l10n-support/templates
```

Ostatní překladatelé mohou stahovat pomocí:

```bash
svn co svn://anonsvn.kde.org/home/kde/trunk/l10n-support/cs/summit
```

Šablony jsou k mání zde:

```bash
svn co svn://anonsvn.kde.org/home/kde/trunk/l10n-support/templates
```

K dispozici jsou překlady a šablony jak programů, tak dokumentace.

Přeložené soubory pošlete e-mailem koordinátorovi (ideálně rozdělené do
adresářů podle toho, kam jednotlivé překlady patří, a zabalené do
archivu).

Čas od času nezapomeňte v adresářích/složkách, které vznikly při stažení
překladů (musí v nich být skrytý adresář .svn), spustit:

```bash
svn up
```

Aktualizace spuštěná v nadřazeném adresáři se provede i v těch
vnořených, takže nemusíte spouštět `svn up` úplně všude, stačí dva
hlavní adresáře, jak ten s překlady, tak ten se šablonami. Tím se
provede aktualizace překladů a zamezí se práci na neaktuálních
souborech.

Bohužel, webové rozhraní se [statistikami
stable](https://l10n.kde.org/stats/gui/stable-kde4/team/cs/)/[trunk](https://l10n.kde.org/stats/gui/trunk-kde4/team/cs/)
umožňující i stažení překladu je tímto použitelné pouze informativně,
stahovat překlady pro překládání odsud je *špatný* nápad, protože tyto
nejsou pro Summit.

### Viz také

  - [Nastavení editoru Lokalize a paměti překladů]({% link _pages/wiki/Překladatelský_software/Lokalize.md %})

### Kontakt

  - stránky překladatelů KDE: <https://kde-czech.sourceforge.net/>
    (poněkud zastaralé)
  - statistiky překladů KDE:
    <https://l10n.kde.org/stats/gui/trunk-kde4/team/cs/>,
    <https://l10n.kde.org/stats/gui/stable-kde4/team/cs/>
  - e-mailová konference KDE:
    <https://lists.sourceforge.net/lists/listinfo/kde-czech-apps/>
  - koordinátor překladů: Klára Cihlářová koty(zavináč)seznam(tečka)cz,
    Lukáš Tinkl lukas(zavináč)kde(tečka)cz, Vít Pelčák
    vit(zavináč)pelcak(tečka)org
