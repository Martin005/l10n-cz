---
layout: page
title: Návody
permalink: /wiki/Návody/
redirect_from:
  - /Návody/
  - /Návody_pro_překladatele/
  - /Kategorie:Návody/
has_children: true
nav_order: 4
---

{% include l10n-cz/table-of-contents.html %}

## Jak...

  - překládat linuxové distribuce, Firefox, LibreOffice nebo podobné
    velké projekty?
      - Kontaktujte [existující komunitu
        překladatelů]({% link _pages/wiki/Překladatelské_týmy.md %}) daného
        projektu.
  - překládat malé aplikace, doplňky do prohlížeče nebo začít s novým
    projektem?
      - \-\> [Jak začít překládat]({% link _pages/wiki/Návody/Jak_začít_překládat.md %})

## Obecné rady

  - Kontaktujte zbytek překladatelského týmu svého projektu a nechte si
    poradit, kde najít všechny potřebné informace a nástroje.
  - Používejte [korektor
    překlepů]({% link _pages/wiki/Kontrola_pravopisu/index.md %}) a další
    [pomůcky pro překladatele]({% link _pages/wiki/Pomůcky_pro_překladatele.md %}).
  - Podívejte se na návody, pravidla a terminologii pro konkrétní
    projekt.
      - Pro doplnění můžete vždy sáhnout po našem [překladatelském
        slovníku]({% link _pages/wiki/Slovníky/Překladatelský_slovník.md %}) a [stylistické
        příručce]({% link _pages/wiki/Slovníky/Stylistická_příručka.md %}).
  - Když si nebudete vědět rady, napište do naší [e-mailové
    konference](https://lists.l10n.cz/mailman/listinfo/diskuze).
  - Dejte svůj překlad přečíst někomu dalšímu, nebo alespoň před
    odesláním překladu počkejte a přečtěte si jej znovu druhý den s
    čistou hlavou.
  - *BONUS: Přečtěte si něco o [překladatelské
    hantýrce]({% link _pages/wiki/Slovníky/Překladatelská_hantýrka.md %}).*

## Zajímavé články

  - FUCHS, Jan. [Lokalizace aplikací,
    Gettext](https://www.abclinuxu.cz/blog/fuky/2009/3/lokalizace-aplikaci-gettext).
    AbcLinuxu, 17. 3. 2009. ISSN 1214-1267
  - CHABADA, Peter. [Príručka
    prekladateľa](http://data.klacansky.com/l10n/sk/docs/prirucka_prekladatela.pdf).
    Soubor PDF, slovensky.
  - KAPICA, Aleš. [Linuxové překladatelské
    nástroje](https://www.abclinuxu.cz/blog/kenyho_stesky/2006/10/linuxove-prekladatelske-nastroje-i).
    Seriál, 5 dílů. AbcLinuxu, 3. 10. 2006. ISSN 1214-1267
  - KOLESA, Daniel. [Gettext snadno a
    rychle](https://www.abclinuxu.cz/clanky/navody/gettext-snadno-a-rychle).
    Seriál, 2 díly. AbcLinuxu, 20.1.2010. ISSN 1214-1267
  - KOVÁŘ, Petr. [Lokalizace otevřeného a svobodného
    software](https://www.root.cz/clanky/lokalizace-otevreneho-a-svobodneho-software/).
    Root.cz, 10. 4. 2008.
  - KRČMÁŘ, Petr. [False friends: aktuálně je tu velká
    trafika](https://blog.root.cz/petrkrcmar/false-friends-aktualne-je-tu-velka-trafika/).
    Blok o zrádných slovíčkách v angličtině. Blog na Rootu, 22. 11.
    2011.
  - KRČMÁŘ, Petr. [Nalezeno v
    překladu](https://blog.root.cz/petrkrcmar/nalezeno-v-prekladu/).
    Blok o zrádných slovíčkách v angličtině. Blog na Rootu, 18. 8. 2014.
  - KRČMÁŘ, Petr. [Procento vs. procentní
    bod](https://blog.root.cz/petrkrcmar/procento-vs-procentni-bod/).
    Blok o zrádných slovíčkách v angličtině. Blog na Rootu, 13. 5. 2012.
  - OTT, Vlastimil. [Jak lokalizovat linuxový
    software 1](https://e-ott.info/2008/02/22/jak-lokalizovat-linuxovy-software-1/).
    Elektronický Ott, 22. 2. 2008.
  - VALASEK, Stanislav. [Ako na preklad slobodných
    programov](https://www.abclinuxu.cz/clanky/navody/ako-na-preklad-slobodnych-programov)
    a [Ako na preklad slobodných
    programov 2](https://www.abclinuxu.cz/clanky/navody/ako-na-preklad-slobodnych-programov-2).
    Seriál, 2 díly. AbcLinuxu, 3. 10. 2006. ISSN 1214-1267
