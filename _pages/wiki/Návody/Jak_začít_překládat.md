---
layout: page
title: Jak začít překládat
permalink: /wiki/Návody/Jak_začít_překládat/
redirect_from:
  - /Jak_začít_překládat/
  - /Překlad_u_autora/
parent: Návody
---

{% include l10n-cz/table-of-contents.html %}

*Pokud už je vámi vybraný projekt byť jen z části v češtině, kontaktujte
nejprve jeho stávající překladatele. Možná ho najdete i mezi našimi
[překladatelskými týmy]({% link _pages/wiki/Překladatelské_týmy.md %}).*

## Jak si vybrat projekt k překladu

Pokud nevíte, jaký program, aplikaci, rozšíření nebo doplněk překládat,
začněte něčím, co sami používáte. I pokud už je software do češtiny
přeložený, mohou stávající překladatelé uvítat pomoc s novými verzemi
nebo nápovědou. Můžete se přidat k některému z našich [překladatelských
týmů]({% link _pages/wiki/Překladatelské_týmy.md %}) nebo si najít zajímavý projekt v
[knize přání]({% link _pages/wiki/Chci_aby_bylo_přeloženo.md %}).

## Jak s překladem začít

### Zjistěte aktuální stav

Spusťte si daný program nebo projekt a podívejte se, jestli je v češtině
nebo do ní jde přepnout. Pokud je čeština aktuální, nevidíte žádné nebo
téměř žádné anglické texty, bude mít projekt aktivní překladatele a je
potřeba na ně najít kontakt. Pokud ne, na přidání češtiny bude potřeba
se domluvit se správcem projektu.

  - **Projekt už je v češtině.** Výborně, tady vám někdo pomůže. Začněte
    na domovské stránce, wiki nebo v dokumentaci projektu. Informace o
    autorech aktuálního překladu mohou být také po stažení poslední
    vydané verze v informacích o aplikaci. Až je najdete, kontaktujte
    stávající překladatelský tým, se kterým se můžete domluvit, jak
    nejlépe pomoci.
  - **Žádnou češtinu nebo kontakt jsem nenašel.** Pokud překlad ještě
    neexistuje, stávající překladatel vám do rozumné doby neodpověděl,
    nebo jste vůbec nenašli kontakt, můžete předpokládat, že jste se
    právě stali novým hlavním překladatelem daného projektu či jeho
    dokumentace. Hledejte na domovské stránce, wiki nebo v dokumentaci,
    jak se do překladů zapojit. Můžete také napsat přímo správcům
    projektu, kteří by vám měli říci, jak na to.

## Jak překlad probíhá

Postup samotného překladu vždy záleží na konkrétním softwaru a stávající
překladatelé nebo alespoň správce projektu by vám měli poskytnout
informace, kde najdete nové texty k překladu, jaké nástroje k jejich
úpravě používat a jak jim svou práci poslat zpátky. Pokud chcete
stihnout češtinu do další verze programu, je potřeba ji mít hotovou co
nejvíce předem. Velké/uvědomělé projekty vám dají vědět, do kdy je
potřeba mít změny pro příští verzi hotové.

Ať už se projekt překládá pomocí jakéhokoliv nástroje, vaší hlavní
starostí vždy bude výsledná čeština. Proto se také poptejte po nějaké
překladatelském slovníku, doporučené terminologii nebo jazykové
příručce. Zřejmě nic nezkazíte, pokud se budete řídit [našimi
radami]({% link _pages/wiki/Návody/index.md %}) a použijete náš
[Překladatelský slovník]({% link _pages/wiki/Slovníky/Překladatelský_slovník.md %}) nebo
[stylistickou příručku]({% link _pages/wiki/Slovníky/Stylistická_příručka.md %}).

### Překlad přes webové rozhraní

Při překladu přes webové rozhraní máte hodně věcí *zadarmo* a můžete se
soustředit jenom na překlady. [Webová
aplikace]({% link _pages/wiki/Překladatelský_software/index.md %})
spravovaná vaším projektem se sama postará o přidávání nových textů a
odesílání vaší práce ke kontrole a zpět správcům projektu. Nezapomeňte
dát občas zbytku překladatelského týmu vědět, co jste přeložili, a
případně si s nimi navzájem svou práci zkontrolovat.

### Překlad úpravou souborů

Přímá úprava souborů bývá trochu složitější, ale zase se nemusíte nikde
registrovat. Autor projektu vám pošle odkaz na soubory, nebo vám je
pošle e-mailem, vy je přeložíte a nahrajete nebo pošlete zpátky. Někdy
soubory najdete přímo ve vývojovém repositáři na GitHubu nebo
samostatném Gitu, SVNku či HG/Mercurialu. Většinou hledáte soubory s
názvem *cs.po* (obecněji cokoliv s kódem *cs* pro češtinu), v
adresářích *locale*, *l10n* nebo *i18n*.

Pro překlad budete potřebovat nějaký [prográmek nebo textový
editor]({% link _pages/wiki/Překladatelský_software/index.md %}). Jeho
správný výběr záleží na konkrétním [lokalizačním
formátu]({% link _pages/wiki/Formáty_souborů/index.md %}).

## Kdo mi poradí nebo pomůže

V první řadě by vám měl většinu informací a otázek zodpovědět stávající
překladatel nebo správci projektu. Pokud se tak nestane, nejsou vám
jejich odpovědi jasné nebo potřebujete ještě trochu popostrčit,
neváhejte se projít zde po naší wiki nebo napsat do naší [e-mailové
konference]({% link _pages/kontakty.md %}).
