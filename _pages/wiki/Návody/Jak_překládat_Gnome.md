---
layout: page
title: Jak překládat Gnome
permalink: /wiki/Návody/Jak_překládat_Gnome/
redirect_from:
  - /Jak_překládat_Gnome/
  - /Kategorie:Gnome/
parent: Návody
---

{% include l10n-cz/table-of-contents.html %}

## GNOME v češtině

Pokud chcete pomoci s překladem GNOME, pokračujte prosím na stránku s
návodem [Jak překládat, aneb pár dobře míněných rad na
začátek](https://live.gnome.org/Czech/JakPrekladat) na oficiálních
stránkách českého překladatelského týmu GNOME. Odkazovaný text vás
seznámí se základními informacemi, které se týkají lokalizace GNOME do
češtiny, včetně struktury katalogů zpráv ([souborů
PO]({% link _pages/wiki/Formáty_souborů/Gettext.md %})), pravidel při lokalizaci specifických řetězců,
výběru editoru katalogů zpráv a finální kontroly katalogu, dále
rezervace konkrétního překladu a nahrání jeho aktualizace ke korektuře a
zařazení.

### Důležité české odkazy a kontakty

  - [Oficiální stránky českého překladatelského týmu
    GNOME](https://live.gnome.org/Czech)
  - [něco statistik k překladům, včetně možnosti
    rezervace](https://l10n.gnome.org/teams/cs)
  - [GNOME
    Bugzilla](https://bugzilla.gnome.org/enter_bug.cgi?product=L10N)
    (hlášení chyb v české lokalizaci)
  - [česká poštovní konference překladatelů
    GNOME](https://mail.gnome.org/mailman/listinfo/gnome-cs-list)
  - [český kanál IRC na serveru
    irc.gnome.org](irc://irc.gnome.org/#gnome-cz)
  - [Petr Kovář](https://live.gnome.org/PetrKovar) (koordinátor českého
    překladatelského týmu)

### Odkazy na GNOME Translation Project

  - [GNOME Translation
    Project](https://live.gnome.org/TranslationProject) (mezinárodní
    překladatelský projekt GNOME)
  - [mezinárodní poštovní konference překladatelů
    GNOME](https://mail.gnome.org/mailman/listinfo/gnome-i18n)
