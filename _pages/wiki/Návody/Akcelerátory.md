---
layout: page
title: Akcelerátory
permalink: /wiki/Návody/Akcelerátory/
redirect_from:
  - /Akcelerátory/
parent: Návody
---

**Akcelerátory** se míní v uživatelském rozhraní obvykle podtržené
znaky, jejichž volbou z klávesnice je možné procházet nabídkami či volit
popisky či tlačítka v dialogových oknech. Používají se také pro
usnadnění přístupnosti pro nevidomé uživatele (**access keys**) a při
používání v kombinaci s čtečkami obrazovky. Např. v aplikacích
založených na GTK+ se značí pomocí podtržítka před daným znakem, který
má uživatel na klávesnici stisknout:

```
#: src/xo-interface.c:516
msgid "_File"
msgstr "_Soubor"
```

Během lokalizace akcelerátorů je především nutné neumísťovat je na znaky
s diakritikou nebo jiné na klávesnici špatně dostupné znaky. Dále nebývá
vhodné umísťovat je na znaky, které se často zobrazují s malou šířkou
(i, l, t, j...). Je možné se také pokusit o zachování stejného
akcelerátoru mezi originálem a překladem. A samozřejmě pozor na kolize
v rámci jedné nabídky, dialogového okna, apod.

## Kontrola problémů s umístěním

Lze použít např. pofilter ze sady [Translate
Toolkit]({% link _pages/wiki/Softwarové_knihovny/Translate_Toolkit.md %}), v KDE vzniká podobná sada pod
souborným názvem
[pology](https://techbase.kde.org/Localization/Tools/Pology). Součástí
jsou i testy na celou řadu dalších věcí v rámci technické korektury.
Doporučit je lze především pro ty, kteří často po ostatních kontrolují
rozsáhlejší překlady.

Samotné problémy s akcelerátory lze řešit i přímo pomocí nástroje msgfmt
distribuovaného s knihovnou [gettext]({% link _pages/wiki/Formáty_souborů/Gettext.md %}):

```bash
msgfmt --check-accelerators=_ soubor.po
```

(Resp. znak "&" namísto "_" v případě KDE a spol.)

Kontrolu v reálném čase při překládání by měly zvládat
[Virtaal]({% link _pages/wiki/Překladatelský_software/Virtaal.md %}), [Pootle]({% link _pages/wiki/Překladatelský_software/Pootle.md %}) a některé
další [nástroje]({% link _pages/wiki/Překladatelský_software/index.md %}), které
na pozadí využívají Translate Toolkit.
