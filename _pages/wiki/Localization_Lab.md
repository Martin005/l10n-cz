---
layout: page
title: Localization Lab
permalink: /wiki/Localization_Lab/
redirect_from:
  - /Kategorie:Localization_Lab/
nav_exclude: true
---

**Localization Lab** je velká komunita dobrovolníků, kteří překládají
hlavně nástroje zaměřené na svobodu internetu, soukromí, bezpečnost a
anonymitu. Příkladem je např. [Tor]({% link _pages/wiki/Tor.md %}), systém Tails,
F-Droid nebo SecureDrop.

## Externí odkazy

  - [Localization Lab Wiki](https://wiki.localizationlab.org/)
  - [Projects](https://wiki.localizationlab.org/index.php/Projects)
  - [Translation
    Tools](https://wiki.localizationlab.org/index.php/Translation_Tools)
