---
layout: page
title: Chci, aby bylo přeloženo
permalink: /wiki/Chci,_aby_bylo_přeloženo/
redirect_from:
  - /Chci,_aby_bylo_přeloženo/
  - /Kde_se_zapojit/
nav_exclude: true
---

Pokud jste narazili na program, který dosud není přeložený, napište sem
jeho název s odkazem na webové stránky a stručně, o co se jedná. Pokud
byste některý z programů na seznamu chtěli sami přeložit, pomůže vám
stránka [Jak začít překládat]({% link _pages/wiki/Návody/Jak_začít_překládat.md %}) a
poradíme vám i v [e-mailové
konferenci]({% link _pages/kontakty.md %}).

  - [DuckDuckGo](https://github.com/duckduckgo/duckduckgo-locales)
  - [Projekty Localization Lab](https://www.transifex.com/otf/public/) -
    např. Bitmask, NoScript, Tor, ...
  - [Slackware](https://slint.fr/) - linuxová distribuce ([více
    info](https://lists.l10n.cz/pipermail/diskuze/2017-March/002475.html))
  - [StarDict](https://stardict.sourceforge.net/) - slovník, potřeboval
    by dopřekládat
  - [Simple Backup](https://launchpad.net/sbackup) - zálohovací aplikace
