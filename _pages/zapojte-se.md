---
layout: page
title: Zapojte se
permalink: /zapojte-se/
redirect_from:
  - /zapojtese/
  - /Jak_se_zapojit/
nav_order: 7
---

Možností, jak se zapojit do projektu l10n.cz, se nabízí celá řada.

  - [Hlaste chyby objevené v
    překladech]({% link _pages/wiki/Hlášení_chyb_v_překladech.md %}).
  - Začněte sami překládat.
      - Připojte se k některému z
        [týmů]({% link _pages/wiki/Překladatelské_týmy.md %}),
      - některému z překladatelů
        [jednotlivců]({% link _pages/wiki/Stabilní_překladatelé.md %}),
      - nebo si vyberte a začněte [překládat nový
        projekt]({% link _pages/wiki/Návody/Jak_začít_překládat.md %}).
  - [Pište o svých překladech na
    L10N.cz]({% link _pages/infrastruktura.md %}).
  - Přispívejte do [e-mailové
    konference]({% link _pages/kontakty.md %}).
      - Diskutujte o terminologii v [našem překladatelském
        slovníku]({% link _pages/wiki/Slovníky/Překladatelský_slovník.md %})
      - a odpovídejte na dotazy ostatních.
  - Rozšiřujte informace v této naši wiki.
      - Doplňujte [návody]({% link _pages/wiki/Návody/index.md %})
      - nebo informace o [překladatelském
        softwaru]({% link _pages/wiki/Překladatelský_software/index.md %}) a
        [formátech souborů]({% link _pages/wiki/Formáty_souborů/index.md %}).
      - Přidávejte nové přínosné informace (dobrým zdrojem bývá např.
        dokumentace překladatelského softwaru, články na Wikipedii nebo
        [zde](https://translate.sourceforge.net/wiki/guide/start)).
