---
layout: page
title: About L10N.cz
permalink: /english/
nav_order: 9
---

L10N.cz is a Czech community project for free and open source software localization. The project aims to accomplish the following goals:

- use consistent terminology across Czech translations of free and open source software,
- collaboration between the translation teams from various projects,
- recruiting new translators,
- improving the quality of translation,
- simplify translators work, and avoiding duplicate translation efforts.

The English-Czech translation glossary, which is [available on this site's L10N.cz Wiki]({% link _pages/wiki/Slovníky/Překladatelský_slovník.md %}), is the main resource for terminology unification. It has emerged from consensus among translators coming from different free and open source software projects, such as [GNOME](https://www.gnome.org/), [KDE](https://www.kde.org/), [Mozilla](https://www.mozilla.org/), various Linux distributions and other.

Discussions on terminology and other English-Czech translation topics take place in the project's [mailing list]({% link _pages/kontakty.md %})

Our wiki pages contain not only the translation dictionary, but also a [summary of basic rules]({% link _pages/wiki/Návody/index.md %}) in the translation process, [translation utilities]({% link _pages/wiki/Pomůcky_pro_překladatele.md %}), and localization tools that can be used by both the beginners in localization and the advanced contributors to the community-driven software translation. Information about other relevant dictionaries, translation team information, related projects and translation software documentation are also available on the wiki. You can edit the wiki pages after signing up for an account.

### Participate
There are [many ways you can help us]({% link _pages/zapojte-se.md %}).

### Contacts
The best way to get in touch is to send a message to our [mailing list]({% link _pages/kontakty.md %}). If you are looking for specific people, you can find on [this page]({{ '/kontakty' | relative_url }}) (*zavináč* means *at*).
