---
layout: page
title: Mapa stránek
permalink: /sitemap/
nav_order: 10
sitemap_exclude: true
search_exclude: true
---

Seznam všech stránek. Pro vyhledání v jejich obsahu použijte vyhledávač nahoře.

{% assign all_pages = site.pages
  | where_exp: 'page', 'page.layout != "redirect"'
  | where_exp: 'page', 'page.title'
  | where_exp: 'page', 'page.sitemap_exclude != true'
  | sort: 'url'
%}

<ul>
  {% for page in all_pages %}
    {% assign is_child = page.parent %}
    <li>
      {% if is_child %}
        <span>↳</span>
        <a href="{{ page.url | relative_url }}" title="{{ page.title }}">
          {{ page.title }}
        </a>
      {% else %}
        <a href="{{ page.url | relative_url }}" title="{{ page.title }}">
          <strong>{{ page.title }}</strong>
        </a>
      {% endif %}
    </li>
  {% endfor %}
</ul>
