---
layout: page
title: Kontakty
permalink: /kontakty/
redirect_from:
  - /emailova-konference/
nav_order: 8
---

### E-mailová konference

{: .highlight }
Naše překladatelská konference má e-mailovou adresu [**diskuze-l10n-cz@lists.openalt.org**](mailto:diskuze-l10n-cz@lists.openalt.org) a [archiv má na serveru OpenAltu](https://lists.openalt.org/wws/info/diskuze-l10n-cz).
Vždy nás prosím primárně **kontaktujte na této adrese**.

### On-line diskuze
- [Matrix](https://matrix.to/#/#l10ncz:matrix.org)

### Správci projektu
- [Petr Kovář](https://live.gnome.org/PetrKovar) - *pknbe zavináč volny.cz*
- Michal Stanke - *mstanke zavináč mozilla.cz*

### Původní autor projektu
- Václav Čermák - *vaclav.cermak zavináč gmail.com*

### Sponzor domény a infrastruktury
- [OpenAlt z.s.](https://www.openalt.org/) - *spravci zavináč lists.openalt.org*

### Autor srovnávacího slovníku
- Martin Böhm - jabber: *mhb zavináč jabber.cz*

### Autor loga
- [Jan Tojnar](http://jtojnar.php5.cz/) - *jtojnar zavináč gmail.com*

### Autor ikony „favicon“
- Marek Černocký - *marek zavináč manet.cz*
