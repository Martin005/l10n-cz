---
layout: post
title: Sloučení webu a wiki
authors:
  - Michal Stanke
---

_Ve zkratce_ - obsah naší wiki teď najdete přímo na tomto webu. Pro původní odkazy je zajištěné přesměrování.

Od [havárie serveru]({% post_url 2019-08-13-havarie-serveru-s-wiki-a-emailovou-konferenci %}) před třemi lety naše wiki sdílela hosting s komunitními stránkami Mozilla.cz, než jsem ji o rok později [přestěhoval na server OpenAltu]({% post_url 2020-07-18-prevod-l10n-cz-pod-spolek-openalt %}), kde běžela až dosud. Bezpečný provoz systému MediaWiki ale vyžadoval pravidelné aktualizace, a vzhledem k tomu, že úprav na wiki bylo za celou tu dobu poměrně málo, padlo rozhodnutí wiki sloučit s webem, který je prakticky bezúdržbový.

Web používá generátor [Jekyll](https://jekyllrb.com/), jehož výstupem jsou statické HTML, CSS a JS soubory, které vrací webserver bez potřeby jakékoliv databáze nebo skriptovacího jazyka. Kromě nízké náročnostni na provoz se tím minimalizuje riziko bezpečnostních a provozních problémů, a tím pádem i náročnost na provoz a údržbu.

Podrobnosti o tom, na čem náš web běží, najdete na stránce [Infrastruktura]({% link _pages/infrastruktura.md %}), stejně jako odkaz na návod, jak na stránky a tím pádem i wiki ponovu přispívat. Pokud narazíte na nějakou chybu, nebo budete mít návrh na zlepšení, budeme moc rádi.