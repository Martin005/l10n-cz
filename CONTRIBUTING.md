# Úprava obsahu

Všechny texty na webu jsou psané ve značkovacím jazyce Markdown, případně v HTML. S psaním Markdownu vám pomohou tyto stránky:
- [Mastering Markdown - GitHub Guides](https://guides.github.com/features/mastering-markdown/)
- [Basic writing and formatting syntax - User Documentation](https://help.github.com/articles/basic-writing-and-formatting-syntax/)
- [Syntax \| kramdown](https://kramdown.gettalong.org/syntax.html)

## Existující stránka

Pro úpravu existující stránky ji zobrazte přímo na webu a v patičce klepněte na odkaz _Upravit_. Tento odkaz vám otevře zdrojový soubor ve webovém rozhraní GitLabu, kde klepněte na tlačítko _Open in Web IDE_. Po provedení změn klepněte na tlačítko _Create commit_ a vámi navrhnovaou změnu popište. Ideálně také vyberte možnosti _Create a new branch_ a _Start a new merge commit_. [Merge request](https://docs.gitlab.com/ce/user/project/merge_requests/) umožňuje změnu zkontrolovat před tím, než bude na web zahrnuta.

## Nová stránka nebo zprávička

Pro přidání nové stránky nebo zprávičky bude potřeba vytvořit nový soubor v v adresáři [`_pages`](_pages) nebo [`_posts`](_posts). Informace o nových překladatelských formátech nebo softwaru a návody k nim je nejlepší umístit jako stránku do adresáře [`_pages/wiki`](_pages/wiki). Ve zprávičkách je vhodné informovat o lokalizačních setkáních, postupech týmů, novinkách v překladech apod.

Pro vytvoření nové stránky nebo zprávičky si ve webovém rozhraní GitLabu otevřete odpovídající adresář (viz předchozí odstavec), klepněte na tlačítko `+ ⌄` a vyberte _New file_. Jako šablonu pro začátek můžete zkopírovat a vložit obsah souboru [`_pages/wiki/_sablona.md.tmpl`](_pages/wiki/_sablona.md.tmpl) pro stránku, nebo [`_posts/_0000-00-00-sablona.md.tmpl`](_posts/_0000-00-00-sablona.md.tmpl) pro zprávičku. Nakonec soubor pojmenujte podle názvu stránky nebo zprávičky. V případě zprávičky ještě na začátek souboru přidejte datum ve formátu `rok-mesic-den-nazev.md`, tedy např. `1918-10-28-dnes-vznikl-samostatny-stat.md`.

# Kontakt

V případě problémů s úpravami obsahu [napište do e-mailové konference](https://www.l10n.cz/kontakty/).
